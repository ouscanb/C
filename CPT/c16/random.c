#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void guess(void);

int main(void) {
    int i;
    time_t s;

    printf("The largest number rand() will return is %d\n\n", RAND_MAX);

    for (i = 1; i <= 5; i++) {
        printf("(rand)om number %02d: %d\n", i, rand());
    }
    
    printf("\n");

    s = time(NULL);
    srand((unsigned) s);   
    
    for (i = 1; i <= 5; i++) {
        printf("(srand)om number %02d: %d\n", i, rand());
    }


    printf("\n");
    guess();

    return 0;
}

void guess(void) {
    int n, g;
    int range = 50;
    time_t s;

    s = time(NULL);
    srand((unsigned) s);
    n = rand() % range + 1;

    printf("Guess the number picked between 1 and %d.\n\n", range);

    do {
        printf("Enter your guess: ");
        scanf("%d", &g);
        
        if ( g > n) {
            printf("It is less than that.\n");
        } else if (g < n) {
            printf("It is greater than that.\n");
        } else {
            printf("You won. It is %d.\n", g);
        }
    } while (g != n);

    return;
}


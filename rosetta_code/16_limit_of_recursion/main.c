#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void funcme(unsigned n)
{
    /* printf("%d\n", n); */
    funcme(n + 1);
}

int main(int argc, char *argv[])
{
    /* debug with gdb */
    funcme(0);

    return EXIT_SUCCESS;
}

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

#define ARRLEN(X) ((sizeof (X)) / (sizeof (X[0])))

static char *reverse(char s[])
{
    size_t i, j;
    for (i = 0, j = strlen(s) - 1; i < j; ++i, --j) {
        int c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
    return s;
}

static char *itob(long n, char s[], size_t slen, int b)
{
    if (b < 2 || b > 36) {
        fprintf(stderr, "Unsupported base: %d\n", b);
        exit(EXIT_FAILURE);
    }

    static char digits[] = "0123456789abcdefghijklmnopqrstuvwxyz";
    long sign = n;

    size_t i = 0;
    do {
        s[i++] = digits[abs(n % b)];
    } while ((n /= b) && i < slen - 2);

    /* check whether the conversion is finished */
    if (abs(n)  > 0)
        return NULL;

    if (sign < 0)
        s[i++] = '-';
    s[i] = '\0';

    return reverse(s);
}

int main(void)
{
    int bases[] = {2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,32,36};
    char snum[128] = {0};

    long num = LONG_MIN;
    printf("%ld in\n", num);

    size_t blen = ARRLEN(bases);
    size_t slen = ARRLEN(snum);
    for (size_t i = 0; i < blen; ++i) {
        if (itob(num, snum, slen, bases[i]))
            printf("\tBase %2d : %s\n", bases[i], snum);
        else
            printf("\tBase %2d : Conversion failed!\n", bases[i]);
    }

    return EXIT_SUCCESS;
}

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[])
{
    time_t t = time(NULL);
    printf("%s\n", ctime(&t));
    struct tm *tl = localtime(&t);
    printf("Hour: %02d\nMin : %02d\nSec : %2d\n", tl->tm_hour, tl->tm_min, tl->tm_sec);

    return EXIT_SUCCESS;
}

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

#define ARRLEN 10

int *rand_arr(void)
{
    static int rarr[ARRLEN];
    time_t t = time(NULL);
    srand((unsigned) t);

    for (int i = 0; i < ARRLEN; i++)
        rarr[i] = rand() % 100;

    return rarr;
}

int greatest(int arr[], size_t arr_len)
{
    assert(arr_len > 0);
    int g = arr[0];
    for (int i = 0; i < arr_len; i++)
        /* if (arr[i] > g) */
        /*     g = arr[i]; */
        g = (arr[i] > g) ? arr[i] : g;

    return g;
}

void print_arr(int arr[], size_t arr_len)
{
    for (int i = 0; i < arr_len; i++)
        printf("%d ", arr[i]);
    puts("");
}

int main(int argc, char *argv[])
{
    int *rarr = rand_arr();

    print_arr(rarr, ARRLEN);
    printf("%d\n", greatest(rarr, ARRLEN));

    return EXIT_SUCCESS;
}

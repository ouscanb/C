#include <bits/stdint-uintn.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "emulator.h"
#include "disassembler.h"

static void PrintState(State8080 *state)
{
	uint8_t f = 0x00;
	f |= state->cc.s  << 7;
	f |= state->cc.z  << 6;
	f |= state->cc.ac << 4;
	f |= state->cc.p  << 2;
	f |= 0x01 << 1;
	f |= state->cc.cy;

	/* processor state: condition codes */
	printf("\t %c%c%c%c\t",
			(state->cc.cy == 1) ? 'c' : '.',
			(state->cc.p  == 1) ? 'p' : '.',
			(state->cc.s  == 1) ? 's' : '.',
			(state->cc.z  == 1) ? 'z' : '.');
	/* processor state: registers */
	printf("AF: $%02x%02x BC: $%02x%02x DE: $%02x%02x HL: $%02x%02x SP: $%04x\n",
			state->a, f, state->b, state->c, state->d, state->e, state->h, state->l, state->sp);
}

static void UnimplementedInstruction(State8080 *state)
{
	(void) state;
	printf("\tError: Unimplemented instruction!\n");
	exit(EXIT_FAILURE);
}

static uint8_t Parity(uint8_t val)
{
	uint8_t count = 0;
	for (int i = 0; i < 8; ++i) {
		uint8_t bit = (val >> i) & 0x01;
		count += bit;
	}

	return (count & 0x01) == 0;
}

static void SetZSP(State8080 *state, uint16_t val)
{
	state->cc.z  = ((val & 0xff) == 0);
	state->cc.s  = ((val & 0x80) != 0);
	state->cc.p  = Parity(val & 0xff);
}

static uint16_t GetRegPair(State8080 *state, char reg)
{
	switch (reg) {
		case 'B':
			return (state->b << 8) | (state->c);
		case 'D':
			return (state->d << 8) | (state->e);
		case 'H':
			return (state->h << 8) | (state->l);
		case 'S':
			return state->memory[state->sp] | (state->memory[state->sp + 1] << 8);
	}
	return 0xffff;
}

static void SetRegPair(State8080 *state, char reg, uint16_t val)
{
	switch (reg) {
		case 'B':
			state->b = (val >> 8) & 0xff;
			state->c = val & 0xff;
			break;
		case 'D':
			state->d = (val >> 8) & 0xff;
			state->e = val & 0xff;
			break;
		case 'H':
			state->h = (val >> 8) & 0xff;
			state->l = val & 0xff;
			break;
		case 'S':
			state->memory[state->sp + 1] = (val >> 8) & 0xff;
			state->memory[state->sp] = (val & 0xff);
			break;
	}
}

static uint8_t GetReg(State8080 *state, char reg)
{
	switch (reg) {
		case 'B':
			return state->b;
		case 'C':
			return state->c;
		case 'D':
			return state->d;
		case 'E':
			return state->e;
		case 'H':
			return state->h;
		case 'L':
			return state->l;
		case 'M':
			return state->memory[GetRegPair(state, 'H')];
		case 'A':
			return state->a;
	}
	return 0xff;
}

static uint8_t GetPsw(State8080 *state)
{
	uint8_t psw = 0x00;
	psw |= state->cc.s  << 7;
	psw |= state->cc.z  << 6;
	psw |= state->cc.ac << 4;
	psw |= state->cc.p  << 2;
	psw |= 0x01 << 1;
	psw |= state->cc.cy;

	return psw;
}

static void SetPsw(State8080 *state, uint8_t psw)
{
	state->cc.s  = (psw >> 7) & 0x01;
	state->cc.z  = (psw >> 6) & 0x01;
	state->cc.ac = (psw >> 4) & 0x01;
	state->cc.p  = (psw >> 2) & 0x01;
	state->cc.cy = psw & 0x01;
}

static void SetReg(State8080 *state, uint8_t val, char reg)
{
	switch (reg) {
		case 'B':
			state->b = val;
			break;
		case 'C':
			state->c = val;
			break;
		case 'D':
			state->d = val;
			break;
		case 'E':
			state->e = val;
			break;
		case 'H':
			state->h = val;
			break;
		case 'L':
			state->l = val;
			break;
		case 'M':
			state->memory[GetRegPair(state, 'H')] = val;
			break;
		case 'A':
			state->a = val;
			break;
	}
}

static uint16_t GetWord(State8080 *state, uint16_t offset)
{
	uint16_t word = (state->memory[offset + 1] << 8) | state->memory[offset];
	return word;
}

static void SetWord(State8080 *state, uint16_t offset, uint16_t val)
{
	state->memory[offset] = (val & 0xff);
	state->memory[offset + 1] = (val >> 8);
}

static void Nop()
{
	return;
}

static void Add(State8080 *state, uint8_t val)
{
	uint16_t answer = state->a + val;
	SetZSP(state, answer);
	state->cc.cy = (answer > 0xff);
	state->cc.ac = ((answer >> 4) > 0x0f);
	state->a = (answer & 0xff);
}

static void Adc(State8080 *state, uint8_t val)
{
	Add(state, val + state->cc.cy);
}

static void Sub(State8080 *state, uint8_t val)
{
	uint16_t answer = state->a - val;
	SetZSP(state, answer);
	state->cc.cy = (answer > 0xff);
	state->cc.ac = ((answer >> 4) > 0x0f);
	state->a = (answer & 0xff);
}

static void Sbb(State8080 *state, uint8_t val)
{
	Sub(state, val + state->cc.cy);
}

static void Adi(State8080 *state)
{
	uint8_t val = state->memory[state->pc + 1];
	Add(state, val);
	state->pc++;
}

static void Aci(State8080 *state)
{
	uint16_t val = state->memory[state->pc + 1] + state->cc.cy;
	Add(state, (val & 0xff));
	state->pc++;
}

static void Sui(State8080 *state)
{
	uint8_t val = state->memory[state->pc + 1];
	Sub(state, val);
	state->pc++;
}

static void Sbi(State8080 *state)
{
	uint16_t val = (state->memory[state->pc + 1] + state->cc.cy);
	Sub(state, (val & 0xff));
	state->pc++;
}

static void Lxi(State8080 *state, char reg)
{
	switch(reg) {
		case 'B':
			SetRegPair(state, 'B', GetWord(state, state->pc + 1));
			break;
		case 'D':
			SetRegPair(state, 'D', GetWord(state, state->pc + 1));
			break;
		case 'H':
			SetRegPair(state, 'H', GetWord(state, state->pc + 1));
			break;
		case 'S':
			state->sp = GetWord(state, state->pc + 1);
	}
	state->pc += 2;
}

static void Stax(State8080 *state, char reg)
{
	uint16_t offset = GetRegPair(state, reg);
	state->memory[offset] = state->a;
}

static void Inx(State8080 *state, char reg)
{
	uint16_t answer = GetRegPair(state, reg) + 1;
	SetRegPair(state, reg, (answer & 0xffff));
	if (reg == 'S') state->sp++;
}

static void Inr(State8080 *state, char reg)
{
	uint8_t val = GetReg(state, reg);
	uint16_t answer = val + 1;
	SetZSP(state, answer);
	SetReg(state, (answer & 0xff), reg);
}

static void Dcx(State8080 *state, char reg)
{
	uint16_t answer = GetRegPair(state, reg) - 1;
	SetRegPair(state, reg, answer);
	if (reg == 'S') state->sp--;
}

static void Dcr(State8080 *state, char reg)
{
	uint8_t val = GetReg(state, reg);
	uint16_t answer = val - 1;
	SetZSP(state, answer);
	SetReg(state, (answer & 0xff), reg);
}

static void Cma(State8080 *state)
{
	state->a = ~state->a;
}

static void Daa(State8080 *state)
{
	uint8_t correction = 0;
	uint8_t lsb = state->a & 0x0f;
	uint8_t msb = state->a >> 4;

	if ((lsb > 9) || state->cc.ac == 1) {
		correction += 0x06;
	}

	if ((msb > 9) || state->cc.cy == 1 || ((msb >= 9) && (lsb > 9))) {
		correction += 0x60;
		state->cc.cy = 1;
	}

	Add(state, correction);
	SetZSP(state, state->a);
}

static void Mvi(State8080 *state, char reg)
{
	SetReg(state, state->memory[state->pc + 1], reg);
	state->pc++;
}

static void Mov(State8080 *state, char reg_dst, char reg_src)
{
	SetReg(state, GetReg(state, reg_src), reg_dst);
}

static void Ana(State8080 *state, char reg)
{
	state->a &= GetReg(state, reg);
	state->cc.cy = 0x00;
	SetZSP(state, state->a);
}

static void Xra(State8080 *state, char reg)
{
	state->a ^= GetReg(state, reg);
	state->cc.cy = 0x00;
	state->cc.ac = 0x00;
	SetZSP(state, state->a);
}

static void Ora(State8080 *state, char reg)
{
	state->a |= GetReg(state, reg);
	state->cc.cy = 0x00;
	state->cc.ac = 0x00;
	SetZSP(state, state->a);
}

static void Cmp(State8080 *state, char reg)
{
	uint16_t answer = state->a  - GetReg(state, reg);
	state->cc.cy = (answer > 0xff);
	state->cc.ac = !((answer >> 4) > 0x0f);
	SetZSP(state, answer);
}

static void Ani(State8080 *state)
{
	state->a &= state->memory[state->pc + 1];
	state->cc.cy = 0x00;
	SetZSP(state, state->a);
	state->pc++;
}

static void Xri(State8080 *state)
{
	state->a ^= state->memory[state->pc + 1];
	state->cc.cy = 0x00;
	SetZSP(state, state->a);
	state->pc++;
}

static void Ori(State8080 *state)
{
	state->a |= state->memory[state->pc + 1];
	state->cc.cy = 0x00;
	SetZSP(state, state->a);
	state->pc++;
}

static void Cpi(State8080 *state)
{
	uint16_t answer = state->a - state->memory[state->pc + 1];
	state->cc.cy = (answer > 0xff);
	state->cc.ac = !((answer >> 4) > 0x0f);
	SetZSP(state, answer);
	state->pc++;
}

static void Rlc(State8080 *state)
{
	state->cc.cy = state->a >> 7;
	state->a = (state->a << 1) | state->cc.cy;
}

static void Rrc(State8080 *state)
{
	state->cc.cy = state->a & 0x01;
	state->a = (state->a >> 1) | (state->cc.cy << 7);
}

static void Dad(State8080 *state, char reg)
{
	uint32_t answer = GetRegPair(state, reg) + GetRegPair(state, 'H');
	state->cc.cy = (answer > 0xffff);
	SetRegPair(state, 'H', (answer & 0xffff));
	if (reg == 'S') state->sp += 2;
}

static void Ldax(State8080 *state, char reg)
{
	uint16_t offset = GetRegPair(state, reg);
	SetReg(state, state->memory[offset], 'A');
}

static void Ral(State8080 *state)
{
	uint8_t cy_prev = state->cc.cy;
	state->cc.cy = state->a >> 7;
	state->a = (state->a << 1) | cy_prev;
}

static void Rar(State8080 *state)
{
	uint8_t cy_prev = state->cc.cy;
	state->cc.cy = state->a & 0x01;
	state->a = (state->a >> 1) | (cy_prev << 7);
}

static void Shld(State8080 *state)
{
	uint16_t offset = GetWord(state, state->pc + 1);
	uint16_t val = GetRegPair(state,'H');
	SetWord(state, offset, val);
	state->pc += 2;
}

static void Lhld(State8080 *state)
{
	uint16_t offset = GetWord(state, state->pc + 1);
	uint16_t val = GetWord(state, offset);
	SetRegPair(state, 'H', val);
	state->pc += 2;
}

static void Jmp(State8080 *state, bool cond)
{
	if (cond) {
		state->pc = GetWord(state, state->pc + 1);
		state->pc--;
	} else {
		state->pc += 2;
	}
}

static void Jnz(State8080 *state)
{
	Jmp(state, (0 == state->cc.z));
}

static void Jz(State8080 *state)
{
	Jmp(state, (1 == state->cc.z));
}

static void Jnc(State8080 *state)
{
	Jmp(state, (0 == state->cc.cy));
}

static void Jc(State8080 *state)
{
	Jmp(state, (1 == state->cc.cy));
}

static void Jpo(State8080 *state)
{
	Jmp(state, (0 == state->cc.p));
}

static void Jpe(State8080 *state)
{
	Jmp(state, (1 == state->cc.p));
}

static void Jp(State8080 *state)
{
	Jmp(state, (0 == state->cc.s));
}

static void Jm(State8080 *state)
{
	Jmp(state, (1 == state->cc.s));
}

static void Pop(State8080 *state, char reg)
{
	SetRegPair(state, reg, GetRegPair(state, 'S'));
	state->sp += 2;
}

static void PopPsw(State8080 *state)
{
	SetPsw(state, state->memory[state->sp + 1]);
	state->a = state->memory[state->sp];
	state->sp += 2;
}

static void Push(State8080 *state, uint16_t val)
{
	state->sp -= 2;
	SetRegPair(state, 'S', val);
}

static void PushPsw(State8080 *state)
{
	state->sp -= 2;
	state->memory[state->sp] = state->a;
	state->memory[state->sp + 1] = GetPsw(state);
}

static void Sphl(State8080 *state)
{
	state->sp = GetRegPair(state, 'H');
}

static void Xthl(State8080 *state)
{
	uint16_t s = GetRegPair(state, 'S');
	SetRegPair(state, 'S', GetRegPair(state, 'H'));
	SetRegPair(state, 'H', s);
}

static void Call(State8080 *state, bool cond)
{
	if (cond) {
		Push(state, state->pc + 2);
		Jmp(state, true);
	} else
		state->pc += 2;
}

static void Cnz(State8080 *state) {
	Call(state, (0 == state->cc.z));
}

static void Cz(State8080 *state) {
	Call(state, (1 == state->cc.z));
}

static void Cnc(State8080 *state) {
	Call(state, (0 == state->cc.cy));
}

static void Cc(State8080 *state) {
	Call(state, (1 == state->cc.cy));
}

static void Cpo(State8080 *state)
{
	Call(state, (0 == state->cc.p));
}

static void Cpe(State8080 *state)
{
	Call(state, (1 == state->cc.p));
}

static void Cp(State8080 *state)
{
	Call(state, (0 == state->cc.s));
}

static void Cm(State8080 *state)
{
	Call(state, (1 == state->cc.s));
}

static void Ret(State8080 *state, bool cond)
{
	if (cond) {
		state->pc = state->memory[state->sp] | (state->memory[state->sp + 1] << 8);
		state->sp += 2;
	}
}

static void Rnz(State8080 *state) {
	Ret(state, (0 == state->cc.z));
}

static void Rz(State8080 *state) {
	Ret(state, (1 == state->cc.z));
}

static void Rnc(State8080 *state) {
	Ret(state, (0 == state->cc.cy));
}

static void Rc(State8080 *state) {
	Ret(state, (1 == state->cc.cy));
}

static void Rpo(State8080 *state)
{
	Ret(state, (0 == state->cc.p));
}

static void Rpe(State8080 *state)
{
	Ret(state, (1 == state->cc.p));
}

static void Rp(State8080 *state)
{
	Ret(state, (0 == state->cc.s));
}

static void Rm(State8080 *state)
{
	Ret(state, (1 == state->cc.s));
}

static void Pchl(State8080 *state)
{
	state->pc = GetRegPair(state, 'H');
	state->pc--;
}

static void Rst(State8080 *state, uint16_t word)
{
	Push(state, state->pc);
	state->pc = word;
}

static void Xchg(State8080 *state)
{
	uint16_t h = GetRegPair(state, 'H');
	SetRegPair(state, 'H', GetRegPair(state, 'D'));
	SetRegPair(state, 'D', h);
	return;
}

static void Hlt(State8080 *state)
{
	state->halted = 1;
}

static void In(State8080 *state, uint8_t port)
{
	state->a = state->port_in(state, port);
	state->pc++;
}

static uint8_t Out(State8080 *state, uint8_t port)
{
	uint8_t ret = state->port_out(state, port);
	state->pc++;
	return ret;
}

static void Ei(State8080 *state)
{
	state->interupt_enabled = 1;
	return;
}

static void Di(State8080 *state)
{
	state->interupt_enabled = 0;
	return;
}

static void Sta(State8080 *state)
{
	uint16_t offset = GetWord(state, state->pc + 1);
	state->memory[offset] = state->a;
	state->pc += 2;
}

static void Lda(State8080 *state)
{
	uint16_t offset = GetWord(state, state->pc + 1);
	state->a = state->memory[offset];
	state->pc += 2;
}

static void Stc(State8080 *state)
{
	state->cc.cy = 0x01;
}

static void Cmc(State8080 *state)
{
	state->cc.cy ^= 0x01;
}

uint8_t Emulate8080(State8080 *state)
{
	uint8_t ret = 0;
	uint8_t *opcode = &state->memory[state->pc];
	Disassemble8080(state->memory, state->pc);

	switch (*opcode) {
		case 0x00: // NOP
			{
				Nop();
			}
			break;
		case 0x01: // LXI B,WORD
			{
				Lxi(state, 'B');
			}
			break;
		case 0x02: // STAX B
			{
				Stax(state, 'B');
			}
			break;
		case 0x03: // INX B
			{
				Inx(state, 'B');
			}
			break;
		case 0x04: // INR B
			{
				Inr(state, 'B');
			}
			break;
		case 0x05: // DCR B
			{
				Dcr(state, 'B');
			}
			break;
		case 0x06: // MVI B
			{
				Mvi(state, 'B');
			}
			break;
		case 0x07: // RLC
			{
				Rlc(state);
			}
			break;
		case 0x08:
			{
				Nop();
			}
			break;
		case 0x09: // DAD B
			{
				Dad(state, 'B');
			}
			break;
		case 0x0a: // LDAX B
			{
				Ldax(state, 'B');
			}
			break;
		case 0x0b: // DCX B
			{
				Dcx(state, 'B');
			}
			break;
		case 0x0c: // INR C
			{
				Inr(state, 'C');
			}
			break;
		case 0x0d: // DCR C
			{
				Dcr(state, 'C');
			}
			break;
		case 0x0e: // MVI C
			{
				Mvi(state, 'C');
			}
			break;
		case 0x0f: // RRC
			{
				Rrc(state);
			}
			break;

		case 0x10:
			{
				Nop();
			}
			break;
		case 0x11: // LXI D,WORD
			{
				Lxi(state, 'D');
			}
			break;
		case 0x12: // STAX D
			{
				Stax(state, 'D');
			}
			break;
		case 0x13: // INX D
			{
				Inx(state, 'D');
			}
			break;
		case 0x14: // INR D
			{
				Inr(state, 'D');
			}
			break;
		case 0x15: // DCR D
			{
				Dcr(state, 'D');
			}
			break;
		case 0x16: // MVI D
			{
				Mvi(state, 'D');
			}
			break;
		case 0x17: // RAL
			{
				Ral(state);
			}
			break;
		case 0x18:
			{
				Nop();
			}
			break;
		case 0x19: // DAD D
			{
				Dad(state, 'D');
			}
			break;
		case 0x1a: // LDAX D
			{
				Ldax(state, 'D');
			}
			break;
		case 0x1b: // DCX D
			{
				Dcx(state, 'D');
			}
			break;
		case 0x1c: // INR E
			{
				Inr(state, 'E');
			}
			break;
		case 0x1d: // DCR E
			{
				Dcr(state, 'E');
			}
			break;
		case 0x1e: // MVI E
			{
				Mvi(state, 'E');
			}
			break;
		case 0x1f:
			{
				Rar(state);
			}
			break;

		case 0x20:
			{
				Nop();
			}
			break;
		case 0x21: // LXI H,WORD
			{
				Lxi(state, 'H');
			}
			break;
		case 0x22: // SHLD ADDR
			{
				Shld(state);
			}
			break;
		case 0x23: // INX H
			{
				Inx(state, 'H');
			}
			break;
		case 0x24: // INR H
			{
				Inr(state, 'H');
			}
			break;
		case 0x25: // DCR H
			{
				Dcr(state, 'H');
			}
			break;
		case 0x26: // MVI H
			{
				Mvi(state, 'H');
			}
			break;
		case 0x27:
			{
				Daa(state);
			}
			break;
		case 0x28:
			{
				Nop();
			}
			break;
		case 0x29: // DAD H
			{
				Dad(state, 'H');
			}
			break;
		case 0x2a: // LHLD ADDR
			{
				Lhld(state);
			}
			break;
		case 0x2b: // DCX H
			{
				Dcx(state, 'H');
			}
			break;
		case 0x2c: // INR L
			{
				Inr(state, 'L');
			}
			break;
		case 0x2d: // DCR L
			{
				Dcr(state, 'L');
			}
			break;
		case 0x2e: // MVI L
			{
				Mvi(state, 'L');
			}
			break;
		case 0x2f:
			{
				Cma(state);
			}
			break;

		case 0x30:
			{
				Nop();
			}
			break;
		case 0x31: // LXI S,WORD
			{
				Lxi(state, 'S');
			}
			break;
		case 0x32: // STA ADDR
			{
				Sta(state);
			}
			break;
		case 0x33: // INX S
			{
				Inx(state, 'S');
			}
			break;
		case 0x34: // INR M
			{
				Inr(state, 'M');
			}
			break;
		case 0x35: // DCR M
			{
				Dcr(state, 'M');
			}
			break;
		case 0x36: // MVI M
			{
					Mvi(state, 'M');
			}
			break;
		case 0x37: // STC
			{
				Stc(state);
			}
			break;
		case 0x38:
			{
				Nop();
			}
			break;
		case 0x39: // DAD S
			{
				Dad(state, 'S');
			}
			break;
		case 0x3a: // LDA ADDR
			{
				Lda(state);
			}
			break;
		case 0x3b: // DCX S
			{
				Dcx(state, 'S');
			}
			break;
		case 0x3c: // INR A
			{
				Inr(state, 'A');
			}
			break;
		case 0x3d: // DCR A
			{
				Dcr(state, 'A');
			}
			break;
		case 0x3e: // MVI A
			{
				Mvi(state, 'A');
			}
			break;
		case 0x3f: // CMC
			{
				Cmc(state);
			}
			break;

		case 0x40: // MOV B,B
			{
				Mov(state, 'B', 'B');
			}
			break;
		case 0x41: // MOV B,C
			{
				Mov(state, 'B', 'C');
			}
			break;
		case 0x42: // MOV B,D
			{
				Mov(state, 'B', 'D');
			}
			break;
		case 0x43: // MOV B,E
			{
				Mov(state, 'B', 'E');
			}
			break;
		case 0x44: // MOV B,H
			{
				Mov(state, 'B', 'H');
			}
			break;
		case 0x45: // MOV B,L
			{
				Mov(state, 'B', 'L');
			}
			break;
		case 0x46: // MOV B,M
			{
				Mov(state, 'B', 'M');
			}
			break;
		case 0x47: // MOV B,A
			{
				Mov(state, 'B', 'A');
			}
			break;
		case 0x48:
			{
				Mov(state, 'C', 'B');
			}
			break;
		case 0x49:
			{
				Mov(state, 'C', 'C');
			}
			break;
		case 0x4a:
			{
				Mov(state, 'C', 'D');
			}
			break;
		case 0x4b:
			{
				Mov(state, 'C', 'E');
			}
			break;
		case 0x4c:
			{
				Mov(state, 'C', 'H');
			}
			break;
		case 0x4d:
			{
				Mov(state, 'C', 'L');
			}
			break;
		case 0x4e:
			{
				Mov(state, 'C', 'M');
			}
			break;
		case 0x4f:
			{
				Mov(state, 'C', 'A');
			}
			break;

		case 0x50:
			{
				Mov(state, 'D', 'B');
			}
			break;
		case 0x51:
			{
				Mov(state, 'D', 'C');
			}
			break;
		case 0x52:
			{
				Mov(state, 'D', 'D');
			}
			break;
		case 0x53:
			{
				Mov(state, 'D', 'E');
			}
			break;
		case 0x54:
			{
				Mov(state, 'D', 'H');
			}
			break;
		case 0x55:
			{
				Mov(state, 'D', 'L');
			}
			break;
		case 0x56:
			{
				Mov(state, 'D', 'M');
			}
			break;
		case 0x57:
			{
				Mov(state, 'D', 'A');
			}
			break;
		case 0x58:
			{
				Mov(state, 'E', 'B');
			}
			break;
		case 0x59:
			{
				Mov(state, 'E', 'C');
			}
			break;
		case 0x5a:
			{
				Mov(state, 'E', 'D');
			}
			break;
		case 0x5b:
			{
				Mov(state, 'E', 'E');
			}
			break;
		case 0x5c:
			{
				Mov(state, 'E', 'H');
			}
			break;
		case 0x5d:
			{
				Mov(state, 'E', 'L');
			}
			break;
		case 0x5e:
			{
				Mov(state, 'E', 'M');
			}
			break;
		case 0x5f:
			{
				Mov(state, 'E', 'A');
			}
			break;

		case 0x60:
			{
				Mov(state, 'H', 'B');
			}
			break;
		case 0x61:
			{
				Mov(state, 'H', 'C');
			}
			break;
		case 0x62:
			{
				Mov(state, 'H', 'D');
			}
			break;
		case 0x63:
			{
				Mov(state, 'H', 'E');
			}
			break;
		case 0x64:
			{
				Mov(state, 'H', 'H');
			}
			break;
		case 0x65:
			{
				Mov(state, 'H', 'L');
			}
			break;
		case 0x66:
			{
				Mov(state, 'H', 'M');
			}
			break;
		case 0x67:
			{
				Mov(state, 'H', 'A');
			}
			break;
		case 0x68:
			{
				Mov(state, 'L', 'B');
			}
			break;
		case 0x69:
			{
				Mov(state, 'L', 'C');
			}
			break;
		case 0x6a:
			{
				Mov(state, 'L', 'D');
			}
			break;
		case 0x6b:
			{
				Mov(state, 'L', 'E');
			}
			break;
		case 0x6c:
			{
				Mov(state, 'L', 'H');
			}
			break;
		case 0x6d:
			{
				Mov(state, 'L', 'L');
			}
			break;
		case 0x6e:
			{
				Mov(state, 'L', 'M');
			}
			break;
		case 0x6f:
			{
				Mov(state, 'L', 'A');
			}
			break;

		case 0x70:
			{
				Mov(state, 'M', 'B');
			}
			break;
		case 0x71:
			{
				Mov(state, 'M', 'C');
			}
			break;
		case 0x72:
			{
				Mov(state, 'M', 'D');
			}
			break;
		case 0x73:
			{
				Mov(state, 'M', 'E');
			}
			break;
		case 0x74:
			{
				Mov(state, 'M', 'H');
			}
			break;
		case 0x75:
			{
				Mov(state, 'M', 'L');
			}
			break;
		case 0x76: // HLT
			{
				Hlt(state);
			}
			break;
		case 0x77:
			{
				Mov(state, 'M', 'A');
			}
			break;
		case 0x78:
			{
				Mov(state, 'A', 'B');
			}
			break;
		case 0x79:
			{
				Mov(state, 'A', 'C');
			}
			break;
		case 0x7a:
			{
				Mov(state, 'A', 'D');
			}
			break;
		case 0x7b:
			{
				Mov(state, 'A', 'E');
			}
			break;
		case 0x7c:
			{
				Mov(state, 'A', 'H');
			}
			break;
		case 0x7d:
			{
				Mov(state, 'A', 'L');
			}
			break;
		case 0x7e:
			{
				Mov(state, 'A', 'M');
			}
			break;
		case 0x7f:
			{
				Mov(state, 'A', 'A');
			}
			break;

		case 0x80: // ADD B
			{
				Add(state, state->b);
			}
			break;
		case 0x81: // ADD C
			{
				Add(state, state->c);
			}
			break;
		case 0x82: // ADD D
			{
				Add(state, state->d);
			}
			break;
		case 0x83: // ADD E
			{
				Add(state, state->e);
			}
			break;
		case 0x84: // ADD H
			{
				Add(state, state->h);
			}
			break;
		case 0x85: // ADD L
			{
				Add(state, state->l);
			}
			break;
		case 0x86: // ADD M
			{
				Add(state, state->memory[GetRegPair(state, 'H')]);
			}
			break;
		case 0x87: // ADD A
			{
				Add(state, state->a);
			}
			break;
		case 0x88: // ADC B
			{
				Adc(state, state->b);
			}
			break;
		case 0x89: // ADC C
			{
				Adc(state, state->c);
			}
			break;
		case 0x8a: // ADC D
			{
				Adc(state, state->d);
			}
			break;
		case 0x8b: // ADC E
			{
				Adc(state, state->e);
			}
			break;
		case 0x8c: // ADC H
			{
				Adc(state, state->h);
			}
			break;
		case 0x8d: // ADC L
			{
				Adc(state, state->l);
			}
			break;
		case 0x8e: // ADC M
			{
				Adc(state, state->memory[GetRegPair(state, 'H')]);
			}
			break;
		case 0x8f: // ADC A
			{
				Adc(state, state->a);
			}
			break;

		case 0x90: // SUB B
			{
				Sub(state, state->b);
			}
			break;
		case 0x91: // SUB C
			{
				Sub(state, state->c);
			}
			break;
		case 0x92: // SUB D
			{
				Sub(state, state->d);
			}
			break;
		case 0x93: // SUB E
			{
				Sub(state, state->e);
			}
			break;
		case 0x94: // SUB H
			{
				Sub(state, state->h);
			}
			break;
		case 0x95: // SUB L
			{
				Sub(state, state->l);
			}
			break;
		case 0x96: // SUB M
			{
				Sub(state, state->memory[GetRegPair(state, 'H')]);
			}
			break;
		case 0x97: // SUB A
			{
				Sub(state, state->a);
			}
			break;
		case 0x98: // SBB B
			{
				Sbb(state, state->b);
			}
			break;
		case 0x99: // SBB C
			{
				Sbb(state, state->c);
			}
			break;
		case 0x9a: // SBB D
			{
				Sbb(state, state->d);
			}
			break;
		case 0x9b: // SBB E
			{
				Sbb(state, state->e);
			}
			break;
		case 0x9c: // SBB H
			{
				Sbb(state, state->h);
			}
			break;
		case 0x9d: // SBB L
			{
				Sbb(state, state->l);
			}
			break;
		case 0x9e: // SBB M
			{
				Sbb(state, state->memory[GetRegPair(state, 'H')]);
			}
			break;
		case 0x9f: // SBB A
			{
				Sbb(state, state->a);
			}
			break;

		case 0xa0: // ANA B
			{
				Ana(state, 'B');
			}
			break;
		case 0xa1:
			{
				Ana(state, 'C');
			}
			break;
		case 0xa2:
			{
				Ana(state, 'D');
			}
			break;
		case 0xa3:
			{
				Ana(state, 'E');
			}
			break;
		case 0xa4:
			{
				Ana(state, 'H');
			}
			break;
		case 0xa5:
			{
				Ana(state, 'L');
			}
			break;
		case 0xa6:
			{
				Ana(state, 'M');
			}
			break;
		case 0xa7:
			{
				Ana(state, 'A');
			}
			break;
		case 0xa8: // XRA B
			{
				Xra(state, 'B');
			}
			break;
		case 0xa9:
			{
				Xra(state, 'C');
			}
			break;
		case 0xaa:
			{
				Xra(state, 'D');
			}
			break;
		case 0xab:
			{
				Xra(state, 'E');
			}
			break;
		case 0xac:
			{
				Xra(state, 'H');
			}
			break;
		case 0xad:
			{
				Xra(state, 'L');
			}
			break;
		case 0xae:
			{
				Xra(state, 'M');
			}
			break;
		case 0xaf:
			{
				Xra(state, 'A');
			}
			break;

		case 0xb0: // ORA B
			{
				Ora(state, 'B');
			}
			break;
		case 0xb1:
			{
				Ora(state, 'C');
			}
			break;
		case 0xb2:
			{
				Ora(state, 'D');
			}
			break;
		case 0xb3:
			{
				Ora(state, 'E');
			}
			break;
		case 0xb4:
			{
				Ora(state, 'H');
			}
			break;
		case 0xb5:
			{
				Ora(state, 'L');
			}
			break;
		case 0xb6:
			{
				Ora(state, 'M');
			}
			break;
		case 0xb7:
			{
				Ora(state, 'A');
			}
			break;
		case 0xb8: // CMP B
			{
				Cmp(state, 'B');
			}
			break;
		case 0xb9:
			{
				Cmp(state, 'C');
			}
			break;
		case 0xba:
			{
				Cmp(state, 'D');
			}
			break;
		case 0xbb:
			{
				Cmp(state, 'E');
			}
			break;
		case 0xbc:
			{
				Cmp(state, 'H');
			}
			break;
		case 0xbd:
			{
				Cmp(state, 'L');
			}
			break;
		case 0xbe:
			{
				Cmp(state, 'M');
			}
			break;
		case 0xbf:
			{
				Cmp(state, 'A');
			}
			break;

		case 0xc0: // RNZ
			{
				Rnz(state);
			}
			break;
		case 0xc1: // POP B
			{
				Pop(state, 'B');
			}
			break;
		case 0xc2: // JNZ ADDR
			{
				Jnz(state);
			}
			break;
		case 0xc3: // JMP ADDR
			{
				Jmp(state, true);
			}
			break;
		case 0xc4: // CNZ ADDR
			{
				Cnz(state);
			}
			break;
		case 0xc5: // PUSH B
			{
				Push(state, GetRegPair(state, 'B'));
			}
			break;
		case 0xc6: // ADI byte
			{
				Adi(state);
			}
			break;
		case 0xc7: // RST 0
			{
				Rst(state, 0 << 3);
			}
			break;
		case 0xc8: // RZ
			{
				Rz(state);
			}
			break;
		case 0xc9: // RET ADDR
			{
				Ret(state, true);
			}
			break;
		case 0xca: // JZ ADDR
			{
				Jz(state);
			}
			break;
		case 0xcb: // JMP ADDR
			{
				Jmp(state, true);
			}
			break;
		case 0xcc: // CZ ADDR
			{
				Cz(state);
			}
			break;
		case 0xcd: // CALL ADDR
			{
				Call(state, true);
			}
			break;
		case 0xce: // ACI
			{
				Aci(state);
			}
			break;
		case 0xcf: // RST 1
			{
				Rst(state, 1 << 3);
			}
			break;

		case 0xd0: // RNC
			{
				Rnc(state);
			}
			break;
		case 0xd1: // POP D
			{
				Pop(state, 'D');
			}
			break;
		case 0xd2: // JNC ADDR
			{
				Jnc(state);
			}
			break;
		case 0xd3: // OUT
			{
				ret = Out(state, state->memory[state->pc + 1]);
			}
			break;
		case 0xd4: // CNC ADDR
			{
				Cnc(state);
			}
			break;
		case 0xd5: // PUSH D
			{
				Push(state, GetRegPair(state, 'D'));
			}
			break;
		case 0xd6:
			{
				Sui(state);
			}
			break;
		case 0xd7: // RST 2
			{
				Rst(state, 2 << 3);
			}
			break;
		case 0xd8: // RC
			{
				Rc(state);
			}
			break;
		case 0xd9: // RET
	 		{
				Ret(state, true);
			}
			break;
		case 0xda: // JC ADDR
			{
				Jc(state);
			}
			break;
		case 0xdb: // IN
			{
				In(state, state->memory[state->pc + 1]);
			}
			break;
		case 0xdc: // CC ADDR
			{
				Cc(state);
			}
			break;
		case 0xdd:
			Call(state, true);
			break;
		case 0xde: // SBI
			{
				Sbi(state);
			}
			break;
		case 0xdf: // RST 3
			{
				Rst(state, 3 << 3);
			}
			break;

		case 0xe0: // RPO
			{
				Rpo(state);
			}
			break;
		case 0xe1: // POP H
			{
				Pop(state, 'H');
			}
			break;
		case 0xe2:
			{
				Jpo(state);
			}
			break;
		case 0xe3: // XTHL
			{
				Xthl(state);
			}
			break;
		case 0xe4: // CPO ADDR
			{
				Cpo(state);
			}
			break;
		case 0xe5: // PUSH H
			{
				Push(state, GetRegPair(state, 'H'));
			}
			break;
		case 0xe6: // ANI
			{
				Ani(state);
			}
			break;
		case 0xe7: // RST 4
			{
				Rst(state, 4 << 3);
			}
			break;
		case 0xe8: // RPE
			{
				Rpe(state);
			}
			break;
		case 0xe9: // PCHL
			{
				Pchl(state);
			}
			break;
		case 0xea:
			{
				Jpe(state);
			}
			break;
		case 0xeb: // XCHG
			{
				Xchg(state);
			}
			break;
		case 0xec: // CPE ADDR
			{
				Cpe(state);
			}
			break;
		case 0xed: // CALL ADDR
			{
				Call(state, true);
			}
			break;
		case 0xee: // XRI
			{
				Xri(state);
			}
			break;
		case 0xef: // RST 5
			{
				Rst(state, 5 << 3);
			}
			break;

		case 0xf0: // RP
			{
				Rp(state);
			}
			break;
		case 0xf1: // POP PSW
			{
				PopPsw(state);
			}
			break;
		case 0xf2: // JP ADDR
			{
				Jp(state);
			}
			break;
		case 0xf3: // DI
			{
				Di(state);
			}
			break;
		case 0xf4: // CP ADDR
			{
				Cp(state);
			}
			break;
		case 0xf5: // PUSH PSW
			{
				PushPsw(state);
			}
			break;
		case 0xf6: // ORI
			{
				Ori(state);
			}
			break;
		case 0xf7: // RST 6
			{
				Rst(state, 6 << 3);
			}
			break;
		case 0xf8: // RM
			{
				Rm(state);
			}
			break;
		case 0xf9: // SPHL
			{
				Sphl(state);
			}
			break;
		case 0xfa: // JM ADDR
			{
				Jm(state);
			}
			break;
		case 0xfb: // EI
			{
				Ei(state);
			}
			break;
		case 0xfc: // CM ADDR
			{
				Cm(state);
			}
			break;
		case 0xfd: // CALL ADDR
			{
				Call(state, true);
			}
			break;
		case 0xfe: // CPI
			{
				Cpi(state);
			}
			break;
		case 0xff: // RST 7
			{
				Rst(state, 7 << 3);
			}
			break;
	}
	state->pc++;

	PrintState(state);

	return ret;
}

void GenerateInterrupt8080(State8080 *state, int interupt_num)
{
	Push(state, state->pc);
	state->pc = 8 * interupt_num;
	state->interupt_enabled = 1;
}

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ARRL 10

int * random_arr(void);
void print_arr(int *arr, int arr_len);
void selection_sort(int *arr, int arr_len);
int  binary_search(int *arr, int arr_len, int n);

int main(void) {

    int *arr;
    arr = random_arr();
    print_arr(arr, ARRL);

    selection_sort(arr, ARRL);
    print_arr(arr, ARRL);

    int q = 0;
    int n, i;
    do {
        printf("Enter an integer: ");
        scanf("%d", &n);
        
        i = binary_search(arr, ARRL, n);
        if (i != -1) {
            printf("%d is found at the index %d.\n", n, i);
            q = 1;
        } else {
            printf("%d is not found.\n", n);
        }
    } while (!q);

    return 0;
}

int * random_arr(void) {
    /* create a static array for random ints */
    static int rarr[ARRL];
    static int t = 1;
    time_t s;

    s = time(NULL);
    srand((unsigned) s * t);
    t++;
 
    int *p = rarr;
    for (int i = 0; i < ARRL; i++) {
        /*rarr[i] = rand() % 100;*/
        *p++ = rand() % 100;
    }

    return rarr;
}

void print_arr(int *arr, int arr_len) {
    for (int i = 0; i < arr_len; i++) {
        printf("%d ", *(arr + i));        
    }

    printf("\n");

    return;
}

void selection_sort(int *arr, int arr_len) {
    /* t is used to swap elements,
     * k is used as the index of the min */
    int t, k;
    for (int i = 0; i < (arr_len - 1); i++) {
        k = i;
        for (int j = i + 1; j < arr_len; j++) {
            if (arr[j] < arr[k]) {
                k = j;
            }            
        }

        if (k != i) {
            t = arr[i];
            arr[i] = arr[k];
            arr[k] = t;
        }
    }

    return;
}

int binary_search(int *arr, int arr_len, int n) {
    /* do a binary search on a sorted array
     * and return the index of the element (if any), 
     * otherwise return -1 */

    /* mi: index of the middle element
     * bi: index of the first element of the (sub)array
     * ei: index of the last element of the (sub)array */
    int mi;
    int bi = 0, ei = arr_len;

    while (ei >= bi) {
        mi = bi + (ei - bi) / 2;
        if (n == arr[mi]) {
            return mi;
        } else if (n < arr[mi]) {
            ei = mi - 1;
        } else {
            bi = mi + 1;
        }
    }

    return -1;
}

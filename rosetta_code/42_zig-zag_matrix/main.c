#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <SFML/Graphics.h>

#define DIM 5

void zigZagDisplay(int m[DIM][DIM])
{
	int xres = 400;
	sfVideoMode mode = {xres, xres, 32};

	sfRenderWindow *window = NULL;
	window = sfRenderWindow_create(mode, "Zig-zag Matrix", sfClose, NULL);
	if (!window)
		exit(EXIT_FAILURE);


	sfFont *font;
	font = sfFont_createFromFile("DejaVuSansMono.ttf");
	if (!font)
		exit(EXIT_FAILURE);

	sfText *text;
	text = sfText_create();
	sfText_setFont(text, font);
	sfText_setCharacterSize(text, 30);

	sfEvent event;
	while (sfRenderWindow_isOpen(window)) {
		while (sfRenderWindow_pollEvent(window, &event)) {
			if (event.type == sfEvtClosed)
				sfRenderWindow_close(window);
		}
		sfRenderWindow_clear(window, sfBlack);
		for (size_t i = 0; i < DIM; ++i) {
			for (size_t j = 0; j < DIM; ++j) {
				char str[32];
				snprintf(str, 31, "%d", m[i][j]);
				sfVector2f pos = {.x = 20 + (float)xres/DIM * i, .y = 20 + (float)xres/DIM * j};
				sfText_setPosition(text, pos);
				sfText_setString(text, str);
				sfRenderWindow_drawText(window, text, NULL);
			}
		}
		sfRenderWindow_display(window);
	}
	
	sfText_destroy(text);
	sfFont_destroy(font);
	sfRenderWindow_destroy(window);
}

void zigZagPrint(int m[DIM][DIM])
{
	for (size_t i = 0; i < DIM; ++i) {
		for (size_t j = 0; j < DIM; ++j) {
			printf("%-2d ", m[i][j]);
		}
		printf("\n");
	}
}

void zigZag()
{
	int m[DIM][DIM] = {{0}};
	int start = 0, end = DIM * DIM - 1;
	int d = -1;

	int i = 0, j = 0;

	do {
		m[i][j] = start++;
		m[DIM - i - 1][DIM - j - 1] = end--;

		i += d;
		j -= d;
		if (i < 0) {
			++i;
			d = -d;
		} else if (j < 0) {
			++j;
			d = -d;
		}
	} while (start < end);

	if (start == end) {
		m[i][j] = start;
	}

	zigZagPrint(m);
	zigZagDisplay(m);
}

int main(void)
{
	zigZag();

	return 0;
}

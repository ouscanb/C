#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

#define EPS 0.0000001

double nth_root(double x0, double A, double n)
{
    double dxn, xn = x0;

    do {
        dxn = (1 / n) * ((A / pow(xn, n - 1)) - xn);
        xn += dxn;
    } while (fabs(dxn) > EPS);

    return xn;
}

int main(int argc, char *argv[])
{
    double result, A, n;

    printf("positive real number: ");
    scanf("%lf", &A);
    assert(A > 0);

    printf("nth root: ");
    scanf("%lf", &n);
    assert(n > 0);

    result = nth_root(A / n, A, n);
    printf("Result: %lf\n", result);

    return EXIT_SUCCESS;
}

#include <stdio.h>

int cmp3(int a, int b, int c);

int main(void) {
    int a1, a2, a3;

    printf("Enter three int separated by space: ");
    scanf("%d %d %d", &a1, &a2, &a3);

    printf("Greatest number is %d\n", cmp3(a1, a2, a3));

    return 0;
}

int cmp3(int a, int b, int c) {
    int i1, i2;

    i1 = (a > b) ? a : b;
    i2 = (i1 > c) ? i1 : c;
    /* or
     * i1 = (a > b && a > c) ? a : ((b > c) ? b : c);
     */
    
    return i2;
}

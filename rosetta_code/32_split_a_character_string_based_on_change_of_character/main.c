#include <stdio.h>

#define DELIM ", "

int main()
{
    char s[] = "";
    char c = s[0], cn;
    for (size_t i = 0; s[i] != '\0'; i++) {
        printf("%c", c);
        cn = s[i + 1];
        if (cn != '\0' && c != cn)
            printf(DELIM);
        c = cn;
    }
    puts("");

    return 0;
}

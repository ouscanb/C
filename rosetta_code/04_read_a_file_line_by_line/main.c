#include <stdio.h>

int main(void)
{
    enum { BUF = 1024 };
    FILE *fp = fopen("test.txt", "r");

    char line[BUF];
    while (fgets(line, BUF, fp)) {
        printf("%s", line);
    }
    fclose(fp);
    return 0;
}

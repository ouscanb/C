#include <stdio.h>
#include <ctype.h>

#include"getch.c"

#define PRINT_ARR(ARR, LEN, F) \
    do { \
        for (size_t i = 0; i < LEN; ++i) \
        printf(F" ", ARR[i]); \
        puts(""); \
    } while (0)

int getint(int *pn);
int getfloat(double *pn);

int main(void)
{
    enum {
        buf = 10,
    };

    int arr[buf] = {0};
    /* double arr[buf] = {0.0}; */

    int ret;
    size_t i = 0;
    while (i < buf && (ret = getint(&arr[i])) != EOF) {
        if (ret == 0)
            continue;
        else
            ++i;
    }

    PRINT_ARR(arr, buf, "%d");
    /* PRINT_ARR(arr, buf, "%g"); */

    return 0;
}

int getint(int *pn)
{
    int c;
    while (isspace(c = getch()))
        ;

    if (!isdigit(c) && c != EOF && c != '-' && c != '+') {
        /* ungetch(c); */
        return 0;
    }

    int sign = (c == '-') ? -1 : 1;
    if (c == '-' || c == '+') {
        c = getch();
        if (!isdigit(c)) {
            ungetch(c);
            return 0;
        }
    }

    for (*pn = 0; isdigit(c); c = getch())
        *pn = *pn * 10 + c - '0';
    *pn *= sign;

    if (c != EOF)
        ungetch(c);

    return c;
}

int getfloat(double *pn)
{
    int c;
    while (isspace(c = getch()))
        ;

    if (!isdigit(c) && c != EOF && c != '-' && c != '+' && c != '.') {
        /* ungetch(c); */
        return 0;
    }

    double sign = (c == '-') ? -1.0 : 1.0;
    if (c == '-' || c == '+') {
        c = getch();
        if (!isdigit(c) && c != '.') {
            ungetch(c);
            return 0;
        }
    }

    for (*pn = 0.0; isdigit(c); c = getch())
        *pn = *pn * 10.0 + (c - '0');
    if (c == '.') {
        c = getch();
        double decim = 1.0;
        for (; isdigit(c); c = getch()) {
            *pn = *pn * 10.0 + (c - '0');
            decim *= 10.0;
        }
        *pn /= decim;
    }
    *pn *= sign;

    if (c != EOF)
        ungetch(c);

    return c;
}

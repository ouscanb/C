#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ARRLEN 100000

static int binsearch(int x, int arr[], size_t arrlen);
static int binsearch2(int x, int arr[], size_t arrlen);
static void rarr(int arr[], size_t arrlen);
static int cmpint(const void *p1, const void *p2);
static void print_arr(int arr[], size_t arrlen);
static void print_runtime(int (*fn)(int, int *, size_t), int x, int arr[], size_t arrlen);

int main(void)
{
    int v[ARRLEN];
    rarr(v, ARRLEN);
    int x = v[rand() % ARRLEN];
    printf("x: %d\n", x);
    qsort(v, ARRLEN, sizeof(int), cmpint);
    /* print_arr(v, ARRLEN); */

    int (*fn1)(int, int *, size_t);
    fn1 = &binsearch;
    print_runtime(fn1, x, v, ARRLEN);
    fn1 = &binsearch2;
    print_runtime(fn1, x, v, ARRLEN);

    return 0;
}

static int binsearch(int x, int arr[], size_t arrlen)
{
    printf("Running %s\n", __func__);
    size_t li = 0,
           hi = arrlen - 1,
           mi;
    while (li <= hi) {
        mi = (hi + li) / 2;
        if (x > arr[mi])
            li = mi + 1;
        else if (x < arr[mi])
            hi = mi - 1;
        else {
            printf("Found: %d\n", x);
            return mi;
        }
    }
    return -1;
}

static int binsearch2(int x, int arr[], size_t arrlen)
{
    printf("Running %s\n", __func__);
    size_t li = 0,
           hi = arrlen - 1,
           mi;
    while (li < hi) {
        mi = (hi + li) / 2;
        if (x > arr[mi])
            li = mi + 1;
        else
            hi = mi;
    }
    if (x == arr[hi]) {
        printf("Found: %d\n", x);
        return hi;
    }
    else
        return -1;
}

static void rarr(int arr[], size_t arrlen)
{
    static int c = 1;
    time_t t = time(NULL);
    srand((unsigned) t * c);
    ++c;

    for (size_t i = 0; i < arrlen; ++i)
        arr[i] = rand() % arrlen;

    return;
}

static int cmpint(const void *p1, const void *p2)
{
    return (*(int *)p1 >= *(int *)p2) ? 1 : 0;
}

static void print_arr(int arr[], size_t arrlen)
{
    for(size_t i = 0; i < arrlen; ++i)
        printf("%d ", arr[i]);
    puts("");

    return;
}

static void print_runtime(int (*fn)(int, int *, size_t), int x, int arr[], size_t arrlen)
{
    clock_t st, en;
    double cpu_time;

    st = clock();
    (*fn)(x, arr, arrlen);
    en = clock();
    cpu_time = ((double) (en - st)) / CLOCKS_PER_SEC;

    printf("It took %f seconds.\n", cpu_time);

    return;
}

#include <stdio.h>

unsigned getbits(unsigned x, size_t p, size_t n);
unsigned invert(unsigned x, size_t p, size_t n);

int main(void)
{
    unsigned n;
    scanf("%u", &n);
    printf("%u %u\n", n, invert(n, 5, 4));

    return 0;
}

unsigned getbits(unsigned x, size_t p, size_t n)
{
    return (x >> (p + 1 -n)) & ~(~0u << n);
}

unsigned invert(unsigned x, size_t p, size_t n)
{
    unsigned mask_x = ~(~0u << n) << (p + 1 - n);

    return (x & ~mask_x) | (~(x & mask_x) & mask_x);
}

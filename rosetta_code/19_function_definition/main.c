#include <stdio.h>
#include <stdlib.h>

#define MULTIPLY(X, Y) ((X) * (Y))

double multiply(double a, double b)
{
    return a * b;
}

int main(int argc, char *argv[])
{
    printf("%g\n", multiply(2, 2));
    printf("%d\n", MULTIPLY(2, 2));

    return EXIT_SUCCESS;
}

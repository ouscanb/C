#define NUMBER  '0'
#define FUNC    '1'
#define VAR     '2'

int getop(char []);
void push(double);
double pop(void);
int getch(void);
void ungetch(int);
double func(char []);
void setvar(int, double);
int getvar(int);
int checkvar(int);

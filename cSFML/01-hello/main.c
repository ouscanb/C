#include <stdio.h>
#include <stdlib.h>

#include <SFML/Graphics.h>
#include <SFML/System.h>


int main(void)  {
    sfVideoMode mode = {400, 320, 32};
    sfRenderWindow* window;
    sfTexture* texture;
    sfSprite* sprite;
    sfFont* font;
    sfText* text;
    sfEvent event;

    /* create the main window */
    /*window = sfRenderWindow_create(mode, "Hello, world!", sfResize | sfClose,  NULL);*/
    window = sfRenderWindow_create(mode, "Hello, world!", sfClose,  NULL);
    if (!window) {
        return EXIT_FAILURE;
    }

    texture = sfTexture_createFromFile("hello.png", NULL);
    if (!texture) {
        return EXIT_FAILURE;
    }

    sprite = sfSprite_create();
    sfSprite_setTexture(sprite, texture, sfTrue);
    sfVector2f tex_pos = { (400 - 380) / 2, (320 - 300) / 2 };
    sfSprite_setPosition(sprite, tex_pos);

    /* create a text to display */
    font = sfFont_createFromFile("DejaVuSans.ttf");
    if (!font) {
        return EXIT_FAILURE;
    }

    text = sfText_create();
    sfText_setString(text, "Hello, world!");
    sfText_setFont(text, font);
    sfText_setCharacterSize(text, 30);
    sfVector2f text_pos = {50, 100};   
    sfText_setPosition(text, text_pos);

    /* create a circle filled with color */
    sfCircleShape* ball;
    ball = sfCircleShape_create();
    sfCircleShape_setRadius(ball, 50);
    /*sfColor ball_col = {0, 255, 0};*/
    sfCircleShape_setFillColor(ball, sfGreen);

    /* start the clock */
    sfClock* clock = sfClock_create();
    /* enter the game loop */
    while (sfRenderWindow_isOpen(window)) {
        /* get the elapsed time */
        sfTime elapsed = sfClock_getElapsedTime(clock);
        float t = sfTime_asSeconds(elapsed);

        float g = 300.0;
        float speed_x = 250.0;
        float speed_y = 0.0;

        static sfVector2f ball_pos = {0, 0};

        /* process events */
        while (sfRenderWindow_pollEvent(window, &event)) {
            if (event.type == sfEvtClosed) {
                sfRenderWindow_close(window);
            }
        }

        /* clear the screen, draw the sprite and the text */
        sfRenderWindow_clear(window, sfBlack);
        sfRenderWindow_drawSprite(window, sprite, NULL);
        sfRenderWindow_drawText(window, text, NULL);
        
        ball_pos.x = speed_x * t;
        ball_pos.y = (speed_y + g * t) * t;

        sfCircleShape_setPosition(ball, ball_pos);
        sfRenderWindow_drawCircleShape(window, ball, NULL);

        /* update the window */
        sfRenderWindow_display(window);
    }

    /* cleanup resources */
    sfText_destroy(text);
    sfFont_destroy(font);
    sfSprite_destroy(sprite);
    sfTexture_destroy(texture);
    sfCircleShape_destroy(ball);
    sfClock_destroy(clock);
    sfRenderWindow_destroy(window);

    return EXIT_SUCCESS;
}

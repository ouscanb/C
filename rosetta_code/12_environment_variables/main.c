#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    const char *env_var = (argc > 1) ? argv[1] : "PATH";

    char *env_val;
    if ((env_val = getenv(env_var)) == NULL)
        exit(EXIT_FAILURE);

    puts(env_val);

    return EXIT_SUCCESS;
}

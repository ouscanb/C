#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define BUF 12

int any(char s1[], char s2[]);
size_t getline(char s[], size_t slen);

int main(void)
{
    char s1[BUF];
    char s2[BUF];
    fgets(s1, BUF, stdin);
    if (!strchr(s1, '\n')) {
        int c;
        while ((c = fgetc(stdin)) != '\n' && c != EOF)
            ;
    }
    fgets(s2, BUF, stdin);
    int ret = any(s1, s2);
    if (ret == -1)
        printf("No characters found.\n");

    return 0;
}

int any(char s1[], char s2[])
{
    size_t i;
    for (i = 0; s2[i] != '\0'; ++i) {
        size_t j;
        for (j = 0; s1[j] != '\0'; ++j) {
            if (s1[j] == s2[i] && isalnum(s1[j])) {
                printf("first occurance of char %c is at index %zu\n", s1[j], j);
                return j;
            }
        }
    }

    return -1;
}

size_t getline(char s[], size_t slen)
{
    int c;
    size_t i;
    for (i = 0; i < slen - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
        s[i] = c;
    /* do not store newline */
    /* if (c == '\n') */
    /*     s[i++] = c; */
    s[i] = '\0';

    return i;
}


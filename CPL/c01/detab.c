#include <stdio.h>

#define BUF 1024
#define TAB 8

int detab(char s[], int slen)
{
    int c, nc, i;
    for (i = 0, nc = 0; i < slen - 1 && (c = getchar()) != EOF && c != '\n';) {
        ++nc;
        if (c == '\t') {
            int ns = TAB - (nc % TAB) + 1;
            if (ns == 0) {
                ++i;
                --nc;
            }
            nc += ns - 1;
            while (ns && i < slen - 1 && nc != 1) {
                s[i++] = ' ';
                --ns;
            }
        } else {
            s[i++] = c;
        }
    }
    if (c == '\n') {
        s[i] = c;
        ++i;
    }
    s[i] = '\0';

    return i;
}

void print_tabstop(int tablen, int nt)
{
    int i, j, nc = 0;
    for (i = 0; i < nt; ++i) {
        for(j = 0; j < tablen; ++j) {
            ++nc;
            if (nc % tablen == 0)
                putchar('|');
            else
                putchar(' ');
        }
    }
}

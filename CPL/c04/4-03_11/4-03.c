#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <string.h>

#define BUFOP   128
#define NUMBER  '0'
#define FUNC    '1'
#define VAR     '2'

int getop(char []);
void push(double);
double pop(void);
double func(char []);
void setvar(int, double);
int getvar(int);
int checkvar(int);

double vars[26] = {0.0};
int vars_def[26] = {0};
int var = 0;

int main(void)
{
    int optype;
    double op2;
    char s[BUFOP];

    while ((optype = getop(s)) != EOF) {
        switch (optype) {
            case NUMBER:
                push(atof(s));
                break;
            case FUNC:
                push(func(s));
                break;
            case VAR:
                var = s[0];
                if (checkvar(var))
                    push(getvar(var));
                break;
            case '=':
                op2 = pop();
                setvar(var, op2);
                push(op2);
                break;
            case '+':
                push(pop() + pop());
                break;
            case '*':
                push(pop() * pop());
                break;
            case '-':
                op2 = pop();
                push(pop() - op2);
                break;
            case '/':
                op2 = pop();
                if(op2 == 0.0) {
                    fprintf(stderr, "Division by zero!\n");
                    exit(EXIT_FAILURE);
                }
                push(pop() / op2);
                break;
            case '%':
                op2 = pop();
                if(op2 == 0.0) {
                    fprintf(stderr, "Division by zero!\n");
                    exit(EXIT_FAILURE);
                }
                push(fmod(pop(), op2));
                break;
            case '\n':
                printf("\t%.8g\n", pop());
                break;
            default:
                fprintf(stderr, "Unknown command %s\n", s);
                exit(EXIT_FAILURE);
                break;
        }
    }

    return EXIT_SUCCESS;
}

#define BUFSVAL 128

size_t spos = 0;        /* stack position */
double sval[BUFSVAL];   /* value stack*/

void push(double f)
{
    if (spos < BUFSVAL)
        sval[spos++] = f;
    else
        fprintf(stderr, "Stack is full! Can' t push %g \n", f);
}

double pop(void)
{
    if (spos > 0)
        return sval[--spos];
    else {
        fprintf(stderr, "Stack is empty!\n");
        return 0.0;
    }
}

int getch(void);
void ungetch(int);

int getop(char s[])
{
    int i, c;

    while ((s[0] = c = getch()) == ' ' || c == '\t')
        ;
    s[1] = '\0';

    i = 0;
    if (isalpha(c)) {
        while (isalpha(s[i++] = c))
            c = getch();
        s[i - 1] = '\0';
        if (c != EOF)
            ungetch(c);
        if (strlen(s) == 1)
            return VAR;
        else
            return FUNC;
    }

    if (!isdigit(c) && c != '.' && c != '-')
        return c;

    if (c == '-') {
        int next = getch();
        if (!isdigit(next) && next != '.')
            return c;
        c = next;
    } else
        c = getch();

    while (isdigit(s[++i] = c))
        c = getch();
    if (c == '.')
        while (isdigit(s[++i] = c = getch()))
            ;
    s[i] = '\0';

    if (c != EOF)
        ungetch(c);

    return NUMBER;
}

#define BUFSIZE 128

char buf[BUFSIZE];  /* buffer for ungetch */
int bufpos = 0;     /* next free position in buf */

int getch(void )
{
    return (bufpos > 0) ? buf[--bufpos] : getchar();
}

void ungetch(int c)
{
    if (bufpos >= BUFSIZE)
        fprintf(stderr, "ungetch: too many characters!\n");
    else
        buf[bufpos++] = c;
}

double func(char s[])
{
    double op2;
    if(strcmp(s, "sin") == 0)
        return sin(pop());
    else if (strcmp(s, "exp") == 0)
        return exp(pop());
    else if (strcmp(s, "pow") == 0) {
        op2 = pop();
        return pow(pop(), op2);
    } else
        fprintf(stderr, "Function %s is not supported!\n", s);

    return 0.0;
}

void setvar(int var, double f)
{
    size_t id = tolower(var) - 'a';
    vars[id] = f;
    vars_def[id] = 1;
}

int getvar(int var)
{
    size_t id = tolower(var) - 'a';
    return vars[id];
}

int checkvar(int var)
{
    size_t id = tolower(var) - 'a';
    return vars_def[id];
}

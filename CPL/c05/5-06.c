#include <stdio.h>
#include <string.h>
#include <sys/types.h> /* ssize_t */

size_t getline(char *, size_t);
ssize_t strindex(char *, char *);

int main(void)
{
    enum {
        BUF = 12
    };

    char buf[BUF];
    char pat[] = "this";
    size_t len;
    while ((len = getline(buf, BUF)) > 0) {
        if (!strchr(buf, '\n')) {
            int c;
            while ((c = getchar()) != EOF && c != '\n')
                ;
            printf("%s\n", buf);
        } else
            printf("%s", buf);
        ssize_t pos;
        if ((pos = strindex(buf, pat)) != -1)
            printf("Pattern '%s' at index %zd\n", pat, pos);
    }

    return 0;
}

size_t getline(char *line, size_t len)
{
    int c;
    char *p = line;
    /* for (size_t i = 0; i < len - 1 && (c = getchar()) != EOF && c != '\n'; ++i) */
    /*     *line++ = c; */
    while (--len && (c = getchar()) != EOF && c != '\n')
        *line++ = c;
    if (c == '\n')
        *line++ = c;
    *line = '\0';

    return line - p;
}

ssize_t strindex(char *line, char *pattern)
{
    char *i;
    for (i = line; *i != '\0'; ++i) {
        char *j, *k;
        for (j = i, k = pattern;  *j == *k && *k != '\0'; ++j, ++k)
            ;
        if (k > pattern && *k == '\0')
            return i - line;
    }

    return -1;
}

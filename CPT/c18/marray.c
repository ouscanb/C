#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void madd(int m, int n, double arr1[][n], double arr2[][n], double arr3[][n]);
void mdisp(int m, int n, double arr[][n]);
void mrarr(int m, int n, double arr[][n]);
void mmult(int m, int n, int p, double arr1[][n], double arr2[][p], double arr3[][p]);

int main(void) {
    double arr01[5][3];
    double arr02[5][3];
    double arr12[5][3];
    double arr04[3][5];
    double arr14[5][5];
    
    mrarr(5, 3, arr01);
    mrarr(5, 3, arr02);
    mrarr(3, 5, arr04);

    printf("\nA =\n");
    mdisp(5, 3, arr01);
    
    printf("\nB =\n");
    mdisp(5, 3, arr02);

    printf("\nC =\n");
    mdisp(3, 5, arr04);

    printf("\nA + B =\n");
    madd(5, 3, arr01, arr02, arr12);

    printf("\nA * C=\n");
    mmult(5, 3, 5, arr01, arr04, arr14);




    return 0;
}

void madd(int m, int n, double arr1[][n], double arr2[][n], double arr3[][n]) {
    int i, j;

    for (i = 0; i < m; i++) {
        for (j = 0; j < n; j++) { 
            arr3[i][j] = arr1[i][j] + arr2[i][j];
        }
    }
    mdisp(m, n, arr3);

    return;
}

void mdisp(int m, int n, double arr[][n]) {
    int i, j;

    for (i = 0; i < m; i++) {
        printf("\t");
        for (j = 0; j < n; j++) {
            printf("%7.1f ", arr[i][j]);
        }
        printf("\n");
    }

    return;
}

void mrarr(int m, int n, double arr[][n]) {
    int i, j;
    static int t = 1;
    time_t s;
    
    s = time(NULL);
    srand((unsigned) s * t);

    for (i = 0; i < m; i++) {
        for (j = 0; j < n; j++) {
            arr[i][j] = rand() % 10 + (rand() % 10) / 15.0;
        }
    }
    t += 1;

    return;
}

void mmult(int m, int n, int p, double arr1[][n], double arr2[][p], double arr3[][p]) {
    int i, j, k;

    for (i = 0; i < m; i++) {
        for (k = 0; k < p; k++) {
            arr3[i][k] = 0;
            for (j = 0; j < n; j++) {
                arr3[i][k] += arr1[i][j] * arr2[j][k];
            }
        }
    }
    mdisp(m, p, arr3);

    return;
}


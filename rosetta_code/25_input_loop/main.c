#include <stdio.h>
#include <stdlib.h>

#define BUF 128

static char *getline(FILE *fp)
{
    int c;
    size_t cc = 0;    /* character count */
    size_t r_inc = 1; /* realloc increment counter */

    char *line = malloc(sizeof *line * BUF);
    if (!line)
        return NULL;

    while ((c = fgetc(fp)) != EOF) {
        line[cc++] = c;
        if (c == '\n')
            break;
        if (cc + 1 >= BUF) {
            line = realloc(line, sizeof *line * BUF * ++r_inc);
        }
    }
    if (cc == 0) {
        free(line);
        return NULL;
    }
    line[cc] = '\0';

    return line;
}

int main(int argc, char *argv[])
{
    char *line;
    while ((line = getline(stdin))) {
        printf("%s", line);
        free(line);
    }

    return EXIT_SUCCESS;
}

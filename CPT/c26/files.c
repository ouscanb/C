#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct vector {
    double x;
    double y;
    double z;
} vector;

int main(void) {
    
    FILE *fp;
    int i;
    char buf[100];

    fp = fopen("numbers.txt", "w");
    if (!fp) {
        fprintf(stderr, "Error: File cannot be created!\n");

        exit(EXIT_FAILURE);
    }

    for (i = 0; i < 10; i++) {
        fprintf(fp, "%d\n", i);
    }
    
    fclose(fp);

    fp = fopen("numbers.txt", "r");
    if (!fp) {
        fprintf(stderr, "Error: File cannot be read!\n");

        exit(EXIT_FAILURE);
    }

    while (fgets(buf, 100, fp) != NULL) {
        printf("%s", buf);
    }

    fseek(fp, 0, SEEK_SET);

    char ch;
    while (1) {
        ch = fgetc(fp);
        if (feof(fp)) break;

        printf("%c", ch);
    }

    fclose(fp);

    fp = fopen("ascii.txt", "w");
    if (!fp) {
        fprintf(stderr, "Error: File cannot be opened!\n");

        exit(EXIT_FAILURE);
    }

    for (i = 0; i < 256; i++) {
        fprintf(fp, "%c\t%d\n", i, i);
    }

    fclose(fp);

    fp = fopen("binary.bin", "wb");
    if (!fp) {
        fprintf(stderr, "Error: File cannot be opened!\n");

        exit(EXIT_FAILURE);
    }

    vector vec;
    for (i = 0; i < 10; i++) {
        vec.x = i * 1.0;
        vec.y = i * 1.2;
        vec.z = i * 1.4;
        fwrite(&vec, sizeof(vector), 1, fp);
    }

    fclose(fp);

    fp = fopen("binary.bin", "rb");
    if (!fp) {
        fprintf(stderr, "Error: File cannot be opened!\n");

        exit(EXIT_FAILURE);
    }

    vector nvec;
    for (i = 0; i < 10; i++) {
        fread(&nvec, sizeof(vector), 1, fp);
        printf("\nvec.x = %f\nvec.y = %f\nvec.z = %f\n",
                nvec.x, nvec.y, nvec.z);
    }

    fseek(fp, 1 * sizeof(vector), SEEK_SET);
    fread(&nvec, sizeof(vector), 1, fp);
    printf("\nvec.x = %f\nvec.y = %f\nvec.z = %f\n",
            nvec.x, nvec.y, nvec.z);
 
    fclose(fp);

    return 0;
}


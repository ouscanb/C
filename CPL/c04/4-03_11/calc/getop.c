#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "calc.h"

int getop(char s[])
{
    static int ungetc = ' ';
    int i, c = ungetc;
    ungetc = ' ';

    while ((s[0] = c) == ' ' || c == '\t')
         c = getch();
    s[1] = '\0';

    i = 0;
    if (isalpha(c)) {
        while (isalpha(s[i++] = c))
            c = getch();
        s[i - 1] = '\0';
        if (c != EOF)
            ungetc = c;
        if (strlen(s) == 1)
            return VAR;
        else
            return FUNC;
    }

    if (!isdigit(c) && c != '.' && c != '-')
        return c;

    if (c == '-') {
        int next = getch();
        if (!isdigit(next) && next != '.')
            return c;
        c = next;
    } else
        c = getch();

    while (isdigit(s[++i] = c))
        c = getch();
    if (c == '.')
        while (isdigit(s[++i] = c = getch()))
            ;
    s[i] = '\0';

    if (c != EOF)
        ungetc = c;

    return NUMBER;
}

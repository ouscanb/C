#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DSTRL 100
#define DBLUL 200

void disp(int *arr, int arrl);

int main(void) {
    char name[DSTRL];
    char *blurb;

    strncpy(name, "The C Programming Language", DSTRL);

    /* allocate memory for the blurb */
    blurb = malloc(DBLUL * sizeof(char));
    if (blurb == NULL) {
        fprintf(stderr, "Error: Unable to allocate memory!\n");
        return -1;
    } else {
        strncpy(blurb,
"The C Programming Language by K&R helps readers keep up \n\
with the finalized ANSI standard  for C. The C Programming \n\
Language by K&R helps readers keep up with the finalized \n\
ANSI standard  for C. The C Programming Language by K&R \n\
helps readers keep up with the finalized ANSI standard for C.", DBLUL);
    }

    printf("Name of the book:\n\t%s\n", name);
    printf("Synopsis of the book:\n\t%s\n", blurb);

    free(blurb);

    printf("\n\n");

    int i, n;
    int *nums;

    printf("Number of integer elements to be entered: ");
    scanf("%d", &n);
    printf("Enter the integers:\n");

    nums = malloc(n * sizeof(int));
    for (i = 0; i < n; i++) {
        scanf("%d", &nums[i]);
    }

    printf("You have entered:\n");
    disp(nums, n);

    printf("\n");

    return 0;
}

void disp(int *arr, int arrl) {
    int i;

    for (i = 0; i < arrl; i++) {
        printf("%d ", arr[i]);
    }

    return;
}


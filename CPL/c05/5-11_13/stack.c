#include <stdio.h>
#include <stdlib.h>

#define STACKSIZE 128

static double stack[STACKSIZE] = {0};
static double *pstack = stack;

void push(double num)
{
    if (pstack < stack + STACKSIZE)
        *pstack++ = num;
    else {
        fprintf(stderr, "%s failed: Stack is full!\n", __func__);
        exit(EXIT_FAILURE);
    }
}

double pop(void)
{
    if (pstack > stack) {
        return *--pstack;
    } else {
        fprintf(stderr, "%s failed: Stack is empty!\n", __func__);
        exit(EXIT_FAILURE);
    }
}

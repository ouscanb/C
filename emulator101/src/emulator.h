#ifndef _EMULATOR_H
#define _EMULATOR_H

#include <bits/stdint-uintn.h>
#include <bits/types/time_t.h>
#include <stdint.h>

typedef struct ConditionCodes {
	uint8_t		z	:1;
	uint8_t		s	:1;
	uint8_t		p	:1;
	uint8_t		cy	:1;
	uint8_t		ac	:1;
	uint8_t		pad	:1;
} ConditionCodes;

typedef struct State8080 {
	uint8_t		a;
	uint8_t		b;
	uint8_t		c;
	uint8_t		d;
	uint8_t		e;
	uint8_t		h;
	uint8_t		l;
	uint16_t	sp;
	uint16_t	pc;
	uint8_t		*memory;
	uint16_t	interupt_vector;
	uint8_t		interupt_enabled;
	uint8_t		halted;
	float		last_interupt;
	uint8_t		(*port_in)(struct State8080 *state, uint8_t port);
	uint8_t		(*port_out)(struct State8080 *state, uint8_t port);
	ConditionCodes	cc;
} State8080;

uint8_t Emulate8080(State8080 *state);
void GenerateInterrupt8080(State8080 *state, int interupt_num);

#endif // _EMULATOR_H

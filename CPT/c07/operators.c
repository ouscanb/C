#define _GNU_SOURCE

#include <stdio.h>
#include <math.h>

int trunc_me(double k);

int main(void) {
    int x = 1;
    printf("Set x = 1\n");
    printf("Increment before (++x -> x): %d -> %d.\n", ++x, x);
    x = 1;
    printf("Increment after  (x++ -> x): %d -> %d.\n", x++, x);
    
    x =1;
    while (x < 30) {
        printf("%d, ", x++);
    }
    printf("\n");
    x =1;
    while (x < 30) {
        printf("%d, ", ++x);
    }
    printf("\n");

    double pi = M_PI;
    printf("\nTruncated value of pi is %d.\n", trunc_me(pi));
    
    /* logical && and || take precedence from right to left */
    if (pow(pi, 200000) && 0) {
        printf("BOOM!\n");
    } else {
        printf("OK.\n");
    }

    return 0;
}

int trunc_me(double k) {
    if (k >= 0) {
        return (int) floor(k);
    } else {
        return (int) ceil(k);
    }
}

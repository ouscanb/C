#include <stdio.h>
#include <string.h>

int strend(char *,  char *);
int strend_alt(char *,  char *);

int main(void)
{
    char s1[] = "Silver fox";
    char s2[] = "fox";
    char s3[] = "ver fox";
    char s4[] = "Silver";
    if (strend(s1, s1))
        printf("%s ends with %s\n", s1, s1);
    if (strend(s1, s2))
        printf("%s ends with %s\n", s1, s2);
    if (strend(s1, s3))
        printf("%s ends with %s\n", s1, s3);
    if (strend(s1, s4))
        printf("%s ends with %s\n", s1, s4);
    else
        printf("%s does not end with %s\n", s1, s4);

    if (strend_alt(s1, s1))
        printf("%s ends with %s\n", s1, s1);
    if (strend_alt(s1, s2))
        printf("%s ends with %s\n", s1, s2);
    if (strend_alt(s1, s3))
        printf("%s ends with %s\n", s1, s3);
    if (strend_alt(s1, s4))
        printf("%s ends with %s\n", s1, s4);
    else
        printf("%s does not end with %s\n", s1, s4);

    return 0;
}

int strend(char *s,  char *t)
{
    while (*s) {
        char *pt = t;
        char *ps = s;
        if (*s != *t) {
            ++s;
            continue;
        }
        for (; *s == *t; ++s, ++t)
            if (*s == '\0')
                return (pt == ps) ? 0 : 1;
                /* return 1; */
        t = pt;
    }
    return 0;
}

int strend_alt(char *s, char *t)
{
    size_t slen = strlen(s);
    size_t tlen = strlen(t);
    if (slen < tlen)
        return 0;

    s += slen - tlen;
    for (; *s == *t; ++s, ++t)
        if (*s == '\0')
            return 1;

    return 0;
}

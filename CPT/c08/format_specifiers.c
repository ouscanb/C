#include <stdio.h>

int main(void) {
    printf("%d\n", 12345);
    printf("%3d\n", 12345);
    printf("%.3d\n", 12345);
    printf("%+d\n", 12345);
    printf("%6d\n", 12345);
    printf("%7d|%+-7d|\n", 12345, 12345);
    printf("%.6d\n", 12345);
    printf("%06d\n", 12345);
    printf("%f\n", (float) 12345);
    printf("%.3f\n", (float) 12345);
    printf("%10.2f|%10.3f|\n", (float) 12345, (float) 12345);
    printf("%-10.2f|%-10.3f|\n", (float) 12.345, (float) 12.345);
    printf("%e\n", (float) 12345);
    printf("%.2e\n", (float) 12345);
    printf("%g\n", (float) 12345);
    printf("%x\n", 123456);
    printf("%#.10x\n", 123456);
    printf("%s\n", "What a stupid program!");
    printf("%22s\n", "What a stupid program!");
    printf("%20s\n", "What a stupid program!");
    printf("|%15.10s|\n", "What a stupid program!");
    printf("|%-15.10s|\n", "What a stupid program!");

    int x = 1;
    printf("Address of variable x is %#x\n", &x);

    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#define ARRLEN(X)   ((sizeof(X)) / (sizeof(X[0])))
#define SWAP(T,X,Y)    \
    do {               \
        T SWAP_C;      \
        SWAP_C = (X);  \
        (X) = (Y);     \
        (Y) = SWAP_C;  \
    } while (0)

char *itoa(long n, char s[], size_t len);
char *reverse(char s[]);
char *itoa_recursive(long n, char s[], size_t len);
char *reverse_recursive(char s[]);
char *reverse_recursive_main(char s[], size_t i, size_t j);

int main(void)
{
    long num = LONG_MIN;
    char snum[32] = {0};
    size_t slen = ARRLEN(snum);
    printf("%ld -> %s\n", num, itoa(num, snum, slen));
    printf("%ld -> %s\n", num, itoa_recursive(num, snum, slen));
    char rs[] = "some text";
    printf("%s\n", rs);
    printf("%s\n", reverse(rs));
    printf("%s\n", reverse_recursive(rs));

    return 0;
}

char *itoa(long n, char s[], size_t len)
{
    if (len < 2)
        return NULL;

    long sign = n;

    size_t i = 0;
    do {
        s[i++] = abs(n % 10) + '0';
    } while ((n /= 10) && i < len - 2);

    /* check whether the conversion is completed */
    if (n) {
        s[--i] = '\0';
        fprintf(stderr, "ERR: %s failed conversion.\n", __func__);
        return NULL;
    }

    if (sign < 0) {
        if (i + 1 < len)
            s[i++] = '-';
        else {
            s[i] = '\0';
            fprintf(stderr, "ERR: %s failed conversion.\n", __func__);
            return NULL;
        }
    }

    s[i] = '\0';

    return reverse(s);
}

char *reverse(char s[])
{
    for (size_t i = 0, j = strlen(s) - 1; i < j; ++i, --j) {
        /* int c = s[i]; */
        /* s[i] = s[j]; */
        /* s[j] = c; */
        SWAP(int, s[i], s[j]);
    }

    return s;
}

char *reverse_recursive(char s[])
{
    return reverse_recursive_main(s, 0, strlen(s) - 1);
}

char *reverse_recursive_main(char s[], size_t i, size_t j)
{
    while (i++ < j--) {
        reverse_recursive_main(s, i, j);
    }
    int c = s[i];
    s[i] = s[j];
    s[j] = c;

    return s;
}

char *itoa_recursive(long n, char s[], size_t len)
{
    if (len < 2)
        return NULL;

    static size_t k = 0;
    static size_t failonce = 1;

    long sign = n;
    if (sign < 0 && k == 0) {
        if (len > 2) {
            s[0] = '-';
            ++k;
        } else
            return NULL;
    }

    if (n / 10 && k < len - 1)
        itoa_recursive(n / 10, s, len);
    if (k + 1 < len)
        s[k++] = abs(n % 10) + '0';
    else {
        s[0] = '\0';
    }

    s[k] = '\0';

    if (s[0] =='\0') {
        if (failonce) {
            fprintf(stderr, "ERR: __func__ failed conversion.\n");
            failonce = 0;
        }
        return NULL;
    } else
        return s;
}

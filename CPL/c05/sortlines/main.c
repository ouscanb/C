#include <stdio.h>
#include <sys/types.h>
#include "utils.h"
#include "myqsort.h"

#define MAXLINES 4096

int main(void)
{
    char *lineptr[MAXLINES];
    size_t nlines;
    if ((nlines = readlines(lineptr, MAXLINES)) >= 0) {
        myqsort(lineptr, 0, nlines - 1);
        writelines(lineptr, nlines);
        return 0;
    } else {
        fprintf(stderr, "error: input too big to sort!\n");
        return 1;
    }
}

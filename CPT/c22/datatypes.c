#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define ALEN(ARR) sizeof(ARR) / sizeof(ARR[0])
#define DSTRL 20

struct student {
    char *name;
    int id;
    char *department;
};

struct coordinates {
    int x;
    int y;
};

char *strlwr(char *str);
char *rmnl(char *str);
char *setname(struct student *stu, char *name);

int main(void) {
    
    struct student stu01, *p_stu01;

    p_stu01 = &stu01;

    printf("Type in the student's name: ");
    //fgets(stu01.name, DSTRL, stdin);
    //rmnl(stu01.name);
    char name[DSTRL];
    fgets(name, DSTRL, stdin);
    rmnl(name);
    setname(&stu01, name);
    setname(&stu01, "John");
    
    printf("Type in the student's id number: ");
    scanf("%d", &stu01.id);
    getchar();

    printf("Type in the student's department: ");
    fgets(stu01.department, DSTRL, stdin);
    rmnl(stu01.department);

    /*
    int len = ALEN(mems);
    printf("\nLength is %d\n", len);
    */
    char *mems[] = {"Name", "Student ID", "Department"};

    printf("Student's;\n");
    /*
    printf("\t%s is %s,\n", mems[0], stu01.name);
    printf("\t%s is %d,\n", mems[1], stu01.id);
    printf("\t%s is %s.", mems[2], strlwr(stu01.department));
    */
    printf("\t%s is %s,\n", mems[0], p_stu01->name);
    printf("\t%s is %d,\n", mems[1], p_stu01->id);
    printf("\t%s is %s.", mems[2], strlwr(p_stu01->department));
    
    printf("\n\n");

    struct coordinates arr[4];
    
    int i;
    for (i = 0; i < 4; i++) {
        printf("Enter the coordinates of corner %d: \n", (i + 1));
        scanf("%d %d", &arr[i].x, &arr[i].y);
    }
    for (i = 0; i < 4; i++) {
        printf("(%d, %d) ", arr[i].x, arr[i].y);
    }

    printf("\n");

    return 0;
}

char *strlwr(char *str) {
    char *p;
    
    p = str;
    while (*p) {
        *p = tolower(*p);
        p++;
    }

    return str;
}

char *rmnl(char *str) {
    int len = strlen(str);

    if (len > 0 && str[len - 1] == '\n') {
        str[len - 1] = '\0';
    }

    return str;
}

char *setname(struct student *stu, char *name) {
    //(*stu).name = name;
    stu->name = name;

    return stu->name;
}

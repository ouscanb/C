#include <stdio.h>
#include <stdlib.h>

#define DIM 10

void bt(void)
{
	int m[DIM][DIM] = {0};
	int last = 1;

	for (size_t i = 0; i < DIM; ++i) {
		size_t j;
		for (j = 0; j <= i; ++j) {
			if (i == 0) {
				m[0][0] = last;
				printf("%-10d ", last);
				continue;
			}
			if (j == 0) {
				m[i][j] = last;
				printf("%-10d ", last);
				continue;
			}
			m[i][j] = m[i][j - 1] + m[i - 1][j - 1];
			printf("%-10d ", m[i][j]);
		}
		last = m[i][j - 1];
		printf("\n");
	}
}

int main(void)
{
	bt();

	return EXIT_SUCCESS;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

typedef struct token {
    char *str;
    struct token *next;
}token_t;

char *dups(const char *str)
{
    char *d = malloc(strlen(str) + 1);
    if (!d) return NULL;
    strcpy(d, str);

    return d;
}

token_t *new_token(char *t_str)
{
    token_t *t_new = malloc(sizeof(token_t));
    assert(t_new);

    t_new->str = dups(t_str);
    t_new->next = NULL;

    return t_new;
}

token_t *add_token(token_t **t_list, token_t *t)
{
    if (!*t_list) {
        *t_list = t;

        return *t_list;
    }

    token_t *cursor = *t_list;
    while (cursor->next)
        cursor = cursor->next;
    cursor->next = t;

    return *t_list;
}

token_t *tokenize(token_t **t_list, const char *str, const char *delim)
{
    char *tmp_str = dups(str); 
    char *t = strtok(tmp_str, delim);
    while (t) {
        add_token(t_list, new_token(t));
        t = strtok(NULL, delim);
    }
    free(tmp_str);

    return *t_list;
}

void print_tokens(token_t *t_list)
{
    token_t *cursor = t_list;
    while (cursor) {
        printf("%s ", cursor->str);
        cursor = cursor->next;
    }

    return;
}

void free_list(token_t **t_list)
{
    if (*t_list == NULL)
        return;

    while (*t_list) {
        token_t *tmp = *t_list;
        *t_list = (*t_list)->next;
        tmp->next = NULL;
        free(tmp->str);
        free(tmp);
    }

    return;
}

int main(int argc, char *argv[])
{
    if (argc != 3) {
        puts("Usage: main [STR] [DELIM]");
        exit(EXIT_FAILURE);
    }

    token_t *token_list = NULL;

    tokenize(&token_list, argv[1], argv[2]);

    print_tokens(token_list);
    free_list(&token_list);

    return EXIT_SUCCESS;
}

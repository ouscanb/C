#include <stdio.h>
#include <stdlib.h>

#include "hash_table.h"

int main(void)
{
	const char *keys[] = {
		"first", "second", "third", NULL
	};
	const char *vals[] = {
		"1st", "2nd", "3rd"
	};

	ht_hash_table *ht = ht_new();
	for (size_t i = 0; keys[i] != NULL; ++i) {
		ht_insert(ht, keys[i], vals[i]);
	}

	printf("key: %-10s value: %s\n", "first", ht_search(ht, "first")->value);
	printf("key: %-10s value: %s\n", "second", ht_search(ht, "second")->value);
	printf("key: %-10s value: %s\n", "third", ht_search(ht, "third")->value);

	ht_del_hash_table(ht);

	return EXIT_SUCCESS;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

void four_is_magic(char *num);
char *numtostr(int num);
void read(char *num);
void read_xx(char *num);

static int ccount = 0;
const char *snum0[] = {
    "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
    "ten", "eleven", "twelve", "thirteen", "fourteen",
    "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"
};
const char *snum1[] = {
    "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"
};
const char *snum2[] = {
    "hundred", "thousand", "million", "billion", "trillion"
};

int main(int argc, char *argv[])
{
    if (argc != 2) {
        printf("Usage: %s [%s]\n", argv[0], "number");

        exit(EXIT_FAILURE);
    }

    char *sn = argv[1];
    int snlen = strlen(sn);
    printf("sn: %s\n", sn);
    printf("snlen: %d\n", snlen);
    long long n = strtoll(sn, NULL, 10);

    if (errno == ERANGE || n == '\0') {
        char *errmsg = n ? strerror(errno) : "Not a valid integer";
        fprintf(stderr, "Error: %s.\n", errmsg);

        exit(EXIT_FAILURE);
    }

    four_is_magic(sn);
    printf("%d\n", ccount);

    return 0;
}

void four_is_magic(char *num)
{
    int nlen = strlen(num);


    do {
        if (nlen == 4) {
            read(numtostr(4));
            printf ("is ");
            read(numtostr(ccount));
            printf(".\n");
            break;
        }
        read(num);
    } while(strlen(num) != 4);

    return;
}

char *numtostr(int num)
{
    static char n[32];
    snprintf(n, 32, "%d", num);

    return n;
}

void read(char *num)
{
    char *pnum = num;
    if (*pnum == '-'){
        printf("negative ");
        ccount += strlen("negative ");
        pnum++;
    }
    while (*pnum == '0') pnum++;

    while (*pnum) {
        int dxxx = strlen(pnum) / 3;
        int xx = strlen(pnum) % 3;

        if (dxxx >= 1) {
            char tmp[4];
            int inc = xx ? xx : 3;
            strncpy(tmp, pnum, inc);
            tmp[inc] = '\0';
            pnum += inc;
            read_xx(tmp);
            if (!*pnum)
                break;
        } else {
            read_xx(pnum);
            break;
        }
        printf("%s ", snum2[xx ? dxxx : dxxx - 1]);
        ccount += strlen(snum2[xx ? dxxx : dxxx - 1]) + 1;
    }

    return;
}

void read_xx(char *num)
{
    char *pnum = num;
    while (*pnum == '0') pnum++;

    int d = atoi(pnum);
    if (!*pnum && d == 0)
        ;
    else if (d < 20) {
        printf("%s ", snum0[d]);
        ccount += strlen(snum0[d]) + 1;
    }
    else if (d < 100) {
        printf("%s ", snum1[(int) (*pnum - '0') - 2]);
        ccount += strlen(snum1[(int) (*pnum - '0') - 2]) + 1;
        read_xx(++pnum);
    } else if (d < 1000) {
        printf("%s %s ", snum0[(int) (*pnum - '0')], snum2[0]);
        ccount += strlen(snum0[(int) (*pnum - '0')]) + strlen(snum2[0]) + 2;
        read_xx(++pnum);
    }

    return;
}

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

#define ARRLEN(ARR) ((sizeof(ARR)) / (sizeof(ARR[0])))

#define PNAME 32
#define ROLLS 4

typedef struct {
    char name[PNAME];
    int strength;
    int dexterity;
    int constitution;
    int intelligence;
    int wisdom;
    int charisma;
    int attrs_sum;
} player_t;

int arrmin(int arr[], size_t len)
{
    int min = arr[0];
    for (size_t i = 0; i < len; i++)
        min = arr[i] < min ? arr[i] : min;

    return min;
}

int dice_roll()
{
    int sum = 0;
    int rolls[ROLLS];

    static unsigned s = 1;
    time_t t = time(NULL);
    srand((unsigned) t * s++);

    for (size_t i = 0; i < ROLLS; i++) {
        rolls[i] = rand() % 6 + 1;
        sum += rolls[i];
    }

    return sum - arrmin(rolls, ROLLS);
}

void attrs_set_name(player_t *p)
{
    printf("Enter name: ");
    fgets(p->name, PNAME, stdin);
    /* size_t len = strlen(p->name); */
    /* if (len > 0 && (p->name)[len - 1] == '\n') */
    /*     (p->name)[--len] = '\0'; */
    p->name[strcspn(p->name, "\n")] = '\0';

    return;
}

player_t *attrs_init()
{
    player_t *new = malloc(sizeof(player_t));
    if (!new) {
        perror(NULL);
        exit(EXIT_FAILURE);
    }

    attrs_set_name(new);
    new->strength = 0;
    new->dexterity = 0;
    new->constitution = 0;
    new->intelligence = 0;
    new->wisdom = 0;
    new->charisma = 0;
    new->attrs_sum = 0;

    return new;
}

player_t *attrs_generate(player_t *p)
{
    p->strength = dice_roll();
    p->dexterity = dice_roll();
    p->constitution = dice_roll();
    p->intelligence = dice_roll();
    p->wisdom = dice_roll();
    p->charisma = dice_roll();
    p->attrs_sum =
        p->strength +
        p->dexterity +
        p->constitution +
        p->intelligence +
        p->wisdom +
        p->charisma;

    return p;
}

bool attrs_check(player_t *p)
{
    int attrs[] = {
        p->strength,
        p->dexterity,
        p->constitution,
        p->intelligence,
        p->wisdom,
        p->charisma
    };
    int attrs_min_c = 0; /* Count of the attrs having min 15  */
    for (int i = 0; i < ARRLEN(attrs); i++)
        if (attrs[i] > 15)
            attrs_min_c++;

    return (attrs_min_c > 2 && p->attrs_sum > 75) ? true : false;
}

player_t *player_new()
{
    player_t *p = attrs_init();

    printf("Rolling for character attributes...\n");
    do {
        attrs_generate(p);
    } while (attrs_check(p) == false);

    return p;
}

void player_print_attrs(player_t *p)
{
    printf(">Player name: %s\n\
>Attributes: \n\
\tSTR: %d\n\
\tDEX: %d\n\
\tCON: %d\n\
\tINT: %d\n\
\tWIS: %d\n\
\tCHA: %d\n\n",
            p->name, p->strength, p->dexterity, p->constitution,
            p->intelligence, p->wisdom, p->charisma);

    return;
}

int main(int argc, char *argv[])
{
    puts("RPG Character Generator");
    puts("");
    player_t *p = player_new();
    player_print_attrs(p);
    free(p);

    return EXIT_SUCCESS;
}

#include <stdio.h>

void triangle(int edge_x, int edge_y, int edge_z);

int main(void) {
    int a, b;

    printf("Enter two integers:\n");
    scanf("%d%d", &a, &b);

    if (a < b) {
        printf("%d is less than %d.\n", a, b);
    } else if (a > b) {
        printf("%d is greater than %d.\n", a, b);
    } else {
        printf("%d is equal to %d.\n", a, b);
    }
    
    int x, y, z;

    printf("\nEnter edge lengths for a triangle:\n");
    scanf("%d%d%d", &x, &y, &z);
    triangle(x, y, z);

    return 0;
}

void triangle(int edge_x, int edge_y, int edge_z) {
    if (edge_x + edge_y <= edge_z ) {
        printf("Not a valid triangle.");
    } else if (edge_x + edge_z <= edge_y ) {
        printf("Not a valid triangle.");
    } else if (edge_y + edge_z <= edge_x ) {
        printf("Not a valid triangle.");
    } else {
        printf("It is  a valid triangle.");
    }
    printf("\n");
}

#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

static char daytab[][13] = {
    {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
    {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
};

bool isleap(size_t year)
{
    assert(year > 0);
    size_t leap = year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
    return leap ? true : false;
}

size_t day_of_year(size_t year, size_t month, size_t day)
{
    assert(month > 0 && month <= 12);
    size_t leap = isleap(year) ? 1 : 0;
    assert(day > 0 && day <= (size_t) daytab[leap][month]);
    for (size_t i = 1; i < month; ++i)
        day += daytab[leap][i];
    return day;
}

void month_day(size_t year, size_t yearday, size_t *pmonth, size_t *pday)
{
    assert(yearday > 0 && yearday <= 365);
    size_t leap = isleap(year) ? 1 : 0;
    size_t i;
    for (i = 1; yearday > (size_t) daytab[leap][i]; ++i)
        yearday -= daytab[leap][i];
    *pmonth = i;
    *pday = yearday;
}

int main(void)
{
    size_t pmonth, pday;
    month_day(1988, 221, &pmonth, &pday);
    printf("the 221th day of 1988 is %02zu.%0zu.1988\n", pday, pmonth);
    printf("8th of august, 1988 is the %zuth day\n", day_of_year(1988, 8, 8));

    return 0;
}

#include <stdio.h>
#include <limits.h>
#include <float.h>

int main(void)
{
    printf("Using limits.h and float.h\n");
    printf("type----signedness----min----------------------max\n");
    printf("char    unsigned      %-25u%u\n", 0, UCHAR_MAX);
    printf("        signed        %-25d%d\n", SCHAR_MIN, SCHAR_MAX);
    printf("int     unsigned      %-25u%u\n", 0, UINT_MAX);
    printf("        signed        %-25d%d\n", INT_MIN, INT_MAX);
    printf("long    unsigned      %-25lu%lu\n", 0l, ULONG_MAX);
    printf("        signed        %-25ld%ld\n", LONG_MIN, LONG_MAX);
    printf("float   -             %-25e%e\n", FLT_MIN, FLT_MAX);
    printf("double  -             %-25e%e\n", DBL_MIN, DBL_MAX);
    printf("long d. -             %-25Le%Le\n", LDBL_MIN, LDBL_MAX);

    /*
     * https://clc-wiki.net/wiki/K%26R2_solutions:Chapter_2:Exercise_1
     */
    printf("\nUsing overflow\n");
    printf("type----signedness----min----------------------max\n");
    unsigned char c;
    unsigned int i;
    unsigned long l;
    c = ~0;
    c >>= 1;
    printf("char    unsigned      %-25u%u\n", 0, 2 * c + 1);
    printf("        signed        %-25d%d\n", -c - 1, c);
    i = ~0;
    i >>= 1;
    printf("int     unsigned      %-25u%u\n", 0, 2 * i + 1);
    printf("        signed        %-25d%d\n", -i - 1, i);
    l = ~0;
    l >>= 1;
    printf("long    unsigned      %-25lu%lu\n", 0l, 2 * l + 1);
    printf("        signed        %-25ld%ld\n", -l - 1, l);

    return 0;
}

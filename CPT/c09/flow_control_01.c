#include <stdio.h>
#include <ctype.h>

int main(void) {
    char roman_num;

    printf("Please enter a Roman numeral:\n");
    roman_num = getchar();

    
    if (islower(roman_num)) {
        roman_num = toupper(roman_num);
    }
    

    switch (roman_num) {
        case 'I':
            printf("Numerical value of %c is 1.\n", roman_num);
            break;
        case 'V':
            printf("Numerical value of %c is 5.\n", roman_num);
            break;
        case 'X':
            printf("Numerical value of %c is 10.\n", roman_num);
            break;
        case 'L':
            printf("Numerical value of %c is 50.\n", roman_num);
            break;
        case 'C':
            printf("Numerical value of %c is 100.\n", roman_num);
            break;
        case 'D':
            printf("Numerical value of %c is 500.\n", roman_num);
            break;
        case 'M':
            printf("Numerical value of %c is 1000.\n", roman_num);
            break;
        default:
            printf("%c is not a Roman numerical.\n", roman_num);
    }

    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <SFML/Graphics.h>

int main(int argc, char *argv[])
{
    sfVideoMode mode = {320, 240, 32};
    sfRenderWindow *window;
    sfImage *image;
    sfTexture *texture;
    sfSprite *sprite;
    sfEvent event;

    window = sfRenderWindow_create(mode, "Draw a Pixel - Rosetta Code", sfClose, NULL);
    if (!window)
        exit(EXIT_FAILURE);

    image = sfImage_create(320, 240);
    if (!image)
        exit(EXIT_FAILURE);

    sfIntRect image_rect = { .left = 0, .top = 0, .width = 320, .height = 240 };
    texture = sfTexture_createFromImage(image, &image_rect);
    if (!texture)
        exit(EXIT_FAILURE);

    sprite = sfSprite_create();
    sfSprite_setTexture(sprite, texture, sfTrue);

    while (sfRenderWindow_isOpen(window)) {
        while (sfRenderWindow_pollEvent(window, &event)) {
            if (event.type == sfEvtClosed || event.key.code == sfKeyQ || event.key.code == sfKeyEscape)
                sfRenderWindow_close(window);
        }

        sfRenderWindow_clear(window, sfBlack);

        /* for (int ix = 0, iy = 0; ix = rand() % 320, iy = rand() % 240;) */
        /*     sfImage_setPixel(image, ix, iy, (rand() % 2) ? sfRed : ((rand() % 2) ? sfGreen : sfBlue)); */

        sfImage_setPixel(image, 100, 100, sfRed);
        sfTexture_updateFromImage(texture, image, 0, 0);

        sfRenderWindow_drawSprite(window, sprite, NULL);

        sfRenderWindow_display(window);
    }

    sfSprite_destroy(sprite);
    sfTexture_destroy(texture);
    sfImage_destroy(image);
    sfRenderWindow_destroy(window);

    return EXIT_SUCCESS;
}

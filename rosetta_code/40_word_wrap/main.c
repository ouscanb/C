#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void wrap(FILE *fp, size_t ncol)
{
	char *line = NULL;
	size_t len = 0;
	char *token = NULL;
	const char delim[] = " ";

	while ((getline(&line, &len, fp)) != -1) {
		size_t count = 0;
		char *tmp = line;
		size_t tlen;
		while ((token = strtok_r(tmp, delim, &tmp))) {
			if (!strcmp(token, "\n")) {
				puts("\n");
				continue;
			}
			token[strcspn(token, "\n")] = '\0';

			tlen = strlen(token);
			if (tlen > ncol) {
				fprintf(stderr, "%s\n", "Token is longer ncol!");
				exit(EXIT_FAILURE);
			}

			count += tlen;
			if (count <= ncol) {
				fprintf(stdout, "%s%s", token, delim);
				++count;
			} else {
				ssize_t fill = ncol - (count - tlen - 1);
				for (size_t i = 0; i < fill; ++i)
					printf(" ");
				fprintf(stdout, "\n%s%s", token, delim);
				count = tlen + 1;
			}
		}
	}
	free(line);

	return;
}

int main(void)
{
	FILE *fp = fopen("lipsum.txt", "r");
	if (!fp)
		exit(EXIT_FAILURE);

	size_t ncol = 80;
	wrap(fp, ncol);

	fclose(fp);

	return EXIT_SUCCESS;
}

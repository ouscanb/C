#include <stdio.h>
#include "storage_e.h"

int main(void) {
    int i = 1;
    {
        int i = 1;
        printf("Address of i: %p\n", &i);
    }
    printf("Address of i: %p\n", &i);

    int n = 1;
    while (n <= action) {
        do_something(n);
        n++;
    }
    
    return 0;
}

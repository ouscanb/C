#ifndef _INPUT_H
#define _INPUT_H

#include <stdint.h>

uint8_t GetInput(void);

#endif //_INPUT_H

#include <stdio.h>
#include <stdlib.h>

#define FBUF 16

int main(int argc, char *argv[])
{
    float n = 7.125;
    printf("%09.3f\n", n);

    return EXIT_SUCCESS;
}

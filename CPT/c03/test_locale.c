#include <stdio.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "tr_TR.UTF-8");
    char ch;
    
    printf("Bir tuşa basınız: ");
    ch = getchar();
    printf("\nGirilen karakter: %c\n", ch);
    printf("abcçdefgğ...\n");

    return 0;
}

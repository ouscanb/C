#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "sort.h"

#define ARRL  10 /* default array length */
#define RARRL 10 /* length of the random array */

void cpystr(char s1[], char s2[]);
int * r_arr(void);
float r_average(int arr[], int arr_len);
int arr_find(int arr[], int arr_len, int n);
double gmean(int arr[], int arr_len);

int main(void) {
    int i, arr_l; /* arr_l: array length */
    int arr[5];
    char s[100], d[100]; /* s: source, d: destination */

    arr_l = sizeof(arr) / sizeof(arr[0]);

    printf("Address of the array: %p\n", &arr);
    for (i = 0; i < arr_l; i++) {
        printf("Address of the a[%d]:  %p\n", i, &arr[i]);
    }
    
    int arr2[] = {1, 2, 3, 4, 5};
    int a_ind = arr_find(arr2, 5, 8);
    if (a_ind != -1) {
        printf("\nFound it at index %d.\n", a_ind);
    } else {
        printf("\nNot found.\n");
    }

    printf("\nA random array of %d elements: \n", RARRL);

    int * rp; /* pointer to the random arrray generated */
    rp = r_arr();
    
    int n;
    for (n = 0; n < RARRL; n++) {
        printf("Value of r_arr[%d] is %d\n", n, * (rp + n));
    }
    
    int na;
    float avg;
    printf("\nEnter the number of integer items to be averaged: ");
    scanf("%d", &na);
    printf("Now, enter the numbers.\n");
    int avg_arr[na];
    avg = r_average(avg_arr, na);
    printf("The average is %.1f.\n", avg);

    getchar();

    printf("\nType a sentence:\n");
    fgets(s, 100, stdin);
    /* remove the newline */
    int len = strlen(s);
    if (len > 0 && s[len - 1] == '\n') {
        s[len - 1] = '\0';
    }

    printf("Source string:\n%s\n", s);
    strcpy(d, s);
    printf("Destination string:\n%s\n", d);

    printf("\nType a sentence:\n");
    fgets(s, 100, stdin);
    /* remove the newline */
    len = strlen(s);
    if (len > 0 && s[len - 1] == '\n') {
        s[len - 1] = '\0';
    }

    printf("Source string:\n%s\n", s);
    cpystr(s, d);
    printf("Destination string:\n%s\n", d);
    
    int barr[10] = {12, 42, 12, 69, -39, -6, 0, 26, 8, 3};
    printf("\nunSorted array:\n");
    for (i = 0; i < 10; i++) {
        printf("%d ", barr[i]);
    }
    printf("\n(selection)Sorted array:\n");
    selection_sort(barr, 10);
    for (i = 0; i < 10; i++) {
        printf("%d ", barr[i]);
    }
    int barr2[10] = {12, 42, 12, 69, -39, -6, 0, 26, 8, 3};
    printf("\n(bubble)Sorted array:\n");
    bubble_sort(barr2, 10);
    for (i = 0; i < 10; i++) {
        printf("%d ", barr2[i]);
    }   
    int barr3[10] = {12, 42, 12, 69, -39, -6, 0, 26, 8, 3};
    printf("\n(insertion)Sorted array:\n");
    insertion_sort(barr3, 10);
    for (i = 0; i < 10; i++) {
        printf("%d ", barr3[i]);
    }

    int arr3[5] = {1, 2, 3, 4, 5};
    printf("\n\nThe geometric mean of the given array is %lf\n", gmean(arr3, 5));

    printf("\n");
    return 0;
}

void cpystr(char s1[], char s2[]) {
    /* copies string s1 into s2 */
    int i = 0;

    while (s1[i] != '\0') {
        s2[i] = s1[i];
        i++;
    }

    s2[i] = '\0';
}
int * r_arr(void) {
    /* creates a pointer to an array filled with random integers */
    static int arr[RARRL];
   
    time_t st;
    st = time(NULL);
    srand((unsigned) st);

    int i;
    for (i = 0; i < RARRL; i++) {
        arr[i] = rand() % 100;
    }

    return arr;
}

float r_average(int arr[], int arr_len) {
    /* takes the average of the array elements */
    int i = 0;
    int sum = 0;

    do {
        printf("-> ");
        scanf("%d", &arr[i]);
        i++;
    } while (i < arr_len);

    for (i = 0; i < arr_len; i++) {
        sum += arr[i];
    }

    return sum / arr_len;
}

int arr_find(int arr[], int arr_len, int n) {
    /* finds the index of the element equal to n */ 
    int i;

    for (i = 0; i < arr_len; i++) {
        if (arr[i] == n) {
            return i;
        }
    }

    return -1;
}

double gmean(int arr[], int arr_len) {
    int i;
    double mult = 1, gmean;

    for (i = 0; i < arr_len; i++) {
        mult *= arr[i];
    }
    gmean = pow(mult, 1.0 / arr_len);

    return gmean;
}

#include <stdio.h>

unsigned getbits(unsigned x, size_t p, size_t n);
unsigned setbits(unsigned x, size_t p, size_t n, size_t y);

int main(void)
{
    unsigned n;
    scanf("%u", &n);
    printf("%u %u\n", n, setbits(n, 5, 3, 3));

    return 0;

}

unsigned getbits(unsigned x, size_t p, size_t n)
{
    return (x >> (p + 1 - n)) & ~(~0u << n);
}

unsigned setbits(unsigned x, size_t p, size_t n, size_t y)
{
    unsigned bits_y = getbits(x, y, n) << (p + 1 - n);
    unsigned mask_p = ~(~0u << n) << (p + 1 - n);

    if (y != p)
        return (x & ~mask_p) | bits_y;
    else
        return x;
}

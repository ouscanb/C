#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#define ARRLEN(ARR) ((sizeof(ARR)) / (sizeof(ARR[0])))

int main(int argc, char *argv[])
{
    const char s[] = "9";
    long n = strtol(s, NULL, 10);
    if (errno == ERANGE) {
        perror(NULL);
        return EXIT_FAILURE;
    }
    int s_len = snprintf(NULL, 0, "%ld", n + 1);
    char s_inc[s_len + 1];
    sprintf(s_inc, "%ld", n + 1);
    /* printf("%ld %d \n", ARRLEN(s), s_len + 1); */
    printf("%s -> %s\n", s, s_inc);

    return EXIT_SUCCESS;
}

#include <stdio.h>

size_t getline(char *line, size_t maxlen)
{
    int c;

    size_t i;
    for (i = 0; i < maxlen - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
        line[i] = c;
    if (c == '\n')
        line[i++] = c;
    line[i] = '\0';

    return i;
}

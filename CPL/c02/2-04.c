#include <stdio.h>
#include <string.h>

#define BUF  12

void squeeze(char s1[], char s2[])
{
    size_t i;
    for (i = 0; s2[i] != '\0'; ++i) {
        size_t j, k;
        for (j = k = 0; s1[j] != '\0'; ++j) {
            if (s1[j] != s2[i])
                s1[k++] = s1[j];
        }
        s1[k] = '\0';
    }
}

size_t getline(char s[], size_t slen)
{
    int c;
    size_t i;
    for (i = 0; i < slen - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
        s[i] = c;
    /* do not store newline*/
    /* if (c == '\n') */
    /*     s[i++] = c; */
    s[i] = '\0';

    return i;
}

int main(void)
{
    char s1[BUF];
    char s2[BUF];
    /* getline(s1, BUF); */
    /* getline(s2, BUF); */
    fgets(s1, BUF, stdin);
    /* https://stackoverflow.com/questions/30388101/
     * how-to-remove-extra-characters-input-from-fgets-in-c */
    if (!strchr(s1, '\n')) {
        int c;
        while ((c = fgetc(stdin)) != '\n' && c != EOF)
            ;
    }
    fgets(s2, BUF, stdin);
    squeeze(s1, s2);
    printf("Squeezed: %s\n", s1);

    return 0;
}

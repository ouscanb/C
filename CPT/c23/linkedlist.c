#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct node {
    int val;

    struct node *next;
} node_t;

void print_list(node_t *head);
int count(node_t *head);
node_t *create(int n, node_t *next);
node_t *random_list(node_t **head, int nrand);
node_t *prepend(node_t **head, int n);
node_t *append(node_t *head, int n);
node_t *insert_after(node_t *head, node_t *prev, int n);
node_t *insert_before(node_t *head, node_t *next, int n);
node_t *remove_any(node_t *head, node_t *nd);
node_t *search(node_t *head, int n);
node_t *reverse(node_t **head);
node_t *sort(node_t **head);
node_t *insert_sorted(node_t **head, int n);
node_t *merge(node_t *head_a, node_t *head_b);
node_t *merge_sorted(node_t **head_a, node_t **head_b);
void free_list(node_t **head);

/*
void check_node(node_t *head, node_t *curr);
void destroy_list(node_t *head);
*/

int main(void) {
/*
    node_t a;
    node_t b;

    a.val = 1;
    b.val = 2;

    a.next = &b;
    b.next = NULL;

    printf("Size of node_t is %d bytes.\n", sizeof(node_t));
    printf("Node  'a'   points to %p,\n", a.next);
    printf("Addr, 'a'   points to %p,\n", &b);
    printf("Node  'b'   points to %p.\n\n", b.next);

    node_t *n1 = NULL,
           *n2 = NULL,
           *n3 = NULL;

    printf("Size of n1 is %d bytes.\n", sizeof(n1));
    n1 = malloc(sizeof(node_t));
    n2 = malloc(sizeof(node_t));
    n3 = malloc(sizeof(node_t));

    n1->val = 1;
    n1->next = n2;
    n2->val = 2;
    n2->next = n3;
    n3->val = 3;
    n3->next = NULL;

    print_list(n1);

    printf("\n");
*/
/*
    node_t *curr,
           *prev = NULL;

    int i;
    for (i = 5; i > 0; i--) {
        curr = malloc(sizeof(node_t));
        curr->val = i;
        curr->next = prev;
        prev = curr;
    }
    curr = prev;

    append(curr, 13);
    append(curr, 17);
    append(curr, 19);
    prepend(&curr, 42);
    node_t *ins = search(curr, 8);
    insert_after(curr, ins, 100);
    insert_before(curr, ins, 100);
    node_t *rem = search(curr, 4);
    remove_any(curr, rem);
    print_list(curr);

    printf("\n");
    reverse(&curr);
    print_list(curr);

    printf("\n");
    sort(&curr);
    insert_sorted(&curr, 11);
    print_list(curr);
*/
    node_t *rand1;
    node_t *rand2;

    random_list(&rand1, 5);
    random_list(&rand2, 5);

    print_list(rand1);
    printf("\n");
    print_list(rand2);
    printf("\n");

    sort(&rand1);
    sort(&rand2);

    node_t *rand;
    rand = merge_sorted(&rand1, &rand2);
    print_list(rand);
/*
    printf("\nNumber of linked lists: %d\n", count(curr));

    node_t *test;
    int n;

    printf("\nEnter an integer to search in the linked list: ");
    scanf("%d", &n);

    test = search(curr, n);
    if (test != NULL) {
        printf("\n%d is found.\n", n);
    } else {
        printf("%d is not found.\n", n);
    }

    free_list(&n1);
    free_list(&curr);
*/

    free_list(&rand);
    free_list(&rand1);
    free_list(&rand2);

    return 0;
}

void print_list(node_t *head) {
    while (head) {
        printf("Value: %d\n", head->val);
        head = head->next;
    }

    return;
}

int count(node_t *head) {
    node_t *cursor = head;
    int count = 0;

    while (cursor != NULL) {
        count++;
        cursor = cursor->next;
    }

    return count;
}

node_t *create(int n, node_t *next) {
    node_t *new;

    new = malloc(sizeof(node_t));
    if (new == NULL) {
        fprintf(stderr, "Error: Memory cannot be allocated.\n");
        exit(EXIT_FAILURE);
    }

    new->val = n;
    new->next = next;

    return new;
}

node_t *random_list(node_t **head, int nrand) {
    int i;
    static int t = 1;
    node_t *current = *head,
           *prev = NULL;

    time_t s;
    s = time(NULL);
    srand((unsigned) s * t);
    t++;

    for (i = 0; i < nrand; i++) {
        current = malloc(sizeof(node_t));
        current->val = rand() % 100;
        current->next = prev;
        prev = current;
    }
    current = prev;
    *head = current;

    return *head;
}

node_t *prepend(node_t **head, int n) {
    node_t *new;

    new = create(n, *head);
    /* see stackoverflow.com/questions/12674616
     * In order to change node pointer (head), 
     * node pointer needs to be passed as a pointer 
     * of pointer node_t **head.
     */
    *head = new;

    return *head;
}

node_t *append(node_t *head, int n) {
    if ( head == NULL) {
        return NULL;
    }

    node_t *cursor = head;
    while (cursor->next != NULL) {
       cursor = cursor->next;
    }
    cursor->next = create(n, NULL);

    return head;
}

node_t *insert_after(node_t *head, node_t *prev, int n) {
    if (head == NULL || prev == NULL) {
        return NULL;
    }

    node_t *cursor = search(head, prev->val);
    if (cursor != NULL) {
        node_t *new = create(n, NULL);
        node_t *next = cursor->next;

        cursor->next = new;
        new->next = next;

        return head;
    }

    return NULL;
}

node_t *insert_before(node_t *head, node_t *next, int n) {
    if (head == NULL || next == NULL) {
        return NULL;
    }

    node_t *cursor = head;
    while (cursor != NULL) {
        if (cursor->next == next) {
            break;
        }

        cursor = cursor->next;
    }

    if (cursor != NULL) {
        node_t *new = create(n, cursor->next);
        node_t *prev = cursor;

        prev->next = new;

        return head;
    }

    return NULL;
}

node_t *remove_any(node_t *head, node_t *nd) {
    if ( head == NULL && nd == NULL) {
        return NULL;
    } else if ( nd == NULL) {
        return head;
    }

    node_t *cursor = head;
    while (cursor != NULL) {
        if (cursor->next == nd) {
            break;
        }

        cursor = cursor->next;
    }

    if (cursor != NULL) {
        cursor->next = nd->next;

        nd->next = NULL;
        free(nd);

        return head;
    }

    return head;
}

node_t *search(node_t *head, int n) {
    node_t *cursor = head;

    while (cursor != NULL) {
        if (cursor->val == n) {
            return cursor;
        }

        cursor = cursor->next;
    }

    return NULL;
}

node_t *reverse(node_t **head) {
    node_t *prev = NULL;
    node_t *current = *head;
    node_t *next;

    while (current != NULL) {
        next = current->next;
        current->next = prev;
        prev = current;
        current = next;
    }
    *head = prev;

    return *head;
}

node_t *sort(node_t **head) {
    /* using insertion sort */

    /* initialize sorted linked list */
    node_t *sorted = NULL;
    node_t *cursor = *head;
    node_t *current = *head;
    node_t *new;
    node_t *next;

    while (current != NULL) {
        next = current->next;

        /* check new (= current) node against the sorted list
         * and insert into it */
        new = current;
        if (sorted == NULL || sorted->val > new->val) {
            new->next = sorted;
            sorted = new;
        } else {
            /* locate the node having the value less than new
             * and insert after it */
            cursor = sorted;
            while (cursor->next != NULL && cursor->next->val < new->val) {
                cursor = cursor->next;
            }
            new->next = cursor->next;
            cursor->next = new;
        }

        current = next;
    }
    *head = sorted;

    return *head;
}

node_t *insert_sorted(node_t **head, int n) {
   if (*head == NULL) {
       return NULL;
   }

   node_t *cursor = *head;
   while (cursor != NULL) {
       if (cursor->val > n) {
           insert_before(*head, cursor, n);

           return *head;
       } else {
           cursor = cursor->next;
       }
   }

   insert_after(*head, cursor, n);

   return *head;
}

node_t *merge(node_t *head_a, node_t *head_b) {
    if (head_a == NULL) {
        return head_b;
    } else if (head_b == NULL) {
        return head_a;
    }

    node_t *cursor = head_a;
    while (cursor != NULL) {
        cursor = cursor->next;
    }

    cursor->next = head_b;

    return head_a;
}

node_t *merge_sorted(node_t **head_a, node_t **head_b) {
    if (*head_a == NULL) return *head_b;
    if (*head_b == NULL) return *head_a;

    node_t *head;

    if ((*head_a)->val < (*head_b)->val) {
        head = *head_a;
    } else {
        head = *head_b;
        *head_b = *head_a;
        *head_a = head;
    }

    while ((*head_a)->next != NULL) {
        if ((*head_a)->next->val > (*head_b)->val) {
            node_t *tmp = (*head_a)->next;
            (*head_a)->next = *head_b;
            *head_b = tmp;
        }
        *head_a = (*head_a)->next;
    }
    (*head_a)->next = *head_b;

    return head;
}

/*
void free_list(node_t **head) {
   node_t *cursor, *tmp;

   if (*head != NULL) {
       cursor = (*head)->next;
       (*head)->next = NULL;
       free(*head);

       while (cursor != NULL) {
           tmp = cursor->next;
           cursor->next = NULL;
           free(cursor);

           cursor = tmp;
       }
   }

   return;
}
*/

void free_list(node_t **head) {
    if (head == NULL) return;

    node_t *tmp;
    while (*head != NULL) {
        tmp = *head;
        *head = (*head)->next;
        tmp->next = NULL;
        free(tmp);
    }

    return;
}

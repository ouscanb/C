#include <stdio.h>

char *strcat_reg(char *, char *);
char *strcat(char *, char *);

int main(void)
{
    char s1[32] = "Hello,";
    char s2[] = " World!";
    printf("%s\n", strcat_reg(s1, s2));
    printf("%s\n", strcat(s1, s2));

    return 0;
}

char *strcat_reg(char *s, char *t)
{
    size_t i = 0, j = 0;
    while (s[i] != '\0')
        ++i;
    while ((s[i++] = t[j++]) != '\0')
        ;
    return s;
}

char *strcat(char *s, char *t)
{
    char *p = s;
    while (*s)
        ++s;
    while ((*s++ = *t++))
        ;
    return p;
}

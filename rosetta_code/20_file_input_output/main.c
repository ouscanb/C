#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>

#define BUF 1024

int main(int argc, char *argv[])
{
    char ofile[] = "output.txt",
         ifile[] = "input.txt";

    FILE *ofp = fopen(ofile, "w+"),
         *ifp = fopen(ifile, "r");
    if (!ofp || !ifp)
        return EXIT_FAILURE;

    /*
    char line[BUF];
    while (fgets(line, BUF, ifp))
        fprintf(ofp, "%s", line);
    */

    char *line = malloc(BUF * sizeof(char));
    if(!line)
        return EXIT_FAILURE;
    size_t buf = BUF;

    while (getline(&line, &buf, ifp) != -1)
        fprintf(ofp, "%s", line);

    free(line);

    if (fclose(ofp) == EOF || fclose(ifp) == EOF) {
        perror(NULL);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

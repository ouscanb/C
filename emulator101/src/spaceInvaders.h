#ifndef _SPACEINVADERS_H
#define _SPACEINVADERS_H

#include <bits/stdint-uintn.h>
#include <stdio.h>
#include <stdint.h>

#include "emulator.h"

uint8_t MachineIn8080(State8080 *state, uint8_t port)
{
	(void) state;
	(void) port;
	return 0x00;
}

/* https://github.com/superzazu/8080/blob/master/i8080_tests.c */
uint8_t MachineOut8080(State8080 *state, uint8_t port)
{
	if (port == 0) {
		return 1;
	} else if (port == 1) {
		uint8_t op = state->c;

		if (op == 2) {
			/* print a character stored in E */
			printf("%c", state->e);
		} else if (op == 9) {
			/* print from memory at DE until '$' character */
			uint16_t offset = (state->d << 8) | state->e;
			do {
				printf("%c", state->memory[offset++]);
			} while (state->memory[offset] != '$');
		}
		
	}
	return 0;
}

#endif // _SPACEINVADERS_H

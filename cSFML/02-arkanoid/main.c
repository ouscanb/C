#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <SFML/Graphics.h>
#include <SFML/System.h>

typedef struct brick {
    unsigned short id;

    sfRectangleShape *shape;
    sfFloatRect rect;

    sfVector2f size;
    sfVector2f pos;
    sfVector2f speed;
    sfColor color;
    sfBool is_destroyable;

    struct brick *next;
} brick;

sfRectangleShape *brick_create_shape(brick *brck);
sfFloatRect brick_create_rect(brick *brck);
void update_rect(brick *brck);
brick *brick_create(sfVector2f size, sfVector2f pos, sfVector2f speed, sfColor color, sfBool is_destroyable);
brick *paddle_create(sfVector2f size, sfVector2f pos, sfVector2f speed, sfColor color, sfBool is_destroyable);
brick *ball_create(sfVector2f size, sfVector2f pos, sfVector2f speed, sfColor color, sfBool is_destroyable);
brick *brick_add(brick **lvl, brick *brick);
brick *level_create(sfVideoMode mode, brick **lvl, char *lvl_str);
void update_level(sfRenderWindow *window, brick *lvl, brick *pddle, brick *ball, sfColor color);
int check_collision(sfVideoMode mode, brick *lvl, brick *pddle, brick *ball);
brick *brick_search(brick *lvl, int id);
brick *brick_search_before(brick *lvl, int id);
void brick_remove(brick **lvl, int id);
void brick_destroy(brick *brck);
void level_destroy(brick **lvl);
brick *level_traverse(brick *lvl);

char *lvl01 = "\
--------\n\
.------.\n\
--------\n\
.------.";

int main(void)  {
    /* set the window resolution */
    sfVector2f vmode = {640, 320};

    sfVideoMode mode = {vmode.x, vmode.y, 32};
    sfRenderWindow *window;
    sfFont *font;
    /*sfText *text;*/
    sfEvent event;

    window = sfRenderWindow_create(mode, "ARKANOYD", sfClose, NULL);
    if (window == NULL) {
        fprintf(stderr, "Error: Window cannot be created!\n");

        exit(EXIT_FAILURE);
    }

    sfRenderWindow_setFramerateLimit(window, 60);

    font = sfFont_createFromFile("DejaVuSans.ttf");
    if (font == NULL) {
        fprintf(stderr, "Error: Font cannot be created!\n");

        exit(EXIT_FAILURE);
    }

    brick *lvl = NULL;
    lvl = level_create(mode, &lvl, lvl01);


    /* set the paddle parameters */
    sfVector2f psize = {70, 10};
    sfVector2f ppos = {(vmode.x - psize.x) / 2, vmode.y - 30};
    sfVector2f pspeed = {10, 0};
    sfColor pcolor = sfGreen;

    /* set the ball parameters */
    sfVector2f ball_size = {10, 10};
    sfVector2f ball_pos = {(vmode.x - ball_size.x) / 2, vmode.y - 32 - psize.y};
    sfColor ball_color = sfWhite;
    sfVector2f ball_speed = {1, -2};

    brick *pddle = NULL;
    pddle = paddle_create(psize, ppos, pspeed, pcolor, sfFalse);
    brick_add(&lvl, pddle);

    brick *ball = NULL;
    ball = ball_create(ball_size, ball_pos, ball_speed, ball_color, sfFalse);

    while (sfRenderWindow_isOpen(window)) {
        while (sfRenderWindow_pollEvent(window, &event)) {
            if (event.type == sfEvtClosed || event.key.code == sfKeyQ) sfRenderWindow_close(window);
        }

        if (sfKeyboard_isKeyPressed(sfKeyA) || sfKeyboard_isKeyPressed(sfKeyLeft)) {
            if (pddle->pos.x >= 0 && pddle->pos.x <= vmode.x - pddle->size.x) {
                if (pddle->pos.x - pddle->speed.x < 0) pddle->pos.x = 0;
                else pddle->pos.x -= pddle->speed.x;
            }
        } else if (sfKeyboard_isKeyPressed(sfKeyS) || sfKeyboard_isKeyPressed(sfKeyRight)) {
            if (pddle->pos.x >= 0 && pddle->pos.x <= vmode.x - pddle->size.x) {
                if (pddle->pos.x + pddle->size.x + pddle->speed.x > vmode.x) pddle->pos.x = vmode.x - pddle->size.x;
                else pddle->pos.x += pddle->speed.x;
            }
        }

        int id = check_collision(mode, lvl, pddle, ball);
        if (id != pddle->id && id != -1) {
            brick_remove(&lvl, id);
        }


        update_level(window, lvl, pddle, ball, sfBlack);
        sfRenderWindow_display(window);
    }

    level_destroy(&lvl);
    brick_destroy(ball);
    sfFont_destroy(font);
    sfRenderWindow_destroy(window);

    return EXIT_SUCCESS;
}

sfRectangleShape *brick_create_shape(brick *brck) {
    sfRectangleShape *shape;
    shape = sfRectangleShape_create();

    sfRectangleShape_setSize(shape, brck->size);
    sfRectangleShape_setPosition(shape, brck->pos);
    sfRectangleShape_setFillColor(shape, brck->color);

    return shape;
}

sfFloatRect brick_create_rect(brick *brck) {
    sfFloatRect rect;

    rect.width = brck->size.x;
    rect.height = brck->size.y;
    rect.left = brck->pos.x;
    rect.top = brck->pos.y;

    return rect;
}

void update_rect(brick *brck) {
    brck->rect.left = brck->pos.x;
    brck->rect.top = brck->pos.y;
    brck->rect.width = brck->size.x;
    brck->rect.height = brck->size.y;

    return;
}

brick *brick_create(sfVector2f size, sfVector2f pos, sfVector2f speed, sfColor color, sfBool is_destroyable) {
    static unsigned short id = 0;
    brick *brck;

    brck = malloc(sizeof(brick));
    if (brck == NULL) {
        fprintf(stderr, "Error: Memory cannot be allocated!\n");

        exit(EXIT_FAILURE);
    }

    brck->id = id;
    brck->is_destroyable = sfTrue;
    brck->size = size;
    brck->pos = pos;
    brck->color = color;
    brck->speed = speed;
    brck->shape = brick_create_shape(brck);
    brck->rect = brick_create_rect(brck);

    brck->next = NULL;
    ++id;

    return brck;
}

brick *paddle_create(sfVector2f size, sfVector2f pos, sfVector2f speed, sfColor color, sfBool is_destroyable) {
    brick *pddle;
    pddle = brick_create(size, pos, speed, color, is_destroyable);

    return pddle;
}

brick *ball_create(sfVector2f size, sfVector2f pos, sfVector2f speed, sfColor color, sfBool is_destroyable) {
    brick *ball;
    ball = brick_create(size, pos, speed, color, is_destroyable);

    return ball;
}

brick *brick_add(brick **lvl, brick *brck) {
    if (*lvl == NULL) {
        *lvl = brck;

        return *lvl;
    }

    brick *lvl_tail = level_traverse(*lvl);
    lvl_tail->next = brck;

    return *lvl;
}

brick *level_create(sfVideoMode mode, brick **lvl, char *lvl_str) {
    /* set the brick parameters */
    sfVector2f bpadding = {5, 5};
    sfVector2f bsize = {50, 20};
    sfVector2f bspeed = {0, 0};
    sfColor bcolor;

    char *b, *l;
    b = lvl_str;
    l = lvl_str;

    int lvl_ncol = 0,
        lvl_nrow = 1;

    int i = 0;
    while (*l) {
        if (*l == '\n') {
            i = 1;
            ++lvl_nrow;
        }
        if (!i) ++lvl_ncol;
        ++l;
    }

    /* set the padding from the window edges */
    sfVector2f padding = {(mode.width - (lvl_ncol * bsize.x + (lvl_ncol - 1) * bpadding.x)) / 2, 50};
    sfVector2f bpos = {padding.x, padding.y};

    while (*b) {
        switch(*b) {
            case '-':
                bcolor = sfYellow;

                brick *brck;
                brck = brick_create(bsize, bpos, bspeed, bcolor, sfTrue);
                brick_add(&(*lvl), brck);

                bpos.x += bsize.x + bpadding.x;
                ++b;

                break;

            case '.':
                bpos.x += bsize.x + bpadding.x;
                ++b;

                break;

            case '\n':
                bpos.x = padding.x;
                bpos.y += bsize.y + bpadding.y;
                ++b;

                break;
        }
    }

    return *lvl;
}

void update_level(sfRenderWindow *window, brick *lvl, brick *pddle, brick *ball, sfColor color) {
    brick *cursor = lvl;

    sfRenderWindow_clear(window, color);

    while (cursor != NULL) {
        sfRenderWindow_drawRectangleShape(window, cursor->shape, NULL);

        cursor = cursor->next;
    }

    ball->pos.x += ball->speed.x;
    ball->pos.y += ball->speed.y;

    update_rect(pddle);
    update_rect(ball);

    sfRectangleShape_setPosition(pddle->shape, pddle->pos);
    sfRectangleShape_setPosition(ball->shape, ball->pos);

    sfRenderWindow_drawRectangleShape(window, pddle->shape, NULL);
    sfRenderWindow_drawRectangleShape(window, ball->shape, NULL);

    return;
}

int check_collision(sfVideoMode mode, brick *lvl, brick *pddle, brick *ball) {
    brick *cursor = lvl;
    sfVector2f wsize;

    wsize.x = mode.width;
    wsize.y = mode.height;

    while (cursor != NULL) {
        if (ball->rect.left < cursor->rect.left + cursor->rect.width &&
                cursor->rect.left < ball->rect.left + ball->rect.width &&
                ball->rect.top < cursor->rect.top + cursor->rect.height &&
                cursor->rect.top < ball->rect.top + ball->rect.height) {

            if (ball->rect.left + ball->rect.width - cursor->rect.left <  ball->rect.width / 5 ||
                    cursor->rect.left + cursor->rect.width - ball->rect.left < ball->rect.width / 5) {

                if (cursor->id == pddle->id) {
                    if (ball->rect.left < cursor->rect.left) {
                        ball->speed.x *= -1;
                        ball->pos.x = cursor->pos.x - ball->size.x;
                        update_rect(ball);
                    }
                }

                ball->speed.x *= -1;
                return cursor->id;
            } else if (ball->rect.top + ball->rect.height - cursor->rect.top <  ball->rect.width / 5 ||
                    cursor->rect.top + cursor->rect.height - ball->rect.top < ball->rect.width / 5) {

                ball->speed.y *= -1;
                return cursor->id;
            }

            ball->speed.y *= -1;
            return cursor->id;
        }
        cursor = cursor->next;
    }

    if (ball->rect.left < 0) ball->speed.x *= -1;
    if (ball->rect.left > wsize.x - ball->rect.width) ball->speed.x *= -1;
    if (ball->rect.top < 0) ball->speed.y *= -1;
    if (ball->rect.top > wsize.y - ball->rect.height) ball->speed.y *= -1;


    return -1;
}

brick *brick_search(brick *lvl, int id) {
    brick *cursor = lvl;

    while (cursor != NULL) {
        if (cursor->id == id) {
            return cursor;
        }

        cursor = cursor->next;
    }

    return NULL;
}

brick *brick_search_before(brick *lvl, int id) {
    brick *cursor = lvl;
    if (cursor == NULL || cursor->next == NULL) {
        return NULL;
    }

    while (cursor->next != NULL) {
        if (cursor->next->id == id) {
            return cursor;
        }

        cursor = cursor->next;
    }

    return NULL;
}

void brick_remove(brick **lvl, int id) {
    brick *brck = brick_search(*lvl, id);
    brick *brck_before = brick_search_before(*lvl, id);

    if (brck != NULL && brck_before != NULL) {
        brck_before->next = brck->next;
        brick_destroy(brck);
    } else if (brck != NULL && brck_before == NULL) {
        brick *tmp = *lvl;
        *lvl = (*lvl)->next;
        brick_destroy(tmp);
    }

    return;
}

void brick_destroy(brick *brck) {
    sfRectangleShape_destroy(brck->shape);
    brck->next = NULL;
    free(brck);

    return;
}

void level_destroy(brick **lvl) {
    if (*lvl == NULL) return;

    brick *tmp = *lvl;
    while (*lvl != NULL) {
        tmp = *lvl;
        *lvl = (*lvl)->next;
        brick_destroy(tmp);
    }

    return;
}

brick *level_traverse(brick *lvl) {
    if (lvl == NULL) return NULL;

    brick *cursor = lvl;
    while (cursor->next != NULL) {
        cursor = cursor->next;
    }

    return cursor;
}


#include <stddef.h>
#include <math.h>

#include "prime.h"

bool is_prime(const size_t x)
{
	if (x < 2) return false;
	if (x < 4) return true;
	if ((x % 2) == 0) return false;

	double lim = floor(sqrt((double)x));
	for (size_t i = 3; i <= (size_t)lim ; ++i) {
		if ((x % i) == 0)
			return false;
	}

	return true;
}

size_t next_prime(size_t x)
{
	while (!is_prime(x)) {
		++x;
	}

	return x;
}

   1              		.file	"hello.c"
   2              		.intel_syntax noprefix
   3              	# GNU C11 (Debian 7.3.0-18) version 7.3.0 (i686-linux-gnu)
   4              	#	compiled by GNU C version 7.3.0, GMP version 6.1.2, MPFR version 4.0.1, MPC version 1.1.0, isl ve
   5              	
   6              	# GGC heuristics: --param ggc-min-expand=97 --param ggc-min-heapsize=127088
   7              	# options passed:  -imultiarch i386-linux-gnu hello.c -masm=intel
   8              	# -mtune=generic -march=i686 -g -fverbose-asm
   9              	# options enabled:  -fPIC -fPIE -faggressive-loop-optimizations
  10              	# -fasynchronous-unwind-tables -fauto-inc-dec -fchkp-check-incomplete-type
  11              	# -fchkp-check-read -fchkp-check-write -fchkp-instrument-calls
  12              	# -fchkp-narrow-bounds -fchkp-optimize -fchkp-store-bounds
  13              	# -fchkp-use-static-bounds -fchkp-use-static-const-bounds
  14              	# -fchkp-use-wrappers -fcommon -fdelete-null-pointer-checks
  15              	# -fdwarf2-cfi-asm -fearly-inlining -feliminate-unused-debug-types
  16              	# -ffp-int-builtin-inexact -ffunction-cse -fgcse-lm -fgnu-runtime
  17              	# -fgnu-unique -fident -finline-atomics -fira-hoist-pressure
  18              	# -fira-share-save-slots -fira-share-spill-slots -fivopts
  19              	# -fkeep-static-consts -fleading-underscore -flifetime-dse
  20              	# -flto-odr-type-merging -fmath-errno -fmerge-debug-strings
  21              	# -fpcc-struct-return -fpeephole -fplt -fprefetch-loop-arrays
  22              	# -fsched-critical-path-heuristic -fsched-dep-count-heuristic
  23              	# -fsched-group-heuristic -fsched-interblock -fsched-last-insn-heuristic
  24              	# -fsched-rank-heuristic -fsched-spec -fsched-spec-insn-heuristic
  25              	# -fsched-stalled-insns-dep -fschedule-fusion -fsemantic-interposition
  26              	# -fshow-column -fshrink-wrap-separate -fsigned-zeros
  27              	# -fsplit-ivs-in-unroller -fssa-backprop -fstdarg-opt
  28              	# -fstrict-volatile-bitfields -fsync-libcalls -ftrapping-math -ftree-cselim
  29              	# -ftree-forwprop -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
  30              	# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop
  31              	# -ftree-reassoc -ftree-scev-cprop -funit-at-a-time -funwind-tables
  32              	# -fverbose-asm -fzero-initialized-in-bss -m32 -m80387 -m96bit-long-double
  33              	# -malign-stringops -mavx256-split-unaligned-load
  34              	# -mavx256-split-unaligned-store -mfancy-math-387 -mfp-ret-in-387 -mglibc
  35              	# -mieee-fp -mlong-double-80 -mno-red-zone -mno-sse4 -mpush-args -msahf
  36              	# -mstv -mtls-direct-seg-refs -mvzeroupper
  37              	
  38              		.text
  39              	.Ltext0:
  40              		.section	.rodata
  41              	.LC0:
  42 0000 48656C6C 		.string	"Hello, world!"
  42      6F2C2077 
  42      6F726C64 
  42      2100
  43              		.text
  44              		.globl	main
  46              	main:
  47              	.LFB0:
  48              		.file 1 "hello.c"
   1:hello.c       **** #include <stdio.h>
   2:hello.c       **** 
   3:hello.c       **** int main()
   4:hello.c       **** {
  49              		.loc 1 4 0
  50              		.cfi_startproc
  51 0000 8D4C2404 		lea	ecx, 4[esp]	#,
  52              		.cfi_def_cfa 1, 0
  53 0004 83E4F0   		and	esp, -16	#,
  54 0007 FF71FC   		push	DWORD PTR -4[ecx]	#
  55 000a 55       		push	ebp	#
  56              		.cfi_escape 0x10,0x5,0x2,0x75,0
  57 000b 89E5     		mov	ebp, esp	#,
  58 000d 53       		push	ebx	#
  59 000e 51       		push	ecx	#
  60              		.cfi_escape 0xf,0x3,0x75,0x78,0x6
  61              		.cfi_escape 0x10,0x3,0x2,0x75,0x7c
  62 000f E8FCFFFF 		call	__x86.get_pc_thunk.ax	#
  62      FF
  63 0014 05010000 		add	eax, OFFSET FLAT:_GLOBAL_OFFSET_TABLE_	# tmp87,
  63      00
  64              	# hello.c:5:     printf("Hello, world!\n");
   5:hello.c       ****     printf("Hello, world!\n");
  65              		.loc 1 5 0
  66 0019 83EC0C   		sub	esp, 12	#,
  67 001c 8D900000 		lea	edx, .LC0@GOTOFF[eax]	# tmp90,
  67      0000
  68 0022 52       		push	edx	# tmp90
  69 0023 89C3     		mov	ebx, eax	#, tmp87
  70 0025 E8FCFFFF 		call	puts@PLT	#
  70      FF
  71 002a 83C410   		add	esp, 16	#,
  72              	# hello.c:7:     return 0;
   6:hello.c       **** 
   7:hello.c       ****     return 0;
  73              		.loc 1 7 0
  74 002d B8000000 		mov	eax, 0	# _3,
  74      00
  75              	# hello.c:8: }
   8:hello.c       **** }
  76              		.loc 1 8 0
  77 0032 8D65F8   		lea	esp, -8[ebp]	#,
  78 0035 59       		pop	ecx	#
  79              		.cfi_restore 1
  80              		.cfi_def_cfa 1, 0
  81 0036 5B       		pop	ebx	#
  82              		.cfi_restore 3
  83 0037 5D       		pop	ebp	#
  84              		.cfi_restore 5
  85 0038 8D61FC   		lea	esp, -4[ecx]	#,
  86              		.cfi_def_cfa 4, 4
  87 003b C3       		ret
  88              		.cfi_endproc
  89              	.LFE0:
  91              		.section	.text.__x86.get_pc_thunk.ax,"axG",@progbits,__x86.get_pc_thunk.ax,comdat
  92              		.globl	__x86.get_pc_thunk.ax
  93              		.hidden	__x86.get_pc_thunk.ax
  95              	__x86.get_pc_thunk.ax:
  96              	.LFB1:
  97              		.cfi_startproc
  98 0000 8B0424   		mov	eax, DWORD PTR [esp]	#,
  99 0003 C3       		ret
 100              		.cfi_endproc
 101              	.LFE1:
 102              		.text
 103              	.Letext0:
 104              		.file 2 "/usr/lib/gcc/i686-linux-gnu/7/include/stddef.h"
 105              		.file 3 "/usr/include/i386-linux-gnu/bits/types.h"
 106              		.file 4 "/usr/include/i386-linux-gnu/bits/libio.h"
 107              		.file 5 "/usr/include/stdio.h"
 108              		.file 6 "/usr/include/i386-linux-gnu/bits/sys_errlist.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARRLEN(ARR) (sizeof(ARR) / sizeof(ARR[0]))
/*from rosettacode*/
#define ARRCONCAT(TYPE, A, An, B, Bn) \
    (TYPE *)arrconcat((const void *) (A), (An), (const void *) (B), (Bn), sizeof(TYPE))

void *arrconcat(const void *a, size_t an, const void *b, size_t bn, size_t s)
{
    char *p = malloc(s * (an + bn));
    memcpy(p, a, an * s);
    memcpy(p + an * s, b, bn * s);

    return p;
}

int main(void)
{
    const int arr1[] = {1, 3, 5};
    const int arr2[] = {2, 4, 6, 8};
/*
    const int arrlen = ARRLEN(arr1) + ARRLEN(arr2);
    int arr[arrlen];

    int *cursor = arr1;
    for (int i = 0; i < arrlen; i++) {
        if (i == ARRLEN(arr1)) {
            cursor = arr2;
        }
        arr[i] = *cursor++;
    }
*/
    const int arrlen = ARRLEN(arr1) + ARRLEN(arr2);
    int *arr = ARRCONCAT(int, arr1, ARRLEN(arr1), arr2, ARRLEN(arr2));

    for (int i = 0; i < arrlen; i++)
        printf("%d ", arr[i]);

    printf("\n");

    free(arr);
    return 0;
}

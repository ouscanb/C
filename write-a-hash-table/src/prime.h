#ifndef _PRIME_H
#define _PRIME_H

#include <stdbool.h>

bool is_prime(const size_t x);
size_t next_prime(size_t x);

#endif /* _PRIME_H */

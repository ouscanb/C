#include <stdio.h>

int factorial(int x);
void fibonacci(int x);
int sum_nums(int x);
void print_nums(int x);
void print_chars(char x, char y);
void countdown(int x,int dec);

int main(void) {
    int n, nfac;
    
    printf("Enter a positive integer for factorial calculation:\n");
    scanf("%d", &n);

    nfac = factorial(n);
    printf("Result of %d! is %d.\n", n, nfac);

    fibonacci(n);

    print_nums(n);
    print_chars('a', 'z');

    printf("Sum of numbers upto %d is %d.\n", n*10, sum_nums(n*10));

    countdown(n, 1);

    return 0;
}

int factorial(int x) {
    int xfac = 1;
    
    if (x == 0) {
        return xfac;
    } else {
        while (x >= 1) {
            xfac *= x;
            x--;
        }
    }

    return xfac;
}

void fibonacci(int x) {
    int i = 0;
    int j = 0, k = 1;
    
    int m; 
    for(m = 0; m < x; m++) {
        printf("%d ", i);
        i += k;
        k = j;
        j = i;
    }

    printf("\n");
}

int sum_nums(int x) {
    int sum = 0;

    /*
    int i = 1;

    do {
        sum += i;
        i++;
    } while (i <= x);
    */
    
    int k;
    for (k = 1; k <= x; k++) {
        sum += k;
    }

    return sum;
}

void print_nums(int x) {
    int i = 1;

    do {
        printf("%d ", i);
        i++;
    } while (i <= x);
    
    printf("\n");
}

void print_chars(char x, char y) {
    do {
        printf("%c:%d ", x, (int) x);
        x++;
    } while (x <= y);

    printf("\n");
}

void countdown(int x, int dec) {
    do {
        printf("%d ", x);
        x -= dec;
    } while (x >= 0);

    printf("\n");
}

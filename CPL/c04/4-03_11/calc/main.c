#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "calc.h"

#define BUFOP   128

/* store the most recent variable name */
int var = 0;

int main(void)
{
    int optype;
    double op2;
    char s[BUFOP];

    while ((optype = getop(s)) != EOF) {
        switch (optype) {
            case NUMBER:
                push(atof(s));
                break;
            case FUNC:
                push(func(s));
                break;
            case VAR:
                var = s[0];
                if (checkvar(var))
                    push(getvar(var));
                break;
            case '=':
                op2 = pop();
                setvar(var, op2);
                push(op2);
                break;
            case '+':
                push(pop() + pop());
                break;
            case '*':
                push(pop() * pop());
                break;
            case '-':
                op2 = pop();
                push(pop() - op2);
                break;
            case '/':
                op2 = pop();
                if(op2 == 0.0) {
                    fprintf(stderr, "Division by zero!\n");
                    exit(EXIT_FAILURE);
                }
                push(pop() / op2);
                break;
            case '%':
                op2 = pop();
                if(op2 == 0.0) {
                    fprintf(stderr, "Division by zero!\n");
                    exit(EXIT_FAILURE);
                }
                push(fmod(pop(), op2));
                break;
            case '\n':
                printf("\t%.8g\n", pop());
                break;
            default:
                fprintf(stderr, "Unknown command %s\n", s);
                exit(EXIT_FAILURE);
                break;
        }
    }

    return EXIT_SUCCESS;
}

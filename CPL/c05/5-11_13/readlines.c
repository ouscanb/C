#include <stdio.h>
#include <string.h>
#include <sys/types.h>

#define MAXLEN 1024

extern size_t getline(char *, size_t);
extern char *alloc(size_t);

ssize_t readlines(char *lineptr[], size_t maxlines)
{
    char line[MAXLEN] = {0};
    size_t nlines = 0;

    size_t len;
    while ((len = getline(line, MAXLEN)) > 0) {
        char *lptr = alloc(len);
        if (lptr && nlines < maxlines) {
            /* line[strcspn(line, "\n")] = '\0'; */
            line[len - 1] = '\0';
            strncpy(lptr, line, len);
            lineptr[nlines++] = lptr;
        } else {
            if (nlines >= maxlines)
                fprintf(stderr, "%s failed: Array is full!\n", __func__);
            return -1;
        }
    }

    return nlines;
}

void writelines(char *lineptr[], size_t maxlines)
{
    while (maxlines-- >0) {
        printf("%s\n", *lineptr++);
    }
}

#include <stdio.h>

void thanoi(int n, char speg, char apeg, char dpeg) {
    /* speg: source peg
     * apeg: auxilary peg
     * dpeg: destination peg
     */
    if (n == 1) {
        printf("Move disk %d from %c to %c.\n", n, speg, dpeg);
        return;
    }

    thanoi((n - 1), speg, dpeg, apeg);
    printf("Move disk %d from %c to %c.\n", n, speg, dpeg);

    thanoi((n - 1), apeg, speg, dpeg);
    return;
}

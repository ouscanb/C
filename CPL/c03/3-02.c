#include <stdio.h>

#define BUF 128

static void escape(char s[], char t[], size_t tlen)
{
    size_t i, j;
    for (i = j = 0; s[i] != '\0' && j < tlen - 1; ++i, ++j) {
        switch (s[i]) {
            case '\n':
                t[j++] = '\\';
                t[j] = 'n';
                break;
            case '\t':
                t[j++] = '\\';
                t[j] = 't';
                break;
            default:
                t[j] = s[i];
                break;
        }
    }
    t[j] = '\0';

    return;
}

static void epacse(char s[], char t[], size_t tlen)
{
    int cp = '\0', c;
    size_t i, j;
    for (i = j = 0; (c = s[i]) != '\0' && j < tlen - 1; ++i, ++j) {
        if (cp == '\\') {
            switch (c) {
                case 'n':
                    t[--j] = '\n';
                    break;
                case 't':
                    t[--j] = '\t';
                    break;
                default:
                    t[j] = s[i];
                    break;
            }
        } else
            t[j] = s[i];
        cp = c;
    }
    t[j] = '\0';

    return;
}

static int getline(char s[], size_t slen)
{
    int c;
    size_t i;
    for (i = 0; i < slen - 1 && (c = getchar()) != '\n' && c != EOF; ++i)
        s[i] = c;
    if (c == '\n')
        s[i++] = c;
    s[i] = '\0';

    return i;
}

int main(void)
{
    char line[BUF];
    char line_esc[BUF];
    while (getline(line, BUF) > 0) {
        escape(line, line_esc, BUF);
        printf("Escaped:\n%s", line_esc);
        puts("");
        epacse(line, line_esc, BUF);
        printf("Epacsed:\n%s", line_esc);
    }

    return 0;
}

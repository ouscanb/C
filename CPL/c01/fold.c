#include <stdio.h>
#include "detab.h"

#define BUF     1024
#define MARGIN  40

int getline(char s[], int slen);
void fold(char s[], int n, char c);
void print_margin(int n, char c);

int main(void)
{
    int showmargin = 1;

    char line[BUF];
    while(detab(line, BUF) > 0) {
        if(showmargin) {
            print_margin(MARGIN, '|');
            showmargin = 0;
        }

        fold(line, MARGIN, '|');
    }

    return 0;
}

int getline(char s[], int slen)
{
    int i, c;
    for (i = 0; i < slen - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
        s[i] = c;
    if (c == '\n')
        s[i++] = c;
    s[i] = '\0';

    return i;
}

void fold(char s[], int n, char c)
{
    int i = 0, j = 0, newline = 0;
    while (s[i]) {
        if (newline) {
            putchar(c);
            puts("");
        } else {
            putchar(s[i++]);
        }
        newline = (j % n == 0) ? 1 : 0;
        ++j;
    }
}

void print_margin(int n, char c)
{
    while (n-- > 0)
        putchar(' ');
    putchar(c);

    return;
}

#include <stdio.h>
#include "calc.h"

#define BUFSIZE 128

static char buf[BUFSIZE];  /* buffer for ungetch */
static int bufpos = 0;     /* next free position in buf */

int getch(void )
{
    return (bufpos > 0) ? buf[--bufpos] : getchar();
}

void ungetch(int c)
{
    if (bufpos >= BUFSIZE)
        fprintf(stderr, "ungetch: too many characters!\n");
    else
        buf[bufpos++] = c;
}

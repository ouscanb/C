#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <time.h>

static void init_arr(uint64_t arr[], uint64_t limit)
{
	for (size_t i = 0; i < limit; ++i)
		arr[i] = i;
}

static void print_arr(uint64_t arr[], size_t arrlen)
{
	for (size_t i = 0; i < arrlen; ++i)
		if (arr[i] != 0 && arr[i] != 1)
			printf("%lu ", arr[i]);
	printf("\n");
}

static void sieve(uint64_t arr[], uint64_t limit)
{
	init_arr(arr, limit);

	for (size_t i = 0; i < limit; ++i) {
		uint64_t n = arr[i];
		if (n == 0 || n == 1)
			continue;
		for (size_t j = 0; j < limit; ++j) {
			n = arr[i] * (j + 2);
			if (n < limit) {
				arr[n] = 0;
			} else
				break;
		}
	}
}

static void sieve2(uint64_t arr[], uint64_t limit)
{
	init_arr(arr, limit);

	for (size_t i = 0; (double)i < sqrt(limit); ++i) {
		uint64_t n = arr[i];
		if (n == 0 || n == 1)
			continue;
		n = arr[i] * arr[i];
		for (size_t j = 0; j < limit; ++j) {
			if (n < limit) {
				arr[n] = 0;
			} else
				break;
			n = arr[i] * (j + 2);
		}
	}
}

int main(void)
{
	uint64_t limit = 1000000,
			 arr_sieve[limit];

	clock_t st, ed;
	double t1, t2;

	st = clock();
	sieve(arr_sieve, limit);
	ed = clock();
	t1 = (double)(ed - st) / CLOCKS_PER_SEC;
	/* print_arr(arr_sieve, limit); */

	st = clock();
	sieve2(arr_sieve, limit);
	ed = clock();
	t2 = (double)(ed - st) / CLOCKS_PER_SEC;
	/* print_arr(arr_sieve, limit); */


	printf("sieve1: %f\nsieve2: %f\n", t1, t2);

	return EXIT_SUCCESS;
}

#include <stdio.h>

#define DARRL 5 /* default array length */

void swap(int *n1, int *n2);
void swap2(int n1, int n2);
int squared(int *n);

int main(void) {
    int x, *p1, *p2;
    int n1 = 1, n2 = 2;
    int arr[DARRL] = {12, 23, 35, 47, 58};

    x = 11;
    p1 = &x;
    p2 = NULL;

    printf("Value of   x:         %d\n", x); 
    printf("Address of x:         %p\n", &x); 
    printf("Address of p1:        %p\n", &p1); 
    printf("Value of   p1:        %p\n", p1); 
    printf("Value of  *p1:        %d\n", *p1);
    printf("Value of   p2(NULL):  %p\n", p2);
    printf("Value of   p2(NULL):  %d\n", (int) p2);

    printf("\nSwap two integers:\n");
    //printf("Address of n1:   %p\n", &n1); 
    //printf("Address of n2:   %p\n", &n2); 
    printf("Before: n1 = %d, n2 = %d\n", n1, n2);
    swap(&n1, &n2);
    //swap2(n1, n2);
    printf("After : n1 = %d, n2 = %d\n", n1, n2);
    
    int n3 = 3;
    printf("\nValue  of n3 is %d\n", n3);
    printf("Square of %d is %d\n", n3, squared(&n3));
    printf("Value  of n3 is %d\n", n3);

    int i, *p3;
    p3 = arr;
    /*Equivalently p3 = &arr[0]; can be used
     * since arr is already a pointer which
     * point to &arr[0].
     */
    for (i = 0; i < DARRL; i++) {
        printf("\nValue of   arr[%d]: %d\n", i, *p3);
        printf("Address of arr[%d]: %p\n", i, p3);
        p3++;
    }

    int arr01[DARRL];
    int sum = 0;

    printf("\nEnter %d numbers:\n", DARRL);
    for (i = 0; i < DARRL; i++) {
        scanf("%d", (arr01 + i));
        /* OR
         * scanf("%d ", &arr01[i]);
         */
        sum += *(arr01 + i);
        /* OR
         * sum += arr01[i];
         */
    }
    printf("Sum of the numbers is %d\n", sum);

    return 0;
}

void swap(int *n1, int *n2) {
    int t;

    t = *n1;
    *n1 = *n2;
    *n2 = t;

    return;
}

void swap2(int n1, int n2) {
    int t;

    printf("Address of n1:   %p\n", &n1); 
    printf("Address of n2:   %p\n", &n2);
    /* n1 and n2 here are different than the ones 
     * defined in the main function. Even though 
     * they are passed as arguments to the swap2 
     * function, they don't have the same memory 
     * addresses. 
     */
    t = n1;
    n1 = n2;
    n2 = t;

    return;
}

int squared(int *n) {
    *n = *n * *n;
    return *n;
}

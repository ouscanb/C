/*
 * KnR exercise 1-22 solution, by Chris Chadwick - big10p1967@gmail.com
 *
 * NOTE: This is a 'debug' version that outputs spaces and tabs as visible characters, so that
 * it can be seen that the program is functioning correctly. Spaces are displayed as underscores "_"
 * and tabs as a sequence of periods "." - also, a bar (of hashes "#") is displayed to show the
 * width all output will be wrapped to.
 */

#include <stdio.h>

#define TAB_SIZE        8
#define OUTPUT_WIDTH    (TAB_SIZE * 3)
#define TRUE            1
#define FALSE           0

int outcol = 1; /* Column position of where the next char will be output. Range = 1 to OUTPUT_WIDTH */

void wrapchar(char c);
void wrapword(char word[], int len);

int main ()
{
    int c, i;
    char word[OUTPUT_WIDTH];    /* Buffers word currently being read from input */
    int wordi = 0;              /* Index of word[] to store next non-blank character of input */
    int showcols = TRUE;        /* Flag to indicate if output wrap width bar needs displaying */
    int overflow = FALSE;       /* Flag to indicate overflow state of word buffer (word[]) */
    extern int outcol;

    while ((c = getchar()) != EOF)
    {
        if (showcols)
        {
            /* Print bar showing width of output to wrap to */
            for (i = OUTPUT_WIDTH; i; --i)
                putchar('#');
            putchar('\n');
            showcols = FALSE;
        }

        if (c == ' ')
        {
            if (wordi)
            {
                /* A word is currently being read into the buffer.
                 * This space signals the end of that word */
                wrapword(word, wordi);
                wordi = 0;
            }

            wrapchar('_'); /* Dummy space, to make it visible */
            overflow = FALSE;
        }
        else if (c == '\t')
        {
            if (wordi)
            {
                /* A word is currently being read into the buffer.
                 * This tab signals the end of that word */
                wrapword(word, wordi);
                wordi = 0;
            }

            /* Calculate spaces to next tab-stop */
            int spaces = TAB_SIZE - (outcol % TAB_SIZE) + 1;

            if ((outcol += spaces) > OUTPUT_WIDTH)
            {
                /* This tab has taken us past line end, so wrap */
                putchar('\n');
                outcol = TAB_SIZE + 1; /* Tab to next stop on the new line */
                spaces = TAB_SIZE;
            }

            /* Output a dummy tab, to make it visible */
            for (i = 1; i <= spaces; ++i)
                putchar('.');

            overflow = FALSE;
        }
        else if (c == '\n')
        {
            if (wordi)
                /* A word is currently being read into the buffer.
                 * This newline signals the end of that word */
                wrapword(word, wordi);

            putchar(c);

            outcol = 1;
            wordi = 0;
            showcols = TRUE;
            overflow = FALSE;
        }
        else
        {
            /* c is a non-blank char, so considered part of a word */
            if (wordi == OUTPUT_WIDTH)
            {
                /* This 'word' has overflowed the word buffer, so is longer than an entire output line! */
                /* We will simply flush the word buffer, breaking it up to wrap to the output */
                for (i = 0; i < OUTPUT_WIDTH; ++i)
                    wrapchar(word[i]);

                wrapchar(c);
                wordi = 0;
                overflow = TRUE; /* Signal that the word buffer is currently overflowing */
            }
            else
            {
                if (overflow)
                {
                    /* Currently in word buffer overflow state, so simply output current char */
                    wrapchar(c);
                }
                else
                {
                    /* Continue buffering word being read from input */
                    word[wordi++] = c;
                }
            }
        }
    }

    return 0;
}

/* Outputs the given char, wrapping as required */
void wrapchar(char c)
{
    extern int outcol;

    if (outcol > OUTPUT_WIDTH)
    {
        /* Wrap to new line */
        putchar('\n');
        outcol = 1;
    }

    putchar(c);
    ++outcol;
}

/* Outputs the given word, wrapping as required */
void wrapword(char word[], int len)
{
    extern int outcol;

    if ((outcol + (len - 1)) > OUTPUT_WIDTH)
    {
        /* Word won't fit on current line, so wrap it to next line */
        putchar('\n');
        outcol = 1;
    }

    for (int i = 0; i < len; ++i)
    {
        putchar(word[i]);
        ++outcol;
    }
}

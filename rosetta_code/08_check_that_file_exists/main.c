#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>

void usage(void)
{
    puts("Usage: main [file|directory]");

    return;
}

void check_file(const char *path)
{
    /* if (access(argv[1], F_OK) != -1) { ... } */

    struct stat filestat;
    if ((stat(path, &filestat)) != -1) {
        if (S_ISREG(filestat.st_mode))
            puts("File exists.");
        else if (S_ISDIR(filestat.st_mode))
            puts("Directory exists.");
    } else
        puts("File/directory does not exist.");

    return;
}

/* http://rosettacode.org/wiki/Check_that_file_exists#C */
/* Check for regular file */
int check_reg(const char *path)
{
    struct stat sb;
    return stat(path, &sb) == 0 && S_ISREG(sb.st_mode);
}

int main(int argc, char *argv[])
{
    if (argc != 2) {
        usage();

        exit(EXIT_FAILURE);
    }

    check_file(argv[1]);

    return EXIT_SUCCESS;
}

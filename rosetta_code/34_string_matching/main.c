#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int isstw(const char haystack[], const char needle[])
{
    /* char *s = strstr(haystack, needle); */
    /* return s ? s == haystack : 0; */
    size_t hlen = strlen(haystack),
           nlen = strlen(needle);
    return hlen > nlen ? strncmp(haystack, needle, nlen) == 0 : 0;
}

int isedw(const char haystack[], const char needle[])
{
    /* char *s = strstr(haystack, needle); */
    /* return s ? *(s + strlen(needle)) == '\0' : 0; */
    size_t hlen = strlen(haystack),
           nlen = strlen(needle);
    return hlen > nlen ? strncmp(haystack + (hlen - nlen), needle, nlen) == 0 : 0;
}

int iscon(const char haystack[], const char needle[])
{
    return strstr(haystack, needle) != NULL;
}

int main(int argc, char *argv[])
{
    if (argc != 3) {
        puts("Usage: main [str1] [str2]");
        exit(EXIT_FAILURE);
    }

    printf("%s %s %s\n",
            argv[1],
            isstw(argv[1], argv[2]) ? "starts with" :
            isedw(argv[1], argv[2]) ? "ends with" :
            iscon(argv[1], argv[2]) ? "contains" : "does not contain",
            argv[2]);

    return EXIT_SUCCESS;
}

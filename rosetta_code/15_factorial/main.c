#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <errno.h>
#include <limits.h>

#define N       5UL
#define ACCSIZE 100

/* Check out:
 * https://stackoverflow.com/questions/19452971/array-size-macro-that-rejects-pointers */
#define ARRLEN(ARR) (sizeof(ARR) / sizeof(ARR[0]))
#define MAX(A, B)   (((A) > (B)) ? (A) : (B))

long long factorial_iter(int n);
long long factorial_rec(int n);
long long factorial_tail_rec(int n);
long long factorial_tail_rec_aux(int n, long long acc);
int *factorial_big_num(int arr[], size_t arr_len, int n);
int *arr_mult(int arr[], size_t arr_len, int n);
int *arr_carry(int arr[], size_t arr_len);
void arr_print(int arr[], size_t arr_len);

int main(int argc, char *argv[])
{
    int n = (argc > 1) ? strtol(argv[1], NULL, 10) : N;
    if (errno == ERANGE) {
        perror(NULL);
        return EXIT_FAILURE;
    }

    /* assert(n < 21); */
    printf("factorial_reg(%d) = %lld\n", n, factorial_tail_rec(n));

    int fac[ACCSIZE], *pfac;
    pfac = factorial_big_num(fac, ACCSIZE, n);
    if (pfac) {
        printf("factorial_big(%d) = ", n);
        arr_print(fac, ACCSIZE);
    } else {
        printf("factorial_big(%d) = ", n);
        printf("-1\n");
    }

    return EXIT_SUCCESS;
}

long long factorial_iter(int n)
{
    if (n < 0)
        return -1;

    long long acc = 1;
    while (n > 0)
        acc *= n--;

    return acc;
}

long long factorial_rec(int n)
{
    /*
    if (n < 0)
        return -1;
    else if (n == 0)
        return 1;
    else
        return n * factorial_rec(n - 1);
    */

    return (n < 0) ? -1 : (n == 0) ? 1 : n * factorial_rec(n - 1);
}

long long factorial_tail_rec(int n)
{
    return factorial_tail_rec_aux(n, 1);
}

long long factorial_tail_rec_aux(int n, long long acc)
{
    return (n < 0) ? -1 : (n == 0) ? acc : factorial_tail_rec_aux(n - 1, acc * n);
}

int *factorial_big_num(int arr[], size_t arr_len, int n)
{
    if (n < 0)
        return NULL;

    for (int i = 0; i < arr_len; i++)
        arr[i] = 0;

    arr[arr_len - 1] = 1;

    while (n > 1) {
        arr_mult(arr, arr_len, n);
        arr_carry(arr, arr_len);
        n--;
    }

    return arr;
}

int *arr_mult(int arr[], size_t arr_len, int n)
{
    /* {1, 2, 3} * 2 -> {2, 4, 6} */
    for (int i = 0; i < arr_len; i++)
        arr[i] *= n;

    return arr;
}

int *arr_carry(int arr[], size_t arr_len)
{
    /* {0, 13, 45, 1} -> {0,17, 5, 1} -> {1, 7, 5, 1} */
    for (int i = arr_len - 1; i > 0; i--) {
        int n = arr[i];
        arr[i] = n % 10;
        arr[i - 1] += n / 10;
        if (i == 1 && (arr[i - 1]) >= 10) {
            puts("Array is full. Incorrect answer!");
            return arr;
        }
    }

    return arr;
}

void arr_print(int arr[], size_t arr_len)
{
    int i = 0;
    while (arr[i] == 0 && i < arr_len)
        ++i;
    while (i < arr_len)
        printf("%d", arr[i++]);
    printf("\n");

    return;
}

#!/bin/bash

gcc -o main main.c -O0 -g -Wall -Wextra -Wpedantic -Werror -lm
./main
pnmtotiff image.ppm >| image.tiff
sxiv image.tiff

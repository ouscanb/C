#include <stdio.h>
#include <ctype.h>

#define ARRLEN(X) ((sizeof (X)) / (sizeof(X[0])))

double atof(char s[]);

int main(void)
{
    char *s[] = {
        "0.12345", "123.456", "    -1.234",
        "+12345.7  ", "+123", "-1234", "1.23e4",
        "1.234E-5", "12e-3", "12E+3",
        "1.234E", "12e-", "12E+","abc"
    };
    size_t slen = ARRLEN(s);
    for (size_t i = 0; i < slen; ++i)
        printf("'%s' = %f\n", s[i], atof(s[i]));

    return 0;
}

double atof(char s[])
{
    size_t i;
    for (i = 0; isspace(s[i]); ++i)
        ;

    int sign = (s[i] == '-') ? -1 : 1;
    if (s[i] == '-' || s[i] == '+')
        ++i;

    /* integer part */
    double val = 0.0;
    while (isdigit(s[i]))
        val = val * 10.0 + (s[i++] - '0');

    /* decimal part */
    double decim = 1.0;
    if (s[i] == '.')
        while (isdigit(s[++i])) {
            val = val * 10.0 + (s[i] - '0');
            decim *= 10.0;
        }

    /* scientific part */
    double sci = 1.0;
    if (s[i] == 'e' || s[i] == 'E') {
        ++i;
        int esign = s[i] == '-' ? -1 : 1;
        if (s[i] == '-' || s[i] == '+')
            ++i;
        int epower = 0;
        while (isdigit(s[i]))
            epower = epower * 10 + (s[i++] - '0');
        while (epower--)
            if (esign < 0)
                sci /= 10.0;
            else
                sci *= 10.0;
    }

    return sign * (val / decim) * sci;
}

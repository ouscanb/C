#include <stdio.h>
#include <time.h>

int main(void) {
    time_t e_sec; 
    time_t d_now; 
    time_t start, finish;
    volatile unsigned long c;

    /* epoch time in seconds */
    start = time(NULL);
    
    e_sec = time(NULL);
    printf("Time elapsed in since Jan 1, 1970 is %ld seconds.\n", e_sec);
    printf("Time elapsed in since Jan 1, 1970 is %ld days.\n", (e_sec / (24 * 60 * 60)));
    printf("Time elapsed in since Jan 1, 1970 is %ld years.\n", (e_sec / (365 * 24 * 60 * 60)));
    
    d_now = time(NULL);
    /* date of today using ctime */
    printf("\nToday is %s\n", ctime(&d_now));

    for (c = 0; c < 1000000000; c++) {}
    finish = time(NULL);

    printf("Total execution time is %f seconds.\n", difftime(finish, start));

    return 0;
}

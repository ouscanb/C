#include <stdio.h>
#include <string.h>

#define USER_FMT   "%s,%s,%s,%s,%s"
#define PASSWD_FMT "%s:%s:%d:%d:"USER_FMT":%s:%s"

typedef struct {
	const char *fullname,
		  *office,
		  *extension,
		  *homephone,
		  *email;
} user_t;

typedef struct {
	const char *account,
		  *password;
	int uid,
		gid;
	user_t info;
	const char *directory,
		  *shell;
} passwd_t;

int fprintf_passwd(FILE *fp, const char *fmt, passwd_t pass)
{
		int ret = fprintf(
				fp, PASSWD_FMT"\n",
				pass.account,
				pass.password,
				pass.uid,
				pass.gid,
				pass.info.fullname,
				pass.info.office,
				pass.info.extension,
				pass.info.homephone,
				pass.info.email,
				pass.directory,
				pass.shell
				);
		return ret;
}

passwd_t passwd_list[]={
	{"jsmith", "x", 1001, 1000,
		{"Joe Smith", "Room 1007", "(234)555-8917", "(234)555-0077", "jsmith@rosettacode.org"},
		"/home/jsmith", "/bin/bash"},
	{"jdoe", "x", 1002, 1000,
		{"Jane Doe", "Room 1004", "(234)555-8914", "(234)555-0044", "jdoe@rosettacode.org"},
		"/home/jdoe", "/bin/bash"}
};

int main(void)
{
	char filepath[] = "passwd.txt";
	FILE *passwd_file = fopen(filepath, "w");
	for (size_t rec_num = 0; rec_num < sizeof(passwd_list) / sizeof(passwd_t); ++rec_num)
		fprintf_passwd(passwd_file, PASSWD_FMT"\n", passwd_list[rec_num]);
	fclose(passwd_file);

	passwd_file = fopen(filepath, "a+");

	passwd_t new = {
		"xyz", "x", 1003, 1000,
		{"X. Yz", "Room 1001", "(234)555-8911", "(234)555-0011", "xyz@rosettacode.org"},
		"/home/xyz", "/bin/bash"
	};

	fprintf_passwd(passwd_file, PASSWD_FMT"\n", new);
	fclose(passwd_file);

	enum { BUF = 1024 };
	char line[BUF];
	passwd_file = fopen(filepath, "r");
	while (fgets(line, BUF, passwd_file)) {
		if (strstr(line, "xyz"))
			printf("Appended: %s\n", line);
	}

	return 0;
}

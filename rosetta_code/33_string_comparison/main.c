#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#define STREQ(A, B) (0 == strcmp((A), (B)))
#define STRNE(A, B) (!STREQ(A, B))
#define STRLT(A, B) (strcmp((A), (B)) < 0)
#define STRLE(A, B) (strcmp((A), (B)) <= 0)
#define STRGT(A, B) STRLT(B, A)
#define STRGE(A, B) STRLE(B, A)

#define STRCEQ(A, B) (0 == strcasemp((A), (B)))
#define STRCNE(A, B) (!STREQ(A, B))
#define STRCLT(A, B) (strcasemp((A), (B)) < 0)
#define STRCLE(A, B) (strcasemp((A), (B)) <= 0)
#define STRCGT(A, B) STRLT(B, A)
#define STRCGE(A, B) STRLE(B, A)

int main(int argc, char *argv[])
{
    if (argc != 3) {
        puts("Usage: main [str1] [str2]");
        exit(EXIT_FAILURE);
    }

    printf("%s is semantically %s %s.\n",
            argv[1],
            (STREQ(argv[1], argv[2]) ? "equal to" :
             STRLT(argv[1], argv[2]) ? "ordered before" : "ordered after"),
            argv[2]);

    return EXIT_SUCCESS;
}

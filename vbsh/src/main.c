/*
 * A very simple shell implementation with a few builtin functions
 * https://www.brennan.io/2015/01/16/write-a-shell-in-c
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#define PWD			"> "
#define TOKENCAP	16
#define TOKENDELIM	" \n\r\t\a"

static char *vbsh_read_line(void);
static char **vbsh_tokenize_line(char *line);
static int vbsh_launch(char **args);
static int vbsh_exec(char **args);
static int vbsh_cd(char **args);
static int vbsh_help(char **args);
static int vbsh_exit(char **args);
static int vbsh_num_builtins(void);

struct builtin {
	const char *name;
	int (*func)(char **args);
};

struct builtin builtins[] = {
	{"cd",   vbsh_cd},
	{"help", vbsh_help},
	{"exit", vbsh_exit}
};

int main(int argc, char *argv[])
{
	(void) argc;
	(void) argv;

	int status;
	do {
		printf(PWD);

		char *line = vbsh_read_line();
		char **tokens = vbsh_tokenize_line(line);

		status = vbsh_exec(tokens);

		free(tokens);
		free(line);
	} while (!status);

	return EXIT_SUCCESS;
}

static char *vbsh_read_line(void)
{
	char *line = NULL;
	size_t bufsize = 0;

	while (getline(&line, &bufsize, stdin) == -1) {
		if (feof(stdin)) {
			exit(EXIT_SUCCESS);
		} else {
			perror("vbsh: readline");
			exit(EXIT_FAILURE);
		}
	}

	return line;
}

static char **vbsh_tokenize_line(char *line)
{
	int token_capacity = TOKENCAP;
	char **tokens = malloc(sizeof(*tokens) * token_capacity);
	if (!tokens) {
		perror("vbsh");
		exit(EXIT_FAILURE);
	}

	const char *delimiters = TOKENDELIM;
	char *token = strtok(line, delimiters);

	int i = 0;
	while (token != NULL) {
		tokens[i++] = token;
		
		if (i >= token_capacity) {
			token_capacity += TOKENCAP;
			tokens = realloc(tokens, sizeof(*tokens) * token_capacity);
			if (!tokens) {
				perror("vbsh");
				exit(EXIT_FAILURE);
			}
		}
		token = strtok(NULL, delimiters);
	}
	tokens[i] = NULL;

	return tokens;
}

static int vbsh_launch(char **args)
{
	pid_t child_pid = fork();

	if (child_pid == 0) {
		if (execvp(args[0], args) == -1) {
			perror("vbsh");
		}
		exit(EXIT_FAILURE);
	} else if (child_pid > 0) {
		int status;
		do {
			waitpid(child_pid, &status, WUNTRACED);
		} while (!WIFEXITED(status) && !WIFSIGNALED(status));
	} else {
		perror("vbsh");
	}

	return 0;
}

static int vbsh_exec(char **args)
{
	if (args[0] == NULL) {
		return 1;
	}

	for (int i = 0; i < vbsh_num_builtins(); i++) {
		if (strcmp(args[0], builtins[i].name) == 0) {
			return builtins[i].func(args);
		}
	}

	return vbsh_launch(args);
}

static int vbsh_num_builtins(void) {
	return sizeof(builtins) / sizeof(builtins[0]);
}

static int vbsh_cd(char **args)
{
	if (args[1] == NULL) {
		fprintf(stderr, "vbsh: cd: missing argument\n");
	} else {
		if (chdir(args[1]) != 0) {
			perror("vbsh: cd");
		}
	}

	return 0;
}

static int vbsh_help(char **args)
{
	(void) args;

	printf(
			"vbsh - Very Basic Shell. "
			"Builtin commands:\n"
			"  cd    Change the working directory\n"
			"  exit  Exit the shell\n"
			"  help  Display this help.\n"
		  );

	return 0;
}

static int vbsh_exit(char **args)
{
	(void) args;
	return 1;
}

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

typedef struct stack {
    int val;

    struct stack *next;
} node_s;

node_s *create(int n);
node_s *push(node_s **head, node_s *nd);
void pop(node_s **head);
/*node_s *pop(node_s **head);*/
node_s *random_list(int nrand);
void print_list(node_s *stack);
void free_list(node_s **head);
void menu(void);

int main(void) {

    char c;
    int r,val;
    bool q = false;

    node_s *stack = NULL;
    while (!q) {

        menu();

        printf(">>");
        c = getchar();
        printf("\n");

        switch(c) {
            case '1':
                printf("Enter an integer: ");
                scanf("%d", &val);
                
                push(&stack, create(val));

                break;

            case '2':
                pop(&stack);

                break;

            case '3':
                print_list(stack);
                printf("\n");

                break;

            case '4':
                printf("Enter the length of the stack: ");
                scanf("%d", &r);

                if (stack != NULL) free_list(&stack);

                stack = random_list(r);

                break;

            case '0':
                printf("\nGoodbye.\n");
                
                q = true;
                free_list(&stack);

                break;

            default:
                printf("Unrecognized option.\n");
        }
        getchar();
    }

    return 0;
}

node_s *create(int n) {
    node_s *new;

    new = malloc(sizeof(node_s));
    if (new == NULL) {
        fprintf(stderr, "Error: Memory cannot be allocated!\n");

        exit(EXIT_FAILURE);
    }

    new->val = n;
    new->next = NULL;

    return new;
}

node_s *push(node_s **head, node_s *nd) {
    if (*head == NULL) {
        *head = nd;

        return *head;
    }
    
    nd->next = *head;
    *head = nd;

    return *head;
}

void pop(node_s **head) {
    if (*head == NULL) {
        printf("Empty stack.\n");

        return;
    }
    
    node_s *nd;

    nd = *head;
    *head = (*head)->next;
    nd->next = NULL;
    free(nd);

    return;
}

/*
node_s *pop(node_s **head) {
    if (*head == NULL) return NULL;
    
    node_s *nd;

    nd = *head;
    *head = (*head)->next;
    nd->next = NULL;

    return nd;
}
*/

/*
node_s *random_list(int nrand) {
    int i, r;
    static int t = 1;
    time_t s;

    s = time(NULL);
    srand((unsigned) s * t);
    t++;

    node_s *new, *prev = NULL; 
    for (i = 0; i < nrand; i++) {
        new = malloc(sizeof(node_s));
        if (new == NULL) {
            fprintf(stderr, "Error: Memory cannot be allocated!\n");

            exit(EXIT_FAILURE);
        }

        r = rand() % 100;
        new = push(&prev, create(r));
        prev = new;
    }
    new = prev;

    return new;
}
*/

node_s *random_list(int nrand) {
    int i, r;
    static int t = 1;
    time_t s;

    s = time(NULL);
    srand((unsigned) s * t);
    t++;

    node_s *head = NULL; 
    for (i = 0; i < nrand; i++) {
        r = rand() % 100;
        push(&head, create(r));
    }

    return head;
}
void print_list(node_s *head) {
    if (head == NULL) {
        printf("Empty stack.\n");

        return;
    }

    node_s *cursor = head;
    while (cursor != NULL) {
        printf("Value: %d\n", cursor->val);
        cursor = cursor->next;
    }

    return;
}

/*
void free_list(node_s **head) {
   node_s *cursor, *tmp;

   if (*head != NULL) {
       cursor = (*head)->next;
       (*head)->next = NULL;
       free(*head);

       while (cursor != NULL) {
           tmp = cursor->next;
           cursor->next = NULL;
           free(cursor);

           cursor = tmp;
       }
   }

   return;
}
*/

void free_list(node_s **head) {
    if (head == NULL) return;

    node_s *tmp;
    while (*head != NULL) {
        tmp = *head;
        *head = (*head)->next;
        tmp->next = NULL;
        free(tmp);
    }

    return;
}

void menu(void) {
    printf("\n");
    printf("1. Push onto the stack,\n");
    printf("2. Pop an item from the stack,\n");
    printf("3. Print the values in the stack,\n");
    printf("4. Create a stack with random numbers,\n");
    printf("\n");
    printf("0. Quit.\n");
}


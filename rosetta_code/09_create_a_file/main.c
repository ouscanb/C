#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

int usage(int status)
{
    printf("Usage: [option]... path...\n\
Create an empty file or directory with the given path.\n\
    -f      create regular file\n\
    -d      create directory\n\
    -h      display this help and exit\n");

    return status;
}

int main(int argc, char *argv[])
{
    /* char *path = (argc > 1) ? argv[1] : "test"; */

    int c, errflg = 0;
    FILE *fp;
    while ((c = getopt(argc, argv, "f:d:")) != -1) {
        switch (c) {
            case 'f':
                fp = fopen(optarg, "w");
                if (!fp) {
                    perror(optarg);
                    return EXIT_FAILURE;
                }
                if (fclose(fp)) {
                    perror(optarg);
                    return EXIT_FAILURE;
                }
                break;
            case 'd':
                if(mkdir(optarg, S_IRWXU)) {
                    perror(optarg);
                    return EXIT_FAILURE;
                }
                break;
            case 'h':
                usage(EXIT_SUCCESS);
                break;
            case ':':
                errflg++;
                break;
            case '?':
                errflg++;
                break;
        }
    }
    if (errflg || optind == 1)
        return usage(EXIT_FAILURE);

    return EXIT_SUCCESS;
}

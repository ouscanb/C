#include <stdio.h>

unsigned rightrot(unsigned x, size_t n);
unsigned getbits(unsigned x, size_t p, size_t n);

int main(void)
{
    unsigned n;
    scanf("%u", &n);
    printf("%u %u\n", n, rightrot(n, 4));
    printf("%u %u\n", n, rightrot(n, 3));
    printf("%u %u\n", n, rightrot(n, 2));
    printf("%u %u\n", n, rightrot(n, 1));
    printf("%u %u\n", n, rightrot(n, 0));

    return 0;
}

unsigned rightrot(unsigned x, size_t n)
{
    size_t p = 0; /* ind. of the leftmost non-zero bit */
    unsigned y = x;
    while ((y >>= 1) > 0u)
        ++p;

    n = n % (p + 1);
    return (x >> n) | (getbits(x, n - 1, n) << (p + 1 - n));
}

unsigned getbits(unsigned x, size_t p, size_t n)
{
    return (x >> (p + 1 -n)) & ~(~0u << n);
}

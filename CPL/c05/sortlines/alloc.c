#include <stddef.h>
#define ALLOCSIZE 1000

static char allocbuf[ALLOCSIZE];
static char *allocp = allocbuf;

char *alloc(size_t n)
{
    if (allocp + n <= allocbuf + ALLOCSIZE) {
        allocp += n;
        return allocp - n;
    } else
        return NULL;
}

void afree(char *p)
{
    if (p >= allocbuf && p <= allocp && p < allocbuf + ALLOCSIZE)
        allocp = p;
}

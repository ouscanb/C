#ifndef NOCURSES_H
#define NOCURSES_H

#define ESC    "\x1b"

#define BLACK   0
#define RED     1
#define GREEN   2
#define YELLOW  3
#define BLUE    4
#define MAGENTA 5
#define CYAN    6
#define WHITE   7

#define BLOCK_BLINK     1
#define BLOCK           2
#define UNDERLINE_BLINK 3
#define UNDERLINE       4
#define BAR_BLINK       5
#define BAR             6

#define TRUE    1
#define FALSE   0

struct termsize {
    int cols;
    int rows;
};

void wait();
void clrscr();
void gotoxy(int x, int y);
void setfontcolor(int color);
void setbgrcolor(int color);
void setfontbold(int status);
void setunderline(int status);
void setblink(int status);
void settitle(char const* title);
void setcurshape(int shape);
struct termsize gettermsize();
int getche();
void clrline();
void resetcolors();

#endif

#include <stdio.h>
#include <string.h>

char * rmnl(char str[]);
void greet(char str[]);
void copy_str(char str2[], char str1[]);
char * strchar(char str[], char ch);
/* OR equivalently:
char * rmnl(char *str);
void greet(char *str);
void copy_str(char *str2, char *str1);
char * strchar(char * str, char ch);
*/


int main(void) {
    char hi[20] = "Hello, world!";   
    char hi_copy[20];
    char name[20];

    printf("%s\n", hi);

    printf("Please, type in your name: ");
    fgets(name, 20, stdin);
    greet(rmnl(name));

    copy_str(hi_copy, hi);
    printf("\n%s\n", hi_copy);

    char verse[] = "All that is gold does not glitter,";
    char *p;

    p = strchar(verse, 't');
    printf("The position of %c: %d\n", 't', (int) (p - verse + 1));

    return 0;
}

char * rmnl(char str[]) {
    int len = strlen(str);
    if (len > 0 && str[len - 1] == '\n') {
        str[len - 1] = '\0';
    }

    return str;
}

void greet(char name[]) {
    printf("\nHello, %s.\n", name);

    return;
}

void copy_str(char str2[], char str1[]) {
    /* copy str1 into str2 */
    while (*str1) {
        *str2 = *str1;
        str1++;
        str2++;
    }
    *str2 = '\0';
}

char * strchar(char str[], char ch) {
    char *p;

    p = str;

    while (*p != '\0') {
        if (*p == ch) {
            return p;
        }
        p++;
    }

    return str - 1;
}
    

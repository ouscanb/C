#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>

/* https://stackoverflow.com/questions/8487986/file-macro-shows-full-path */
#ifdef __LINUX__
/* #define FILENAME (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__) */
#define FILENAME (strrchr("/", __FILE__, '/') + 1)
#else
#define FILENAME __FILE__
#endif

#define MAXLINES 4096

extern ssize_t readlines(char **, size_t);
extern void writelines(char **, size_t);
void usage(void);

int main(int argc, char *argv[])
{
    long tail_count = 10;
    while (--argc > 0 && (*++argv)[0] == '-') {
        int c;
        char *arg;
        while ((c = *++argv[0])) {
            switch (c) {
                case 'n':
                    arg = argv[1];
                    errno = 0;
                    tail_count = strtol(arg, NULL, 10);
                    if (errno == ERANGE || tail_count < 0) {
                        fprintf(stderr, "%s: invalid line count -- %s\n", FILENAME, arg);
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 'h':
                    usage();
                    exit(EXIT_SUCCESS);
                default:
                    fprintf(stderr, "%s: invalid option -- %c\n", FILENAME, c);
                    fprintf(stderr, "Try 'tail -h' for more information.\n");
                    exit(EXIT_FAILURE);
            }
        }
    }

    char *lines[MAXLINES];

    ssize_t lines_read = 0;
    if ((lines_read = readlines(lines, MAXLINES)) >= 0) {
        if (lines_read - (ssize_t) tail_count >= 0)
            writelines(lines + (lines_read - tail_count), tail_count);
        else
            writelines(lines, lines_read);
    }

    return lines_read >= 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}

void usage(void)
{
    printf("\
Usage: tail [OPTION]... [FILE]...\n\
Print the last 10 lines of FILE to standard output.\n\n\
    -n output the last n lines, instead of 10\n");
}

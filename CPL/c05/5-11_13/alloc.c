#include <stdio.h>
#define ALLOCSIZE 65536

static char allocbuf[ALLOCSIZE];
static char *allocptr = allocbuf;

char *alloc(size_t size)
{
    if (allocptr + size <= allocbuf + ALLOCSIZE) {
        char *retptr = allocptr;
        allocptr += size;
        return retptr;
    } else {
        fprintf(stderr, "%s failed: Storage is full!\n", __func__);
        return NULL;
    }
}

void afree(char *ptr)
{
    if (ptr >= allocbuf && ptr < allocbuf + ALLOCSIZE)
        allocptr = ptr;
}

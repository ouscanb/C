#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#define LINEBUF 1024

size_t getline(char line[], size_t len);
void usage(void);

int main(int argc, char *argv[])
{
    if (argc < 2) {
        usage();
        exit(EXIT_FAILURE);
    }

    char buffer[LINEBUF];
    size_t lineno = 0;
    bool except = false,
         number = false;

    int c;
    while (--argc > 0 && (*++argv)[0] == '-') {
        while ((c = *++argv[0])) {
            switch(c) {
                case 'x':
                    except = true;
                    break;
                case 'n':
                    number = true;
                    break;
                case 'h':
                    usage();
                    exit(EXIT_SUCCESS);
                default:
                    puts("Illegal option!");
                    usage();
                    exit(EXIT_FAILURE);
                    break;
            }
        }
    }

    while (getline(buffer, LINEBUF) > 0) {
        lineno++;
        if ((strstr(buffer, *argv) != NULL) != except) {
            if (number)
                printf("%02zu: ", lineno);
            printf("%s", buffer);
        }
    }

    return EXIT_SUCCESS;
}

size_t getline(char line[], size_t len)
{
    int c;

    size_t i;
    for (i = 0; i < len - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
        line[i] = c;
    if (c == '\n')
        line[i++] = c;
    line[i] = '\0';

    return i;
}

void usage(void)
{
    printf("\
Usage: find [OPTION]... PATTERN\n\
    -x exclude lines matching PATTERN\n\
    -n print line number with output lines\n");
}

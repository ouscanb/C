#ifndef _DISASSEMBLE_H
#define _DISASSEMBLE_H

uint16_t Disassemble8080(uint8_t *codebuffer, uint16_t pc);

#endif // _DISASSEMBLE_H

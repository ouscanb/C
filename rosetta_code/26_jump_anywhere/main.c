#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int n;
    printf("Enter an integer: ");
    scanf("%d", &n);

    /* The label must be literal, not computed at run time. */
    /* This won't work: */
    /* goto (x > 0 ? positive : negative); */
    if (n >= 0)
        goto pos;
    else if (n < 0)
        goto neg;

pos:
    puts("It's positive.");
    goto end;
neg:
    puts("It's negative.");

end:
    return EXIT_SUCCESS;
}

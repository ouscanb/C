#include <stdio.h>
#include <limits.h>
#include <float.h>

int main()
{
    printf("Storage size for int is %d bytes.\n", sizeof(int));
    printf("Min and max integer values are %d and %d.\n", INT_MIN, INT_MAX);
    printf("Storage size for short is %d bytes.\n", sizeof(short));
    printf("Storage size for long is %d bytes.\n", sizeof(long));
    printf("Storage size for float is %d bytes.\n", sizeof(float));
    printf("Min and max positive float values are %E and %E.\n", FLT_MIN, FLT_MAX);
    printf("Storage size for double is %d bytes.\n", sizeof(double));
    printf("Storage size for long double is %d bytes.\n", sizeof(long double));
    
    // explicit conversion
    int a = 5, b = 2;
    double c;
    float d;
    
    // type-cast int into double then do implicit conversion
    // double a / int b -> double c
    c = (double) a / b;
    d = a / b;
    printf("\nValue of c after explicit conv. is %f.\n", c);
    printf("Value of c after implicit conv. is %f.\n", d);

    return 0;
}

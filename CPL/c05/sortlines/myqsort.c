#include <string.h>

static void swap(char *v[], size_t i, size_t j)
{
    char *t = v[i];
    v[i] = v[j];
    v[j] = t;
}

void myqsort(char *v[], size_t left, size_t right)
{
    size_t i, last;

    if (left >= right)
        return;

    swap(v, left, (left + right) / 2);

    last = left;
    for (i = left + 1; i <= right; ++i)
        if (strcmp(v[i], v[left]) < 0)
            swap(v, ++last, i);

    swap(v, left, last);
    myqsort(v, left, last - 1);
    myqsort(v, last + 1, right);
}

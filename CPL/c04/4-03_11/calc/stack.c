#include <stdio.h>
#include "calc.h"

#define BUFSVAL 128

static size_t spos = 0;        /* stack position */
static double sval[BUFSVAL];   /* value stack*/

void push(double f)
{
    if (spos < BUFSVAL)
        sval[spos++] = f;
    else
        fprintf(stderr, "Stack is full! Can' t push %g \n", f);
}

double pop(void)
{
    if (spos > 0)
        return sval[--spos];
    else {
        fprintf(stderr, "Stack is empty!\n");
        return 0.0;
    }
}

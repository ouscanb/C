#include <stdio.h>

#define BUF 256

int getline(char s[], int slen)
{
    int i = 0, c;
    while (i < slen - 1) {
        if ((c = getchar()) == '\n')
            break;
        else if (c == EOF)
            break;
        else
            s[i++] = c;
    }
    if (c == '\n')
        s[i++] = c;
    s[i] = '\0';

    return i;
}

int main(void)
{
    char line[BUF];
    int len;
    while ((len = getline(line, BUF)) > 0) {
        printf("%s", line);
    }

    return 0;
}

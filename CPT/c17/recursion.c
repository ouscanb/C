#include <stdio.h>
#include "thanoi.h"

int factorial(int n);
int ssummation(int n);
/* nth Fibonacci number */
int nfibonacci(int n);
int multiply(int n1, int n2);
int multiply2(int n1, int n2);
/* least commom multiple */
int lcm(int n1, int n2);
int lcm2(int n1, int n2);
/* greatest commom divisor */
int gcd(int n1, int n2);
int gcd2(int n1, int n2);
long to_binary(long n);

int main(void) {
    int i, num, num2;
    
    printf("Enter a positive integer for factorial calculation: ");
    scanf("%d", &num);
    printf("%d! = %d\n", num, factorial(num));

    printf("Enter a positive integer for summation calculation: ");
    scanf("%d", &num);
    printf("sigma(n)[n: 1->%d] = %d\n", num, ssummation(num));
    
    printf("How many Fibonacci numbers would you like to see?: ");
    scanf("%d", &num);
    for (i = 1; i <= num; i++) {
        printf("%d ", nfibonacci(i));
    }
    printf("\n");

    printf("Enter two integers(multiply): ");
    scanf("%d %d", &num, &num2);
    printf("%d * %d = %d\n", num, num2, multiply(num, num2));
    printf("%d * %d = %d\n", num, num2, multiply2(num, num2));
    
    printf("Enter two integers(lcm): ");
    scanf("%d %d", &num, &num2);
    printf("The least common multiple is    %d\n", lcm(num, num2));
    printf("The least common multiple is    %d\n", lcm2(num, num2));
    printf("The greatest common divisor is  %d\n", gcd2(num, num2));
 
    printf("Enter an integer (to_binary): ");
    scanf("%d", &num);
    printf("The binary number is %ld\n", to_binary(num));   

    printf("\n");

    printf("Enter the number of disks of the Tower of Hanoi: ");
    scanf("%d", &num);
    thanoi(num, 'A', 'B', 'C');

    return 0;
}

int factorial(int n) {
    if (n < 1) {
        return 1;
    } else {
        return n * factorial(n - 1);
    }
}

int ssummation(int n) {
    if (n < 1) {
        return 0;
    } else {
        return n + ssummation(n - 1);
    }
}

int nfibonacci(int n) {
    if (n == 1) {
        return 0;
    } else if (n == 2) {
        return 1;
    } else {
        return nfibonacci(n - 1) + nfibonacci(n - 2);
    }
}

int multiply(int n1, int n2) {
    if (n1 == 0 || n2 == 0) {
        return 0;
    } else if (n1 == 1 || n2 == 1) {
        return (n1 + n2 - 1);
    } else {
        return n1 + multiply(n1, n2 - 1);
    }
}

int multiply2(int n1, int n2) {
    static int mult = 0, i = 0;

    if (i < n1) {
        mult += n2;
        i++;
        multiply2(n1, n2);
    }

    return mult;
}

int lcm(int n1, int n2) {
    return n1 * n2 / gcd(n1, n2);
}

int gcd(int n1, int n2) {
    int i, m;
    m = (n1 > n2) ? n2 : n1;

    for (i = m; i <= m; i--) {
        if ((n1 % i == 0) && (n2 % i == 0)) {
            return i;
        }
    }

    return 1;
}

int lcm2(int n1, int n2) {
    static int i = 1;

    if ((i % n1 == 0) && (i % n2 == 0)) {
        return i;
    }
    i++;
    lcm2(n1, n2);
    
    return i;
}

int gcd2(int n1, int n2) {    
    while (n1 != n2) {
        if (n1 > n2) {
            return gcd2(n1 - n2, n2);
        } else {
            return gcd2(n1, n2 - n1);
        }
    }

    return n1;
}

long to_binary(long n) {
    static long rem, div, bin = 0, step = 1;

    if (n != 0) {
        div = n / 2;
        rem = n % 2;
        bin += rem * step;
        step *= 10;
        to_binary(div);
    }

    return bin;
}


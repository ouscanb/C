#include <stdbool.h>
#include <time.h>

typedef struct queue {
    int val;

    struct queue *next;
} node_q;

node_q *create(int n);
node_q *traverse(node_q *head);
node_q *enqueue(node_q **head, node_q *nd);
node_q *dequeue(node_q **head);
node_q *random_list(int nrand);
void print_list(node_q *stack);
void free_list(node_q **head);
void menu(void);

int main(void) {

    char c;
    int r,val;
    bool q = false;

    node_q *queue = NULL;
    while (!q) {

        menu();

        printf(">>");
        c = getchar();
        printf("\n");

        switch(c) {
            case '1':
                printf("Enter an integer: ");
                scanf("%d", &val);
                
                enqueue(&queue, create(val));

                break;

            case '2':
                dequeue(&queue);

                break;

            case '3':
                print_list(queue);
                printf("\n");

                break;

            case '4':
                printf("Enter the length of the queue: ");
                scanf("%d", &r);

                if (queue != NULL) free_list(&queue);

                queue = random_list(r);

                break;

            case '0':
                printf("\nGoodbye.\n");
                
                q = true;
                free_list(&queue);

                break;

            default:
                printf("Unrecognized option.\n");
        }
        getchar();
    }

    return 0;
}

node_q *create(int n) {
    node_q *new;

    new = malloc(sizeof(node_q));
    if (new == NULL) {
        fprintf(stderr, "Error: Memory cannot be allocated!\n");

        exit(EXIT_FAILURE);
    }

    new->val = n;
    new->next = NULL;

    return new;
}

node_q *traverse(node_q *head) {
    if (head == NULL) return NULL;

    node_q *cursor = head;
    while (cursor->next != NULL) {
        cursor = cursor->next;
    }

    return cursor;
}

node_q *enqueue(node_q **head, node_q *nd) {
    if (*head == NULL) {
        *head = nd;

        return *head;
    }

    node_q *tail = traverse(*head);
    tail->next = nd;

    return *head;
}

/*
node_q *dequeue(node_q **head) {
    if (*head == NULL) {
        printf("Queue is empty.\n");
        
        return NULL;
    } else if ((*head)->next == NULL) {
        *head = NULL;
        free(*head);

        return *head;
    }

    node_q *tmp = *head;
    
    *head = (*head)->next;
    tmp->next = NULL;
    free(tmp);

    return *head;
}
*/

node_q *dequeue(node_q **head) {
    if (*head == NULL) {
        printf("Queue is empty.\n");
        
        return NULL;
    } 

    node_q *tmp = *head;
    
    *head = (*head)->next;
    tmp->next = NULL;
    free(tmp);

    return *head;
}

node_q *random_list(int nrand) {
    int i, r;
    static int t = 1;
    time_t s;

    s = time(NULL);
    srand((unsigned) s * t);
    t++;

    node_q *head = NULL; 
    for (i = 0; i < nrand; i++) {
        r = rand() % 100;
        enqueue(&head, create(r));
    }
       
    return head;
}

void print_list(node_q *head) {
    if (head == NULL) {
        printf("Empty queue.\n");

        return;
    }

    node_q *cursor = head;
    while (cursor != NULL) {
        printf("Value: %d\n", cursor->val);
        cursor = cursor->next;
    }

    return;
}

/*
void free_list(node_q **head) {
   node_q *cursor, *tmp;

   if (*head != NULL) {
       cursor = (*head)->next;
       (*head)->next = NULL;
       free(*head);

       while (cursor != NULL) {
           tmp = cursor->next;
           cursor->next = NULL;
           free(cursor);

           cursor = tmp;
       }
   }

   return;
}
*/

void free_list(node_q **head) {
    if (head == NULL) return;

    node_q *tmp;
    while (*head != NULL) {
        tmp = *head;
        *head = (*head)->next;
        tmp->next = NULL;
        free(tmp);
    }

    return;
}

void menu(void) {
    printf("\n");
    printf("1. Enqueue an integer,\n");
    printf("2. Dequeue,\n");
    printf("3. Print the values in the queue,\n");
    printf("4. Create a queue with random_list numbers,\n");
    printf("\n");
    printf("0. Quit.\n");
}


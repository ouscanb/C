#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

void prepend(char *s, const char *p)
{
    size_t slen = strlen(s),
           plen = strlen(p);
    memmove(s + plen, s, slen + 1);
    memcpy(s, p, plen);

    return;
}

int main(int argc, char *argv[])
{
    if (argc != 3) {
        puts("Usage: main [STR] [STR]");
        exit(EXIT_FAILURE);
    }

    char *s = malloc(128);
    strcpy(s, argv[1]);
    prepend(s, argv[2]);
    puts(s);
    free(s);

    return EXIT_SUCCESS;
}

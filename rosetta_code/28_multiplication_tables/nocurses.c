/*
  nocurses.h - Provides a basic 'VT100 ESC sequences' printing without
               the need to use ncurses. 
               This is inspired by Borland conio.h

  Author     - Rahul M. Juliato
  Original   - 25 jun 2005
  Revision   -    oct 2019
*/

#include <stdio.h>
#ifdef _WIN32
# include <windows.h>
#elif defined(unix) || defined(__unix) || defined(__unix__) || defined(__APPLE__) || defined(__linux__)
# ifndef __unix__
#  define __unix__
# endif
# include <sys/ioctl.h>
# include <termios.h>
# include <unistd.h>
#endif

#include "nocurses.h"

int bg_color = BLACK,
  font_color = WHITE,
  font_bold  = FALSE;

void wait(){
    while (fgetc(stdin) != '\n');
}

void clrscr(){
    printf(ESC"[2J"ESC"[?6h");
}

void gotoxy(int x, int y){
    printf(ESC"[%d;%dH", y, x);
}

void setfontcolor(int color){
    printf(ESC"[3%dm", color);
    font_color = color;
}

void setbgrcolor(int color){
    printf(ESC"[4%dm", color);
    bg_color = color;
}

void setfontbold(int status){
    printf(ESC"[%dm", status);
    font_bold = status;
    setfontcolor(font_color);
    setbgrcolor(bg_color);
}

void setunderline(int status){
    if (status) status = 4;
    printf(ESC"[%dm", status);
    setfontcolor(font_color);
    setbgrcolor(bg_color);
    setfontbold(font_bold);
}

void setblink(int status){
    if (status) status = 5;
    printf(ESC"[%dm", status);
    setfontcolor(font_color);
    setbgrcolor(bg_color);
    setfontbold(font_bold);
}

void settitle(char const* title) {
    printf(ESC"]0;%s\x7", title);
}

void setcurshape(int shape){
    // vt520/xterm-style; linux terminal uses ESC[?1;2;3c, not implemented
    printf(ESC"[%d q", shape);
}

struct termsize gettermsize(){
    struct termsize size;
#ifdef _WIN32
    CONSOLE_SCREEN_BUFFER_INFO csbi;

    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
    size.cols = csbi.srWindow.Right - csbi.srWindow.Left + 1;
    size.rows = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;
#elif defined(__unix__)
    struct winsize win;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &win);
    size.cols = win.ws_col;
    size.rows = win.ws_row;
#else
    size.cols = 0;
    size.rows = 0;
#endif
    return size;
}

int getch(){
#ifdef _WIN32
    HANDLE input = GetStdHandle(STD_INPUT_HANDLE);
    if (h == NULL) return EOF;

    DWORD oldmode;
    GetConsoleMode(input, &oldmode);
    DWORD newmode = oldmode & ~(ENABLE_LINE_INPUT | ENABLE_ECHO_INPUT);
    SetConsoleMode(input, newmode);
#elif defined(__unix__)
    struct termios oldattr, newattr;
    tcgetattr(STDIN_FILENO, &oldattr);

    newattr = oldattr;
    newattr.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newattr);
#endif

    int ch = getc(stdin);

#ifdef _WIN32
    SetConsoleMode(input, oldmode);
#elif defined(__unix__)
    tcsetattr(STDIN_FILENO, TCSANOW, &oldattr);
#endif

    return ch;
}

int getche(){
#ifdef _WIN32
    HANDLE input = GetStdHandle(STD_INPUT_HANDLE);
    if (h == NULL) return EOF;

    DWORD oldmode;
    GetConsoleMode(input, &oldmode);
    DWORD newmode = oldmode & ~ENABLE_LINE_INPUT;
    SetConsoleMode(input, newmode);
#elif defined(__unix__)
    struct termios oldattr, newattr;
    tcgetattr(STDIN_FILENO, &oldattr);

    newattr = oldattr;
    newattr.c_lflag &= ~ICANON;
    tcsetattr(STDIN_FILENO, TCSANOW, &newattr);
#endif

    int ch = getc(stdin);

#ifdef _WIN32
    SetConsoleMode(input, oldmode);
#elif defined(__unix__)
    tcsetattr(STDIN_FILENO, TCSANOW, &oldattr);
#endif

    return ch;
}

void clrline(){
    printf(ESC"[2K"ESC"E");
}

void resetcolors(){
    printf(ESC"[0m");
}

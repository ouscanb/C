#include <stdio.h>
#include <sys/types.h>

#define BUF 1024

size_t getline(char line[], size_t max);
ssize_t strindex(char source[], const char pattern[]);
ssize_t strrindex(char source[], const char pattern[]);
ssize_t strindex_iter(char source[], const char pattern[]);

int main(void)
{
    const char pattern[] = "ould";
    char line[BUF] = {0};
    int found = 0;

    printf("PATTERN: %s\n", pattern);

    ssize_t pind;
    while (getline(line, BUF) > 0) {
        /* if ((ind = strindex(line, pattern)) >= 0) { */
        size_t prev = 0;
        while ((pind = strindex_iter(line, pattern)) >= 0) {
            /* printf("%zu %zu ", prev, pind); */
            for (size_t i = 0; i < pind - prev; ++i)
                putchar(' ');
            putchar('^');
            prev = ++pind;
            found++;
        }
        puts("");
    }

    return found;
}

size_t getline(char s[], size_t lim)
{
    int c;
    size_t i = 0;
    while (--lim > 0 && (c = getchar()) != EOF && c != '\n')
        s[i++] = c;
    if (c == '\n')
        s[i++] = c;
    s[i] = '\0';
    return i;
}

ssize_t strindex(char s[], const char p[])
{
    for (size_t i = 0; s[i] != '\0'; ++i) {
        size_t k = 0;
        for (size_t j = i;
                p[k] != '\0' && s[j] == p[k]; ++j, ++k)
            ;
        if (k > 0 && p[k] == '\0')
            return i;
    }
    return -1;
}

ssize_t strrindex(char s[], const char p[])
{
    ssize_t ind = -1;
    for (size_t i = 0; s[i] != '\0'; ++i) {
        size_t k = 0;
        for (size_t j = i;
                p[k] != '\0' && s[j] == p[k]; ++j, ++k)
            ;
        if (k > 0 && p[k] == '\0')
            ind = i;
    }
    return ind;
}

ssize_t strindex_iter(char s[], const char p[])
{
    static ssize_t ind = 0;
    for (size_t i = ind; s[i] != '\0'; ++i) {
        size_t k = 0;
        size_t j = i;
        for (;p[k] != '\0' && s[j] == p[k]; ++j, ++k)
            ;
        if (k > 0 && p[k] == '\0') {
            ind = j;
            return i;
        }
    }
    ind = 0;
    return -1;
}

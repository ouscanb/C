#include <stdio.h>
#include <stdlib.h>
#include "nocurses.h"

int main(int argc, char *argv[])
{
    clrscr();
    clrline();
    setfontbold(TRUE);
    printf("\
 #     # #    # #     ####### # #####  #     #     #\n\
 ##   ## #    # #        #    # #    # #      #   #\n\
 # # # # #    # #        #    # #    # #       # #\n\
 #  #  # #    # #        #    # #####  #        #\n\
 #     # #    # #        #    # #      #        #\n\
 #     #  ####  #####    #    # #      #####    #\n\n");
    setfontcolor(GREEN);
    printf("%s", "   | ");
    for (int i = 1; i <= 12; i++)
        printf("%3d ", i);
    printf("\n");
    printf("%s", "___|");
    for (int i = 1; i <= 12; i++)
        printf("%s", "____");
    printf("\n");
    setfontbold(FALSE);
    for (int i = 1; i <= 12; i++) {
        setfontbold(TRUE);
        setfontcolor(GREEN);
        printf("%2d | ", i);
        setfontbold(FALSE);
        resetcolors();
        for (int j = 1; j <= 12; j++) {
            /* if (j < i) */
            /*     printf("%3s ", ""); */
            /* else */
            /*     printf("%3d ", i * j); */
            printf(j < i ? "    " : "%3d ", i * j);
        }
        printf("\n");
    }
    puts("Press any key to quit.");
    wait();
    clrscr();

    return EXIT_SUCCESS;
}

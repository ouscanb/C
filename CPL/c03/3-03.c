#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include <stdbool.h>

#define ARRLEN(X) ((sizeof(X)) / (sizeof(X[0])))

#define EXPBUF 128

static bool check_range(int st, int ed)
{
    if ((st < ed) &&
            (
             (isupper(st) && isupper(ed)) ||
             (islower(st) && islower(ed)) ||
             (isdigit(st) && isdigit(ed))
            )
       )
        return true;
    else
        return false;
}

static bool check_repeated(int c, char s[], size_t slen)
{
    for (size_t i = 0; i < slen; ++i)
        if (s[i] == c)
            return true;
    return false;
}

static char *expand(const char s1[], char s2[])
{
    size_t slen;
    assert(slen = strlen(s1));

    size_t i, j;
    for (i = j = 0; i < slen && j < EXPBUF - 1; ++i)  {
        int c = s1[i];
        if (c == '-' && (i == 0 || i == slen - 1)) {
            if (!check_repeated('-', s2, j))
                s2[j++] = c;
        } else if (c == '-') {
            int range_st = s1[i - 1];
            int range_ed = s1[i + 1];
            if (!check_range(range_st, range_ed)) {
                printf("Not a valid range!\n");
                return NULL;
            }
            for (int k = range_st; k <= range_ed; ++k) {
                if (!check_repeated(k, s2, j))
                    s2[j++] = k;
            }
        }
    }
    s2[j] = '\0';

    return s2;
}

int main(void)
{
    char str_expn[EXPBUF];
    char *str_expr[] = {
        "a-b-c", "a-z0-9", "a-zA-Z0-9",
        "c-m7-9", "C-M7-9",
        "-a-z", "a-z-", "-a-z-",
        "z-a", "Z-A9-0", "---", ""
    };
    size_t elen = ARRLEN(str_expr);
    for (size_t i = 0; i < elen; ++i) {
        printf("[%10s] ", str_expr[i]);
        if (expand(str_expr[i], str_expn))
            printf("%s\n", str_expn);
    }

    return 0;
}

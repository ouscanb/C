#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <string.h>
#include "calc.h"

extern int var;
static double vars[26] = {0.0};
static int vars_def[26] = {0};

double func(char s[])
{
    double op2;
    if(strcmp(s, "sin") == 0)
        return sin(pop());
    else if (strcmp(s, "exp") == 0)
        return exp(pop());
    else if (strcmp(s, "pow") == 0) {
        op2 = pop();
        return pow(pop(), op2);
    } else
        fprintf(stderr, "Function %s is not supported!\n", s);

    return 0.0;
}

void setvar(int var, double f)
{
    size_t id = tolower(var) - 'a';
    vars[id] = f;
    vars_def[id] = 1;
}

int getvar(int var)
{
    size_t id = tolower(var) - 'a';
    return vars[id];
}

int checkvar(int var)
{
    size_t id = tolower(var) - 'a';
    return vars_def[id];
}

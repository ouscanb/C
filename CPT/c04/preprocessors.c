#include <stdio.h>

#ifndef PI
#define PI 3.1415
#endif

#undef PI
#define PI 3

#define MESSAGE(A) printf("Hoşgeldin, %s.\n\n", #A)

#define MAX(A, B) (((A) > (B)) ? (A) : (B))

int main()
{
    MESSAGE(Totoro);
    int a, b;
    
    printf("İki pozitif tamsayı giriniz:\n");
    scanf("%d%d", &a, &b);
    printf("En büyük tamsayı: %d\n", MAX(a, b));

    //printf("\nExecutable properties:\n\t%s\n\t%s\n\t%s\n",
    //        __DATE__, __TIME__, __FILE__);
    printf("\nSource file: %s\n", __FILE__);

    return 0;
}

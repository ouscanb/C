#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define BUFSIZE 1024

char *strncpy(char *dest, const char *src, size_t n)
{
    size_t i;
    for (i = 0; i < n && src[i] != '\0'; i++)
        dest[i] = src[i];
    for (; i < n; i++)
        dest[i] = '\0';

    return dest;
}

char *strdup(const char *s)
{
    char *s_dup = malloc(strlen(s) + 1);
    if (s_dup != NULL)
        strcpy(s_dup, s);

    return s_dup;
}

int main(int argc, char *argv[])
{
    const char *s = (argc > 1) ? argv[1] : "Hello world!";
    char s_copy[BUFSIZE];
    size_t s_len = strlen(s);

    assert(s_len > 0 && s_len < BUFSIZE);
    strncpy(s_copy, s, s_len)[s_len] = '\0';

    char *s_dup;
    if ((s_dup = strdup(s)) == NULL) {
        perror("Cannot duplicate the string");
        exit(EXIT_FAILURE);
    }

    printf("Text copied:        %s\n", s_copy);
    printf("Text duplicated:    %s\n", s_dup);

    free(s_dup);
    return EXIT_SUCCESS;
}

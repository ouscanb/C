#include <stdio.h>

int action = 5;
int do_something(int n);

int do_something(int n) {
    printf("I did something %d time(s) out %d.\n", n, action);

    return 0;
}

#include <stdio.h>
#include <stdlib.h>

/* Preprocessor operators: #, ##, \, defined */
/* #       -> stringize operator */
/* ##      -> token pasting operator */
/* \       -> macro continuation operator */
/* defined -> defined operator */

/* https://rosettacode.org/wiki/FizzBuzz#C */
/* #include <stdio.h> */
/* #define F(x,y) printf("%s",i%x?"":#y"zz") */
/* int main(int i){for(--i;i++^100;puts(""))F(3,Fi)|F(5,Bu)||printf("%i",i);return 0;} */

int main(int argc, char *argv[])
{
    for (int i = 1; i <= 100; i++) {
        if ((i % 15) == 0)
            printf("%3d: %s\n", i, "FizzBuzz");
        else if ((i % 3) == 0)
            printf("%3d: %s\n", i, "Fizz");
        else if ((i % 5) == 0)
            printf("%3d: %s\n", i, "Buzz");
        else
            printf("%3d:\n", i);
    }

    return EXIT_SUCCESS;
}

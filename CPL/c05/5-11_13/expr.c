#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>
#include "stack.h"

#define ARRAYSIZE(arr) ((sizeof(arr)) / (sizeof(arr[0])))

typedef enum {
    NUMBER,
    OPERATOR,
    EMPTY,
    UNKNOWN
} exprarg_t;

exprarg_t getarg(char *arg, double *num);
bool isoperator(char *arg);
bool perform_op(char op);
void usage(void);

int main(int argc, char *argv[])
{
    if (argc < 4) {
        usage();
        exit(EXIT_FAILURE);
    }

    double number = 0.0;
    while (argc-- > 0) {
        exprarg_t ret = getarg(*++argv, &number);
        switch (ret) {
            case NUMBER:
                push(number);
                break;
            case OPERATOR:
                if(!perform_op(*argv[0]))
                    exit(EXIT_FAILURE);
                break;
            case EMPTY:
                printf("Result: %g\n", pop());
                break;
            case UNKNOWN:
            default:
                puts("Illegal input!");
                exit(EXIT_FAILURE);
        }
    }

    return EXIT_SUCCESS;
}

exprarg_t getarg(char *arg, double *num)
{
    exprarg_t ret = UNKNOWN;

    if (arg == NULL)
        return ret = EMPTY;

    /* see manpages strtod/NOTES */
    char *endptr;
    *num = strtod(arg, &endptr);
    if (*endptr == '\0' && endptr != arg)
        ret = NUMBER;
    else if (isoperator(arg))
        ret = OPERATOR;

    return ret;
}

bool isoperator(char *arg)
{
    char operators[] = {'+', '-','*', '/'};
    size_t len = ARRAYSIZE(operators);
    for (size_t i = 0; i < len; ++i) {
        if (operators[i] == *arg && *++arg == '\0')
            return true;
    }
    return false;
}

bool perform_op(char op)
{
    bool issuccess = true;
    switch (op) {
        double opr2;
        case '+':
            push(pop() + pop());
            break;
        case '-':
            opr2 = pop();
            push(pop() - opr2);
            break;
        case '*':
            push(pop() * pop());
            break;
        case '/':
            opr2 = pop();
            if (opr2 == 0.0) {
                puts("Division by zero!");
                issuccess = false;
            } else
                push(pop() / opr2);
            break;
        default:
            puts("Unknown operation!");
            issuccess = false;
            break;
    }
    return issuccess;
}

void usage()
{
    puts("Usage: expr 2 3 *");
}

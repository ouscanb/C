#include <stdio.h>

char *strncpy(char *d, const char *s, size_t n);
char *strncat(char *d, const char *s, size_t n);

int main(void)
{
    char s1[32];
    char s2[] = "Hello, World!";
    char s3[] = "Hello, ";
    char s4[] = "World!";
    printf("%s.\n", strncpy(s1, s2, 5));
    printf("%s.\n", strncpy(s1, s2, 13));
    printf("%s.\n", strncpy(s1, s2, 30));
    printf("%s.\n", strncat(s3, s4, 1));
    printf("%s.\n", strncat(s3, s4, 5));
    printf("%s.\n", strncat(s3, s4, 13));
    printf("%s.\n", strncat(s3, s4, 30));

    return 0;
}

char *strncpy(char *d, const char *s, size_t n)
{
    char *ret = d;

    size_t i;
    for (i = 0; i < n ; ++i)
        if (!(*d++ = *s++))
            break;

    if (*d)
        *d++ = '\0';

    return ret;
}

char *strncat(char *d, const char *s, size_t n)
{
    char *ret = d;

    for (; *d; ++d)
        ;

    size_t i;
    for (i = 0; i < n ; ++i)
        if (!(*d++ = *s++))
            break;

    if (*d)
        *d++ = '\0';

    return ret;
}

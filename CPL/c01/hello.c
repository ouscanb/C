#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#define LOWER  0
#define UPPER  300
#define STEP   20

#define OUT    0   /* outside a word    */
#define IN     1   /* inside a word     */
#define MAXWL  20  /* max. word length  */
#define ASCIIL 127 

#define MAXLEN 1024 /* max. string length of a line */

void copy(char dest[], char sour[]);
void hello(char str[]);
float ftoc(float f);
int getline(char str[], int lim);
void getmychar(void);
int wordcount(void);
void countch(void);
void wordhist(void);
void charhist(void);
long power(int n, int m);
void reverse(char str[]);

int main(void)
{
    /*
    float fahr;

    hello("world");

    puts("Fahrenheit\tCelsius");
    puts("----------\t----------");
    for (fahr = LOWER; fahr <= UPPER; fahr += STEP) {
        printf("%-10.2f\t%-10.2f\n", fahr, ftoc(fahr));
    }


    puts("");
    getmychar();
    wordcount();

    puts("");
    printf("getchar() == EOF: %d\n", (getchar() == EOF));
    printf("Value of EOF: %#x\n", EOF); // press ^D 

    puts("");
    countch();

    puts("");
    wordhist();

    puts("");
    charhist();

    puts("");
    printf("%ld\n", power(2, 16));
    printf("%ld\n", power(-2, 7));
    */

    puts("");
    int len, max = 0;
    char line[MAXLEN];
    char longest[MAXLEN];

    while ((len = getline(line, MAXLEN)) > 0) {
        if (len > max) {
            max = len;
            copy(longest, line);
        }
        if (len > 10)
            printf(">10: %d %s\n", len, longest);
    }
    if (max > 0) {
        printf("%d %s\n", max, longest);
        reverse(longest);
        printf("%d R:%s\n", max, longest);
    }

    return 0;
}

void copy(char dest[], char sour[])
{
    int i = 0;
    while ((dest[i] = sour[i]) != '\0')
        ++i;
}

void hello(char str[])
{
    printf("Hello, %s.\n", str);

    return;
}

float ftoc(float f)
{
    return (5.0 * (f - 32.0)) / 9.0;
}

int getline(char str[], int lim)
{
    int i, j, c;
    int inspace = 0;
    for (i = 0, j = 0; (i < lim - 1) && ((c = getchar()) != EOF) && c != '\n'; ++i) {
        if (c == ' ' || c == '\t') {
            if (!inspace)
                inspace = i;
        }
        else {
            inspace = 0;
        }
        str[j++] = c;
    }
    if (inspace)
        i = j = inspace;
    if (c == '\n') {
        if (i <= lim -1)
            str[j++] = c;
        ++i;
    }
    if (c == EOF)
        puts("");
    str[j] = '\0';

    return i;
}

void getmychar(void)
{
    int c;
    int nc = 0;
    int nl = 0;
    int nb = 0;
    int pb = 0; /* is it previously blank */

    while((c = getchar()) != EOF) {
        ++nc;
        if (c == '\n')
            ++nl;
        else if (c == ' '){
            ++nb;
            if (pb == 1)
                c = '\0';
            else 
                pb = 1;
        }
        else if (c == '\t') {
            c = '\0';
            printf("\\t");
        }
        else pb = 0;

        putchar(c);
    }
    printf("# of chars  entered: %d\n", nc);
    printf("# of lines  entered: %d\n", nl);
    printf("# of blanks entered: %d\n", nb);

    return;
}

int wordcount(void)
{
    int c;
    int nc, nw, nl, state;

    nc = nw = nl = 0;
    state = OUT;
    while ((c = getchar()) != EOF) {
        ++nc;
        if (c == '\n' || c == '\t' || c == ' ') {
            if (c == '\n') ++nl;
            state = OUT;
        }
        else if (state == OUT) {
            ++nw;
            state = IN;
        }

        putchar(c);
    }

    printf("# of lines  entered: %d\n", nl);
    printf("# of words  entered: %d\n", nw);
    printf("# of chars  entered: %d\n", nc);

    return nw;
}

void countch(void)
{
    int c, i;
    int nwhite = 0,
        nother = 0;
    int ndigit[10];

    for (i = 0; i < 10; ++i)
        ndigit[i] = 0;

    while ((c = getchar()) != EOF) {
        if (c >= '0' && c <= '9')
            ++ndigit[c - '0'];
        else if (c == ' ' || c == '\n' || c == '\t')
            ++nwhite;
        else
            ++nother;
    }

    printf("digits: ");
    for (i = 0; i < 10; ++i) 
        printf(" %d", ndigit[i]);
    printf(", whitespace = %d, other = %d\n", nwhite, nother);

    return;
}

void wordhist(void)
{
    int c, i, j;
    int nwc = 0, state = OUT;
    int hist[MAXWL];

    for (i = 0; i < MAXWL; ++i)
        hist[i] = 0;

    while ((c = getchar()) != EOF) {
        if (c == ' ' || c == '\n' || c == '\t') {
            state = OUT;
            if (nwc != 0)
                ++hist[nwc];
            nwc = 0;
        }
        else {
            if (state == OUT) 
                state = IN;
            ++nwc;
        }
    }

    for (i = 1; i < MAXWL; ++i) {
        printf("%2d: ", i);
        for (j = 0; j < hist[i]; ++j) {
            printf("-");
        }
        printf("\n");
    }

    return;
}

void charhist(void)
{
    int c, i, j;
    int hist[ASCIIL];

    for (i = 0; i < ASCIIL; ++i)
        hist[i] = 0;

    while ((c = getchar()) != EOF) {
        ++hist[c];
    }

    for (i = 33; i < ASCIIL; ++i) {
        printf("%c: ", i);
        for (j = 0; j < hist[i]; ++j) {
            printf("-");
        }
        printf("\n");
    }

    return;
}

long power(int n, int m)
{
    int result;

    assert(m >= 0);

    if (n == 0 && m == 0)
        exit(EXIT_FAILURE);
    else if (m == 0)
        return 1;
    else {
        for (result = 1; m > 0; --m) {
            result *= n;
        }
    }

    return result;
}

void reverse(char str[])
{
    int lim;
    for (lim = 0; str[lim] != '\n' && str[lim] != '\0'; ++lim);
    if (lim < 1)
        return;

    int i, j, tmp;
    for (i = 0, j = lim - 1; i < (float)lim / 2; ++i) {
        tmp = str[i];
        str[i] = str[j];
        str[j--] = tmp;
    }
}

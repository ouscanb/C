#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
    char s[5];
    do {
        fgets(s, 5, stdin);
        char *p;
        if ((p = strchr(s, '\n')) != NULL)
            *p = '\0';
        printf("%c\n", atoi(s));
    } while (*s);

    return 0;
}

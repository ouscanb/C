#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>

#define BUF			32
#define WIDTH		100
#define HEIGTH		100
#define ITER_LIMIT	144

typedef struct {
	int red;
	int green;
	int blue;
} Pixel;

typedef struct {
	size_t width;
	size_t height;
	FILE *image;
} Image;

int is_convergent(double complex c);

int main()
{
	FILE *fp = fopen("image.ppm" , "w+");
	if (!fp) {
		perror("Can't open image: ");
		exit(EXIT_FAILURE);
	}

	Image img = { .width = WIDTH, .height = HEIGTH, .image = fp };

	char ppm_header[BUF];
	snprintf(ppm_header, BUF, "%s\n%ld %ld\n%d", "P3", img.width, img.height, 256);
	fprintf(img.image, "%s\n", ppm_header);

	Pixel (*pdata)[WIDTH] = calloc(HEIGTH, sizeof(*pdata));
	for (size_t i = 0; i < img.height; ++i) {
		for (size_t j = 0; j < img.width; ++j) {
			Pixel pcur = pdata[i][j];
			int ret;
			if ((ret = is_convergent((float) j / img.width - 1.5 + ((float) i / img.height - 0.5) * I))) {
				pcur = (Pixel) {.red = ret, .green = ret, .blue = ret};
			}
			fprintf(img.image, "%d %d %d\n", pcur.red, pcur.green, pcur.blue);
		}
		fprintf(img.image, "\n");
	}

	fclose(img.image);
	free(pdata);

	return EXIT_SUCCESS;
}

int is_convergent(double complex c)
{
	size_t iter = 0;
	double complex z = 0;
	while (cabs(z) < 2 && iter <= ITER_LIMIT) {
		z = z * z + c;
		++iter;
	}

	return (iter < ITER_LIMIT) ? 255 * ((float) iter / ITER_LIMIT) : 0;
}

#include <stdio.h>

void selection_sort(int arr[], int arr_len) {
    /* t: temporary element 
     * mi: index of the minimum */
    int i, j, t, mi; 

    for (i = 0; i < arr_len - 1; i++) {
        mi = i;
        
        for (j = i + 1; j < arr_len;j++) {
            if (arr[j] < arr[mi]) {
                mi = j;
            }
        }
        
        if (mi != i) {
            /* swap the minimum with the current 
             * element at index i */
            t = arr[i];
            arr[i] = arr[mi];
            arr[mi] = t;
        }
    }

    return;
}

void swap(int *n1, int *n2) {
    int t;

    t = *n1;
    *n1 = *n2;
    *n2 = t;

    return;
}

void insertion_sort(int arr[], int arr_len) {
    int i, j, t;

    /*
    for (i = 0; i < arr_len - 1; i++) {
        if (arr[i] > arr[i + 1]) {
            swap(&arr[i], &arr[i + 1]);
        } else {
            continue;
        }

        for (j = i; j > 0; j--) {
            if (arr[j - 1] > arr[j]) {
                swap(&arr[j], &arr[j - 1]);
            } else {
                break;
            }
        }
    }
    */

    for (i = 1; i < arr_len; i++) {
        j = i;

        while (j > 0 && arr[j - 1] > arr[j]) {
            t = arr[j - 1];
            arr[j - 1] = arr[j];
            arr[j] = t;

            j--;
        }
    }

    return;
}

void bubble_sort(int arr[], int arr_len) {
    /* t: temporary element */
    int i, t;
    int j = 0;
    int swap = 1;

    while (swap) {
        swap = 0;
        j++;
        /* at each iteration of the while loop the
         * maximum integer is pushed back at the end of the array,
         * therefore there is no need to check the whole array
         * but arr_len - j elements of the ith iteration */
        for (i = 0; i < arr_len - j; i++) {
            if (arr[i + 1] < arr[i]) {
                t = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = t;
                swap = 1;
            }
        }
    }

    return;
}

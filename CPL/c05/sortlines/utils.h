size_t getline(char *line, size_t maxlen);
ssize_t readlines(char *lineptr[], size_t maxlines);
void writelines(char *lineptr[], size_t nlines);

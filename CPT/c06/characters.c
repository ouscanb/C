#include <stdio.h>
#include <string.h>
#include <ctype.h>

void print_ascii(void);

int main(void) {
    char ch;

    printf("\nPress 0 to exit.\n\n");
   
    while (1) {
        printf("Enter a character: ");
        scanf("%c", &ch);

        if (ch == '0') {
            break;
        } else if (isdigit(ch)) {
            printf("You entered a digit.\n");
        } else if (isalpha(ch)) {
            printf("You entered alphabetic.\n");
        } else if (isspace(ch)) {
            printf("You entered a space.\n");
        } else {
            printf("You entered %c.\n", ch);
        }
        getchar();
    }
    
    printf("\nAscii characters:\n\n");
    print_ascii();
    printf("\n");

    return 0;
}

void print_ascii(void) {
    unsigned char a;

    for (a = 32; a < 128; a++) {
        printf("%4d = '%c'\t", a, a);
    }
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include "hash_table.h"
#include "prime.h"

#define HT_PRIME_1 13
#define HT_PRIME_2 19
#define HT_INITIAL_BASE_SIZE 53

static char *strdup(const char *s);
static ht_item *ht_new_item(const char *k, const char *v);
static ht_hash_table *ht_new_sized(const size_t base_size);
static void ht_del_item(ht_item *i);
static size_t ht_hash(const char *s, const int a, const int m);
static size_t ht_get_hash(const char *s, const int num_buckets, const int attempt);
static bool ht_resize(ht_hash_table *ht, const size_t base_size);
static bool ht_resize_up(ht_hash_table *ht);
static bool ht_resize_down(ht_hash_table *ht);

static ht_item HT_DELETE_ITEM = {NULL, NULL};

static char *strdup(const char *s)
{
	size_t len = strlen(s) + 1;

	char *d = calloc(len, sizeof(*d));
	if (d)
		strcpy(d, s);

	return d;
}

static ht_item *ht_new_item(const char *k, const char *v)
{
	ht_item *i = malloc(sizeof(*i));
	if (!i) {
		perror("malloc:");
		return NULL;
	}

	i->key = strdup(k);
	i->value = strdup(v);

	return i;
}

static ht_hash_table *ht_new_sized(const size_t base_size)
{
	ht_hash_table *ht = malloc(sizeof(*ht));
	if (!ht) {
		perror("malloc:");
		return NULL;
	}

	ht->base_size = base_size;
	ht->size = next_prime(ht->base_size);
	ht->count = 0;
	ht->items = calloc(ht->size, sizeof(ht_item *));
	/* ht->items = calloc(ht->size, sizeof(*ht->items)); */
	if (!ht->items) {
		perror("calloc:");
		return NULL;
	}

	return ht;
}

ht_hash_table *ht_new(void)
{
	return ht_new_sized(HT_INITIAL_BASE_SIZE);
}

static void ht_del_item(ht_item *i)
{
	free(i->key);
	free(i->value);
	free(i);
}

void ht_del_hash_table(ht_hash_table *ht)
{
	for (size_t i = 0; i < ht->size; ++i) {
		ht_item *item = ht->items[i];
		if (item != NULL) {
			ht_del_item(item);
		}
	}

	free(ht->items);
	free(ht);
}

static size_t ht_hash(const char *s, const int a, const int m)
{
	size_t hash = 0;
	const size_t len_s = strlen(s);
	for (size_t i = 0; i < len_s; ++i) {
		hash += (size_t)pow(a, len_s - (i + 1)) * s[i];
		hash = hash % m;
	}

	return hash;
}

static size_t ht_get_hash(const char *s, const int num_buckets, const int attempt)
{
	const size_t hash_a = ht_hash(s, HT_PRIME_1, num_buckets);
	const size_t hash_b = ht_hash(s, HT_PRIME_2, num_buckets);

	return (hash_a + (attempt * (hash_b + 1))) % num_buckets;
}

void ht_insert(ht_hash_table *ht, const char *key, const char *value)
{
	const size_t load = ht->count * 100 / ht->size;
	if (load > 70) {
		printf("Resizing hash table...\n");
		ht_resize_up(ht);
	}

	ht_item *item = ht_new_item(key, value);
	size_t index = ht_get_hash(item->key, ht->size, 0);
	ht_item *cur_item = ht->items[index];

	for (size_t i = 1; cur_item != NULL; ++i) {
		if (item != &HT_DELETE_ITEM) {
			if ((strcmp(cur_item->key, key)) == 0) {
				ht_del_item(cur_item);
				ht->items[index] = item;
				return;
			}
		}
		index = ht_get_hash(item->key, ht->size, i);
		cur_item = ht->items[index];
	}

	ht->items[index] = item;
	ht->count++;
}

ht_item *ht_search(ht_hash_table *ht, const char *key)
{
	size_t index = ht_get_hash(key, ht->size, 0);
	ht_item *item = ht->items[index];

	for (size_t i = 1; item != NULL; ++i) {
		if (item != &HT_DELETE_ITEM) {
			if ((strcmp(item->key, key)) == 0) {
				return item;
			}
		}

		index = ht_get_hash(key, ht->size, i);
		item = ht->items[index];
	}

	return NULL;
}

void ht_delete(ht_hash_table *ht, const char *key)
{
	const size_t load = ht->count * 100 / ht->size;
	if (load < 10) {
		printf("Resizing hash table...\n");
		ht_resize_down(ht);
	}

	size_t index = ht_get_hash(key, ht->size, 0);
	ht_item *item = ht->items[index];

	for (size_t i = 1; item != NULL; ++i) {
		if (item != &HT_DELETE_ITEM) {
			if ((strcmp(item->key, key)) == 0) {
				ht_del_item(item);
				ht->items[index] = &HT_DELETE_ITEM;
			}
		}

		index = ht_get_hash(key, ht->size, i);
		item = ht->items[index];
	}
	ht->count--;
}

static bool ht_resize(ht_hash_table *ht, const size_t base_size)
{
	if (base_size < HT_INITIAL_BASE_SIZE) {
		fprintf(stderr, "base_size is less than HT_INITIAL_BASE_SIZE.\n");
		return false;
	}

	ht_hash_table *new_ht = ht_new_sized(base_size);
	for (size_t i = 0; i < ht->size; ++i) {
		ht_item *item = ht->items[i];
		if (item != NULL && item != &HT_DELETE_ITEM) {
			ht_insert(new_ht, item->key, item->value);
		}
	}

	ht->base_size = new_ht->base_size;
	ht->count = new_ht->count;

	const size_t tmp_size = ht->size;
	ht->size = new_ht->size;
	new_ht->size = tmp_size;

	ht_item **tmp_items = ht->items;
	ht->items =  new_ht->items;
	new_ht->items = tmp_items;

	ht_del_hash_table(new_ht);

	return true;
}

static bool ht_resize_up(ht_hash_table *ht)
{
	const size_t new_size = ht->base_size * 2;
	return ht_resize(ht, new_size);
}

static bool ht_resize_down(ht_hash_table *ht)
{
	const size_t new_size = ht->base_size / 2;
	return ht_resize(ht, new_size);
}

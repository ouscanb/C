#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define BUFOP 128
#define NUMBER '0'

int getop(char []);
void push(double);
double pop(void);

int main(void)
{
    int optype;
    double op2;
    char s[BUFOP];

    while ((optype = getop(s)) != EOF) {
        switch (optype) {
            case NUMBER:
                push(atof(s));
                break;
            case '+':
                push(pop() + pop());
                break;
            case '*':
                push(pop() * pop());
                break;
            case '-':
                op2 = pop();
                push(pop() - op2);
                break;
            case '/':
                op2 = pop();
                if(op2 == 0.0) {
                    fprintf(stderr, "Division by zero!\n");
                    exit(EXIT_FAILURE);
                }
                push(pop() / op2);
                break;
            case '\n':
                printf("\t%.8g\n", pop());
                break;
            default:
                fprintf(stderr, "Unknown command %s\n", s);
                exit(EXIT_FAILURE);
                break;
        }
    }

    return EXIT_SUCCESS;
}

#define BUFSVAL 128

size_t spos = 0;        /* stack position */
double sval[BUFSVAL];   /* value stack*/

void push(double f)
{
    if (spos < BUFSVAL)
        sval[spos++] = f;
    else
        fprintf(stderr, "Stack is full! Can' t push %g \n", f);
}

double pop(void)
{
    if (spos > 0)
        return sval[--spos];
    else {
        fprintf(stderr, "Stack is empty!\n");
        return 0.0;
    }
}

int getch(void);
void ungetch(int);

int getop(char s[])
{
    int i, c;

    while ((s[0] = c = getch()) == ' ' || c == '\t')
        ;
    s[1] = '\0';
    if (!isdigit(c) && c != '.')
        return c;
    i = 0;
    if (isdigit(c))
        while (isdigit(s[++i] = c = getch()))
            ;
    if (c == '.')
        while (isdigit(s[++i] = c = getch()))
            ;
    s[i] = '\0';
    if (c != EOF)
        ungetch(c);

    return NUMBER;
}

#define BUFSIZE 128

char buf[BUFSIZE];  /* buffer for ungetch */
int bufpos = 0;     /* next free position in buf */

int getch(void )
{
    return (bufpos > 0) ? buf[--bufpos] : getchar();
}

void ungetch(int c)
{
    if (bufpos >= BUFSIZE)
        fprintf(stderr, "ungetch: too many characters!\n");
    else
        buf[bufpos++] = c;
}

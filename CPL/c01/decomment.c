#include <stdio.h>
#include <stdlib.h>

#define BUF 1024

int getline(char s[], int slen);

int main(void)
{
    int len = 0;
    char line[BUF];
    while ((len = getline(line, BUF)) > 0) {
        puts(line);
    }

    return 0;
}

int getline(char s[], int slen)
{
    static int in_com = 0;

    int i, c, cp = '\0';
    for (i = 0; i < slen - 1 && (c = getchar()) != EOF && c != '\n'; ++i) {
        if (cp == '/' && c == '*') {
            in_com = 1;
            s[i - 1] = ' ';
        }
        else if (cp == '*' && c == '/') {
            in_com = 0;
            s[i - 1] = ' ';
            c = ' ';
        }
        if (!in_com)
            s[i] = c;
        else
            s[i] = ' ';
        cp = c;
    }
    if (in_com) {
        s[0] = '\0';
    }
    else {
        if (c == '\n')
            s[i] = '\n';
        s[++i] = '\0';
    }

    return i;
}

#include <stdio.h>
#include <ctype.h>

long htol(char s[]);
int lower(int c);
long powerl(int n, int m);

int main(void)
{
    char s[33];
    printf("Hexadecimal number: ");
    scanf("%32s", s);
    printf("Decimal number:     %ld\n", htol(s));

    return 0;
}

long htol(char s[])
{
    long n = 0;
    int cp = '\0', c;

    int isprefixed = 0;
    int i;
    for (i = 0; s[i] != '\0'; ++i) {
        c = s[i];
        if ((c == 'x' || c == 'X') && cp == '0') {
            isprefixed = 1;
            break;
        }
        cp = c;
    }
    int sind = 0;
    int isvalid = 0;
    for (i = isprefixed ? sind = i + 1 : 0; (s[i] >= '0' && s[i] <= '9') ||
                (s[i] >= 'A' && s[i] <= 'F') ||
                (s[i] >= 'a' && s[i] <= 'f'); ++i) {
        if (!isvalid) isvalid = 1;
    }

    --i;
    if (isprefixed && isvalid) {
        int h = i - sind, j = sind;
        while (h >= 0) {
            if (isalpha(s[j]))
                n += powerl(16, h) * (lower(s[j]) - 'a' + 10);
            else
                n += powerl(16, h) * (s[j] - '0');
            --h;
            ++j;
        }
    } else if (isvalid) {
        int h = i, j = 0;
        while (h >= 0) {
            if (isalpha(s[j]))
                n += powerl(16, h) * (lower(s[j]) - 'a' + 10);
            else
                n += powerl(16, h) * (s[j] - '0');
            --h;
            ++j;
        }
    }

    return n;
}

int lower(int c)
{
    if (c >= 'A' && c <= 'Z')
        return c + 'a' - 'A';
    else
        return c;
}

long powerl(int n, int m)
{
    long res = 1;
    if (m == 0)
        return res;

    while (m-- > 0)
        res *= n;

    return res;

}

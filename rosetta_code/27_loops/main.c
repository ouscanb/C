#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

static bool isprime(unsigned long n)
{
    if (n < 2)
        return false;

    for (unsigned long i = 2; i <= n / 2; i++)
        if (n % i == 0)
            return false;

    return true;
}

int main(int argc, char *argv[])
{
    for (size_t i = 1; i <= 5; i++) {
        for (size_t j = 1; j <= i; j++)
            putchar('*');
        puts("");
    }

    time_t t = time(NULL);
    srand((unsigned) t);
    for (;;) {
        int r = rand() % 20;
        printf("%d\n", r);
        if (r == 10)
            break;
    }

    size_t pc = 0;
    for (unsigned long i = 42; pc < 20; i++) {
        if (isprime(i) == true) {
            pc++;
            printf("%02zu: %ld\n", pc ,i);
            i += i - 1;
        }
    }

    int step = 2;
    for (size_t i = 0; i < 11; i += step)
        printf("%zu\n", i);

    return EXIT_SUCCESS;
}

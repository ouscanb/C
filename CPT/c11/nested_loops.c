#include <stdio.h>
#include <math.h>

void print_primes(int x);
void print_hpyramid(int row);
void print_rpyramid(int row);
void print_mpyramid(int row); 
void print_pyramid(int row, int col);
void floyd_tri(int row);
long pascal_fac(int n);
void pascal_tri(int row);

int main(void) {
    int n;
    int q = 0;

    int h, w;

    printf("Display prime numbers up to: ");
    scanf("%d", &n);
    
    print_primes(n);
    print_hpyramid(5);
    print_rpyramid(10);
    print_mpyramid(10);
    
    while (!(q)) {
        printf("Enter the height of the pyramid :\n");
        scanf("%d", &h);
        printf("Enter the width of the pyramid  :\n");
        scanf("%d", &w);
       
        print_pyramid(h, w);
        
        printf("Press 0 to quit, 1 to continue.");
        scanf("%d", &q);

        if (q == 0 ) {
            break;
        } else {
            q = 0;
        }
   }

    floyd_tri(10);

    printf("\n");

    pascal_tri(10);

    return 0;
}

void print_primes(int x) {
    /* check Sieve of Eratosthenes */ 
    int i, j;
    for (i = 2; i < x; i++) {
        for (j = 2; j <= (i / j); j++) {
            /*
            printf("i:%2d j:%2d i/j:%2d i%%j:%2d\n", i, j, i/j, i%j);
            if(!(i % j)) break;
            */
            if((i % j) == 0) break;
        }
        if (j > (i / j)) printf("%d ", i);
    }
    printf("\n");
}

void print_hpyramid(int row) {
    int i, j;
    for (i = 1; i <= row; i++) {
        for (j = 1; j <= i; j++) {
            printf("*");
        }
        printf("\n");
    }
}


void print_rpyramid(int row) {
    int i, j, n;
    n = row;
        
    for (i = 1; i <= row; i++) {
        for (j = 1; j < n; j++) {
            printf(" ");
        }
        n--;

        for (j = 1; j <= (2 * i - 1); j++) {
            printf("*");
        }
        printf("\n");
    }
}

void print_mpyramid(int row) {
    int i, j, k = 1, n;
    n = row;
        
    for (i = 1; i <= row; i++) {
        for (j = 1; j < n; j++) {
            printf(" ");
        }
        n--;

        for (j = 1; j <= (2 * i - 1); j++) {
            if (k % 2 == 1) {
                printf("*");
                k = 0;
            } else {
                printf("A");
                k = 1;
            }
        }
        k = 1;
        printf("\n");
    }
}

void print_pyramid(int row, int col) {
    /* width of the base should be odd to
     * build a proper pyramidal shape
     */
    col = ((col % 2) == 0) ? (col + 1) : (col); 
    
    /* number of blocks to be added to
     * consecutive floors to form pyramidal shape
     */
    int m;
    m = round(((float) col - 1) / ((float) row - 1));
    //m = (col - 1) / (row - 1);
    
    /* it is a pyramid not the Eiffel Tower */
    if (m < 2) {
        printf("Pyramid cannot be built.\n");
        return;
    }
    
    /* there is only one block at the top */
    int b = 1;
    int t; /* for modified b var */
    
    int i, j, k;
    for (i = 1; i <= row; i++) {
         /* block count in the floors having even number
         * of blocks needs to be increased by one to form
         * proper pyramidal shape
         */
        if (i == row) t = col;
        else t = ((b % 2) == 0) ? (b + 1) : (b);

        for (j = 1; j <= ((col - t) / 2) ; j++) {
            printf(" ");
        }
        for (k = 1; k <= t; k++) {
            printf("*");
        }
        b += m;

        printf("\n");
    }
}

void floyd_tri(int row) {
    int i, j;
    int k = 1;

    for (i = 1; i <= row; i++) {
        for (j = 1; j <= i; j++) {
            printf("%d ", k++);
        }
        printf("\n");
    }
}

long pascal_fac(int n) {
    if (n < 1) {
        return 1;
    } else {
        return n * pascal_fac(n - 1); 
    }
}


void pascal_tri(int row) {
    int n, r, nr;
    /*int i;*/

    for (n = 0; n < (row - 1); n++) {
        /*
        for (i = 0; i < (row - n); i++) {
            printf(" ");
        }
        */
        for (r = 0; r <= n; r++) {
            nr = pascal_fac(n) / (pascal_fac(r) * pascal_fac(n - r));
            printf("%d ", nr);
        }
        printf("\n");
    }
}


 

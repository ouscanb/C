#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <time.h>

#define ARRL 10

typedef struct node {
    int data;

    struct node *left;
    struct node *right;
} node_t;

node_t *create(int data);
node_t *insert(node_t **root, int data);
node_t *lookup(node_t *root, int data);
node_t *findmin(node_t *root);
node_t *delete(node_t **root, int data);
void tree_destroy(node_t **root);
void traverse_inorder(node_t *root);
int *rarr(void);
int menu(void);

int main(void) {
    node_t *tree = NULL;
    bool q = false;

    do {
        int choice = menu();
        int data;

        switch (choice) {
            case 1:
                printf("Enter a integer: ");
                scanf("%d", &data);
                insert(&tree, data);

                break;

            case 2:
                printf("Enter an integer: ");
                scanf("%d", &data);
                delete(&tree, data);

                break;

            case 3:
                if (tree != NULL) tree_destroy(&tree);
                //tree = NULL;

                int *arr = rarr();
                for (int i = 0; i < ARRL; ++i) {
                    insert(&tree, arr[i]);
                }

                break;

            case 4:
                puts(""); 
                if (tree == NULL)
                    printf("Tree is empty.");
                else
                    traverse_inorder(tree);
                puts(""); 

                break;

            case 0:
                tree_destroy(&tree);
                puts("End of the program.");
                q = true;

                break;

            default:
                puts("Unrecognized option.");

                break;
        }
    } while(!q);

    return 0;
}

node_t *create(int data) {
    node_t *new = malloc(sizeof(node_t));
    assert(new);

    new->data = data;
    new->left = NULL;
    new->right = NULL;

    return new;
}

node_t *insert(node_t **root, int data) {
    if (*root == NULL) return *root = create(data);

    if (data < (*root)->data)
        insert(&(*root)->left, data);
    else if (data > (*root)->data)
        insert(&(*root)->right, data);

    return *root;
}

node_t *lookup(node_t *root, int data) {
    if (root == NULL || root->data == data)
        return root;

    if (data < root->data)
        return lookup(root->left, data);
    else
        return lookup(root->right, data);
}

node_t *findmin(node_t *root) {
    if (root->left)
        findmin(root->left);

    return root;
}

node_t *delete(node_t **root, int data) {
    if ((*root) == NULL) return NULL;

    if (data < (*root)->data)
        delete(&(*root)->left, data);
    else if (data > (*root)->data)
        delete(&(*root)->right, data);
    else {
        if ((*root)->left == NULL) {
            node_t *temp = (*root);
            (*root) = (*root)->right;
            free(temp);

            return (*root);
        } else if ((*root)->right == NULL) {
            node_t *temp = (*root);
            (*root) = (*root)->left;
            free(temp);

            return (*root);
        } else {
            node_t *min = findmin((*root)->right);

            (*root)->data = min->data;
            delete(&(*root)->right, min->data);
        }
    }

    return *root;
}

void tree_destroy(node_t **root) {
    if (*root != NULL) {
        tree_destroy(&(*root)->left);
        tree_destroy(&(*root)->right);
        free(*root);
    }
    *root = NULL;

    return;
}

void traverse_inorder(node_t *root) {
    if (root != NULL) {
        traverse_inorder(root->left);
        printf("%d ", root->data);
        traverse_inorder(root->right);
    }

    return;
}

int *rarr(void) {
    static int rarr[ARRL];
    static int t = 1;

    time_t s = time(NULL);
    srand(s * t);
    ++t;

    for (int i = 0; i < ARRL; ++i) {
        rarr[i] = rand() % 100;
    }

    return rarr;
}

int menu(void) {
    puts("");
    puts("1. Insert an integer,");
    puts("2. Delete an integer,");
    puts("3. Create a BST filled with random integers,");
    puts("4. Display the inorder traversal of the tree,");
    puts("");
    puts("0. Quit.");
    puts("");
    printf(">> ");

    int c = 0;
    scanf("%d", &c);

    return c;
}

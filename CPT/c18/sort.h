extern void selection_sort(int arr[], int arr_len);
extern void swap(int *n1, int *n2);
extern void insertion_sort(int arr[], int arr_len);
extern void bubble_sort(int arr[], int arr_len);

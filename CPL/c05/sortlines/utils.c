#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include "alloc.h"

#define MAXLEN 1024

size_t getline(char *line, size_t maxlen)
{
    int c;
    size_t i;
    for (i = 0; i < maxlen - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
        line[i] = c;
    if (c == '\n')
        line[i++] = c;
    line[i] = '\0';

    return i;
}

ssize_t readlines(char *lineptr[], size_t maxlines)
{
    int len, nlines = 0;
    char *p, line[MAXLEN];

    while ((len = getline(line, MAXLEN)) > 0) {
        if (!strchr(line, '\n')) {
            int c;
            while ((c = getchar()) != EOF && c != '\n')
                ;
        }
        if (nlines >= maxlines || (p = alloc(len + 1)) == NULL)
            return -1;
        else {
            line[strcspn(line, "\n")] = '\0';
            strncpy(p, line, len + 1);
            lineptr[nlines++] = p;
        }
    }

    return nlines;
}

void writelines(char *lineptr[], size_t nlines)
{
    while (nlines-- > 0)
        printf("%s\n", *lineptr++);
}

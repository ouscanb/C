/* An incomplete emulator for Space Invader game */
/* http://emulator101.com/ */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

#include "emulator.h"
#include "disassembler.h"

#include "spaceInvaders.h"

#define CPUDIAG_BIN

static State8080 *Init8080(void);
static void Free8080(State8080 *state);
static void ReadIntoMemory(State8080 *state, char *filename, uint32_t offset);
static void Usage(void);

int main(int argc, char **argv)
{
	if (argc < 2) {
		Usage();
		exit(EXIT_FAILURE);
	}

	State8080 *state = Init8080();
	ReadIntoMemory(state, argv[1], 0x100);

#ifdef CPUDIAG_BIN
	/* https://github.com/superzazu/8080/blob/master/i8080_tests.c */
	state->pc = 0x0100;

	// inject "out 0,a" at 0x0000 (signal to stop the test)
	state->memory[0x0000] = 0xD3;
	state->memory[0x0001] = 0x00;

	// inject "out 1,a" at 0x0005 (signal to output some characters)
	state->memory[0x0005] = 0xD3;
	state->memory[0x0006] = 0x01;
	state->memory[0x0007] = 0xC9;
#endif

	while (!Emulate8080(state)) {
		time_t now = time(NULL);
		if (state->interupt_enabled && (now - state->last_interupt) / 60.0f > (1 / 60.0f)) {
			GenerateInterrupt8080(state, 2);
			state->last_interupt = now;
			state->halted = 0;
		}
	}
	Free8080(state);
	printf("\n");

	return EXIT_SUCCESS;
}

static State8080 *Init8080(void)
{
	State8080 *state = calloc(1, sizeof(*state));
	state->memory = calloc(1, 0x10000); // 16K
	state->port_in  = MachineIn8080;
	state->port_out = MachineOut8080;

	return state;
}

static void Free8080(State8080 *state)
{
	free(state->memory);
	free(state);
}

static void ReadIntoMemory(State8080 *state, char *filename, uint32_t offset)
{
	FILE *fp = fopen(filename, "rb");
	if (!fp) {
		fprintf(stderr, "Error: Could not open %s\n", filename);
		exit(EXIT_FAILURE);
	}

	fseek(fp, 0L, SEEK_END);
	size_t fsize = ftell(fp);
	fseek(fp, 0L, SEEK_SET);

	uint8_t *buffer = &state->memory[offset];

	fread(buffer, fsize, 1, fp);
	fclose(fp);
}

static void Usage(void)
{
	printf("Usage: e8080 [ROM]\n");
}
